//
//  MyTabBarViewController.swift
//  autolayouting
//
//  Created by Manam on 06/04/22.
//

import UIKit

class MyTabBarViewController: UITabBarController,UITabBarControllerDelegate {
    let gradientlayer = CAGradientLayer()


    override func viewDidLoad() {
        super.viewDidLoad()
        centerButton()
        
        
        self.tabBar.layer.masksToBounds = false
        self.tabBar.layer.shadowColor = UIColor(red: 18/255.0, green: 47/255.0, blue: 105/255.0, alpha: 0.3).cgColor
        self.tabBar.layer.shadowOpacity = 0.4
            self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.tabBar.layer.shadowRadius = 8

        
        
        
        
        
        
        
        
        
        

    }

//    func addGradient(){
//    let gradient = CAGradientLayer()
//    let topColor = UIColor(red: 49/255.0, green: 131/255.0, blue: 198/255.0, alpha: 1.0)
//    let bottomColor = UIColor(red: 22/255.0, green: 76/255.0, blue: 148/255.0, alpha: 1.0)
//    gradient.colors = [topColor.cgColor,bottomColor.cgColor]
//    }
        
       


  
    
    
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabBar.frame.size.height = tabBar.frame.size.height*1.2
        tabBar.frame.origin.y = view.frame.height - tabBar.frame.size.height
       // tabBar.layer.masksToBounds = false
        //tabBar.layer.borderColor = UIColor(red: 18/255.0, green: 47/255.0, blue: 105/255.0, alpha: 1.0).cgColor
//        for vc in self.viewControllers! {
//            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            vc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
//        }
//        self.tabBar.unselectedItemTintColor = UIColor.systemBlue
        
        
    }
    //TO ADD BUTTON
    func centerButton(){
        
//        let buttonView = UIView(frame: CGRect(x: (self.view.bounds.width/2)-31, y: -8, width: 60, height: 60))
//        buttonView.layer.cornerRadius = 10
//        //middleButton.backgroundColor=UIColor.blue
//        middleButton.setImage(UIImage(named: "qrCode5"), for: .normal)
//
////        middleButton.clipsToBounds=true
//        // ADD to the Tab BAR
////        self.tabBar.addSubview(middleButton)
////        middleButton.addTarget(self, action: #selector(centerButtonAction), for: .touchUpInside)
////        self.view.layoutIfNeeded()
//
//
//        buttonView.clipsToBounds=true
//        let gradient = CAGradientLayer()
//        gradient.frame = buttonView.bounds
//        let topColor = UIColor(red: 49/255.0, green: 131/255.0, blue: 198/255.0, alpha: 1.0)
//        let botomColor = UIColor(red: 22/255.0, green: 76/255.0, blue: 148/255.0, alpha: 1.0)
//        gradient.colors = [topColor.cgColor, botomColor.cgColor]
//        buttonView.layer.insertSublayer(gradient, at: 0)
//
//
//
//
//
//        buttonView.layer.shadowColor = UIColor(red: 21/255.0, green: 87/255.0, blue: 176/255.0, alpha: 0.49).cgColor;
//        buttonView.layer.shadowRadius = 9.0
//        buttonView.layer.shadowOffset = CGSize(width: 0, height: 3)
//
////        middleButton.layer.shadowColor = UIColor(red: 21/216.0, green: 200/222.0, blue: 255/231.0, alpha: 0.6).cgColor
//
//        buttonView.layer.shadowOpacity = 1
////        middleButton.layer.shadowPath = UIBezierPath(roundedRect:middleButton.bounds, cornerRadius:middleButton.layer.cornerRadius).cgPath
////        middleButton.layer.masksToBounds=true
//
//
       
//
//        self.tabBar.addSubview(buttonView)
        
        let buttonView1 = UIView(frame: CGRect(x: (self.view.bounds.width/2)-24, y: -8, width: 48, height: 48))
       
        
        //shadow
        buttonView1.clipsToBounds=false
        buttonView1.layer.shadowColor = UIColor(red: 21/255.0, green: 87/255.0, blue: 176/255.0, alpha: 0.49).cgColor;
        buttonView1.layer.shadowOffset = CGSize(width: 0, height: 4)
        buttonView1.layer.shadowOpacity = 1.0
        buttonView1.layer.shadowRadius = 5.0
        buttonView1.layer.borderColor=UIColor(red: 132/255.0, green: 200/255.0, blue: 255/255.0, alpha: 0.6).cgColor
       
       
                let middleButton = UIButton(frame: buttonView1.bounds)
        //gradient

        let gradient = CAGradientLayer()
        gradient.frame = middleButton.bounds
        let topColor = UIColor(red: 49/255.0, green: 131/255.0, blue: 198/255.0, alpha: 1.0)
        let botomColor = UIColor(red: 22/255.0, green: 76/255.0, blue: 148/255.0, alpha: 1.0)
        gradient.colors = [topColor.cgColor, botomColor.cgColor]
        middleButton.layer.insertSublayer(gradient, at: 0)
            //middleButton.setImage(UIImage(named: "qrCode5"), for: .normal)

        // corner radius
        middleButton.clipsToBounds=true

        middleButton.layer.cornerRadius = buttonView1.frame.height/2
        
        
        let image = UIImage(named: "qrCode5.png")
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 13, y: 14, width: 22, height: 22)
        middleButton.addSubview(imageView)
        
      
        middleButton.addTarget(self, action: #selector(centerButtonAction), for: .touchUpInside)

      
        
        buttonView1.addSubview(middleButton)

        self.tabBar.addSubview(buttonView1)
        

        self.view.layoutIfNeeded()

       


        
      

     
        ///button
        
        
        
        
        
     
    }
    
    
    
    //Click
    @objc func centerButtonAction(sender:UIButton){
        self.selectedIndex = 2
        
        
        
    }

}
