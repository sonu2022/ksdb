//
//  LoginViewController.swift
//  autolayouting
//
//  Created by Manam on 18/07/22.
//

import UIKit
import TextFieldEffects

class LoginViewController: UIViewController,UITextFieldDelegate, UICollectionViewDelegate {
    
    @IBOutlet var pleaseloginLabel: UILabel!
    
    @IBOutlet var welcomeSatwikLabel: UILabel!
    
    
    @IBOutlet var wholeEnterpinView: UIView!
    
    @IBOutlet var enterPinView: UIView!
    
    
    @IBOutlet var loginWithTouchID: UIView!
    
    
    
//    @IBOutlet var enterYourPinTextField: UITextField!
    
    @IBOutlet var enterThePinLabel: UILabel!
    
    @IBOutlet var ForgotPinButton: UILabel!
    
    
    @IBOutlet var LoginWithTouchIDButton: UIButton!
    
    
//bill pay call
//    let ApiBillClass = BillPaymentApiClass()
    var billPaymentArray:[BillersList] = []


    
    
    private var enterYoutPinTextField: HoshiTextField = {
    let enterYoutPinTextField = HoshiTextField(frame: CGRect(x: 46, y:575, width: 261, height: 29))
        enterYoutPinTextField.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        enterYoutPinTextField.placeholderColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        enterYoutPinTextField.placeholderFontScale = 1.1
        enterYoutPinTextField.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        enterYoutPinTextField.placeholder="Enter Your PIN"

        

        return enterYoutPinTextField
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        billPayCall()

        
        
        
        
        wholeEnterpinView.layer.cornerRadius = 22
        
        
        enterPinView.layer.cornerRadius = 28
        
        
        loginWithTouchID.layer.cornerRadius = 28
        loginWithTouchID.layer.shadowColor = UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 0.11).cgColor
        loginWithTouchID.layer.shadowRadius = 10.0
        loginWithTouchID.layer.shadowOffset = CGSize(width: 10.0, height: 15.0)
        loginWithTouchID.layer.shadowOpacity = 0.99
        loginWithTouchID.layer.shadowPath = UIBezierPath(roundedRect:loginWithTouchID.bounds, cornerRadius:loginWithTouchID.layer.cornerRadius).cgPath
        
        
        enterPinView.addSubview(enterYoutPinTextField)
        enterYoutPinTextField.translatesAutoresizingMaskIntoConstraints = false
        
        enterYoutPinTextField.centerYAnchor.constraint(equalTo: enterPinView.centerYAnchor).isActive = true
        enterYoutPinTextField.centerXAnchor.constraint(equalTo: enterPinView.centerXAnchor).isActive = true
        

        
        initializeHideKeyboard()
        enterYoutPinTextField.delegate = self
        if enterYoutPinTextField.isTouchInside{
            
        }
        enterYoutPinTextField.inputView = NumericKeyboard(target: enterYoutPinTextField)
        
        
        
      
        
         }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         
            if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
                nextField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
            return false
        }
    
    
//    func billPayCall(){
//        ApiBillClass.PostBillerList(channel:"App",inst: 1, comp:{ Datas in
//
//
//
//            self.billPaymentArray = Datas.billersList!
//
//                DispatchQueue.main.async {
//            let story=UIStoryboard.init(name: "Main", bundle: nil)
//            let showPackageView=story.instantiateViewController(withIdentifier: "ViewController2") as? ViewController2
//            showPackageView?.billPaymentArray=self.billPaymentArray
////            showPackageView?.apiBillPaymentCollectionView.delegate = self
//
//
//
//        }
//
//
//
//
//
//    })
//
//    }
    
    
//    func jsonDataRequest(_ parameters: Any?) -> Data? {
//        var dictionaryResult: [AnyHashable : Any] = [:]
//
//        if parameters is NSString {
//            ((parameters as AnyObject).components(separatedBy: "&") as NSArray?)?.enumerateObjects({ obj, idx, stop in
//                let components = (obj as AnyObject).components(separatedBy: "=")
//                dictionaryResult[components[0]] = components[1]
//            })
//        } else {
//            if let parameters = parameters as? [AnyHashable : Any] {
//                dictionaryResult = parameters
//            }
//        }
//
//        var convertedJsonData: Data? = nil
//        do {
//            convertedJsonData = try JSONSerialization.data(withJSONObject: dictionaryResult, options: [])
//        } catch {
//            print("Json string to data conversion error")
//        }
//        var jsonToString: String? = nil
//        if let convertedJsonData = convertedJsonData {
//            jsonToString = String(data: convertedJsonData, encoding: .utf8)
//        }
//        print("the final json output: %@", jsonToString as Any)
//
//        return convertedJsonData
//
//    }
    
    
    
}
    extension LoginViewController{
        
        func initializeHideKeyboard(){
            let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissMyKeyboard))
        
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
           }
           
        @objc func dismissMyKeyboard(){
            view.endEditing(true)

           }
        
        
    
    }
    
    
    
   
   
    

    
    
    
    
    
    
   
    
    
    
   

