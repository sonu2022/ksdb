//
//  Login2ViewController.swift
//  autolayouting
//
//  Created by Manam on 22/07/22.
//

import UIKit

class Login2ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var outerStackView: UIStackView!
    
    @IBOutlet var innerStackView: UIStackView!
    
    @IBOutlet var TouchIDView1: UIView!
    
    @IBOutlet var TouchIDView2: UIView!
    
    @IBOutlet var enterPinBtn: UIButton!
    @IBOutlet var PinView: UIView!
    
    @IBOutlet var EnterPinButton: UIView!
    
    var alt = false
    
    @IBOutlet var TouchIDBtn: UIButton!
    
    
    @IBOutlet var TouchIDViewBtn: UIButton!
    
    
    @IBOutlet var innerVie: UIView!
    
    @IBOutlet var backSpaceButton: UIButton!
    
    @IBOutlet var PinPadView: UIView!
    
    
    @IBOutlet var FooterView: UIView!
    
    @IBOutlet var SwipeUpFooterView: UIView!
    
    
    @IBOutlet var enterPinView: UIView!
    
    
    @IBOutlet var PinLabel: UITextField!
    
    var pinString: String?
    
    @IBOutlet var BackGroundImage: UIImageView!
    
    @IBOutlet var ProceedBtn: UIButton!
    
    
    @IBOutlet var BtnValue: [UIButton]!
    
    
    
    
    
    
    var selectedBiometric = false
    var laidOut = false
    var faceID = false
    
    
    private var tapcount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backSpaceButton.isHidden = true
        ProceedBtn.isUserInteractionEnabled = false
        
        PinPadView.isHidden = true
        TouchIDView2.isHidden = true

        tapcount = 0
        
        
        
        let swipeUPRecog = UISwipeGestureRecognizer(target: self, action:#selector(swipeUPRecognizer(_:)))
        swipeUPRecog.direction = .up
        swipeUPRecog.delegate = self
        SwipeUpFooterView.addGestureRecognizer(swipeUPRecog)

        
        
        
    }

    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
     
        self.animateViews()
        for btn in BtnValue {
            btn.isUserInteractionEnabled = true
        }
        
        }
    
    
   @objc func swipeUPRecognizer(_ sender: UISwipeGestureRecognizer?) {

        if sender?.direction == .up {
            swipeUpBottomView()
        }
    }
    
    func swipeUpBottomView(){
        UIView.animate(withDuration: 0.3, animations: { [self] in

            TouchIDView2.isHidden = true
            PinPadView.isHidden = true
            if faceID {
                TouchIDView1.isHidden = false
            } else {
                TouchIDView1.isHidden = true
            }
            TouchIDView1.isHidden = false

            FooterView?.isHidden = false
            SwipeUpFooterView.isHidden = true
            alt = true
        })
    }
    
    
    
    
    override func viewWillLayoutSubviews(){
        for btn in self.BtnValue {
            btn.layer.cornerRadius = 5.0
            btn.layer.masksToBounds = true
            
            
            
            btn.addTarget(self, action: #selector(self.btnPressed(_:)), for: .touchUpInside)
            
        }
        
        ProceedBtn.layer.cornerRadius = 5.0
        ProceedBtn.layer.masksToBounds = true
        

        innerStackView.layer.cornerRadius = 10.0
        innerStackView.layer.masksToBounds = true

        innerVie.layer.cornerRadius = 10.0
        innerVie.layer.masksToBounds = true
        
        
        enterPinBtn.layer.cornerRadius = enterPinBtn.frame.size.height / 2.0
        enterPinBtn.layer.masksToBounds = false
        enterPinBtn.layer.shadowColor = UIColor.gray.cgColor
        enterPinBtn.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        enterPinBtn.layer.shadowOpacity = 0.5
        enterPinBtn.layer.shadowRadius = 0.0
        
        
        TouchIDViewBtn.layer.cornerRadius = TouchIDViewBtn.frame.size.height / 2.0
        TouchIDViewBtn.layer.masksToBounds = false
        TouchIDViewBtn.layer.shadowColor = UIColor.gray.cgColor
        TouchIDViewBtn.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        TouchIDViewBtn.layer.shadowOpacity = 0.5
        TouchIDViewBtn.layer.shadowRadius = 0.0
        
        
        TouchIDBtn.layer.cornerRadius = TouchIDBtn.frame.size.height / 2.0
        TouchIDBtn.layer.masksToBounds = false

        TouchIDBtn.layer.shadowColor = UIColor.gray.cgColor
        TouchIDBtn.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        TouchIDBtn.layer.shadowOpacity = 0.5
        TouchIDBtn.layer.shadowRadius = 0.0
        
    }
    
    
   
    
    @objc func btnPressed(_ sender: UIButton?) {
        tapcount += 1
        enterPinBtn.setTitle("", for: .normal)
    
//        let button = BtnValue
        let buttonTitle =  1
        
        
        
        
        
        
        
        
        if (tapcount == 1) {
            pinString = "\(buttonTitle)"
            self.PinLabel.text = self.pinString
            

           
            backSpaceButton.isHidden = false

            return
        }
        if (tapcount == 2) {
            pinString = pinString!+"\(String(describing: buttonTitle))"
            self.PinLabel.text = self.pinString
           
            backSpaceButton.isHidden = false

            return
        }
        if (tapcount == 3) {
            pinString = pinString!+"\(String(describing: buttonTitle))"
            self.PinLabel.text = self.pinString
           
            backSpaceButton.isHidden = false

            return
        }
        if (tapcount == 4) {
            pinString = pinString!+"\(String(describing: buttonTitle))"
            self.PinLabel.text = self.pinString

           
            backSpaceButton.isHidden = false
            ProceedBtn.isUserInteractionEnabled = true
            return
        }
        
        if (tapcount == 5) {
            pinString = pinString!+"\(String(describing: buttonTitle))"
            self.PinLabel.text = self.pinString

           
            backSpaceButton.isHidden = false
            return
        }
        if (tapcount == 6) {
            pinString = pinString!+"\(String(describing: buttonTitle))"
            self.PinLabel.text = self.pinString
            
            
            for btn in BtnValue {
                btn.isUserInteractionEnabled = false
            }
           
            return
        }
        
    }
    
    @IBAction func backSpacePressed(_ sender: Any) {
        textDeleted()
        if(tapcount == 0)
        {
            self.disableBackSpaceButton()
        }
        else if(tapcount <= 3)
        {
    //        self.proceedBtn.hidden = YES;
            self.ProceedBtn.isUserInteractionEnabled = false
            self.ProceedBtn .backgroundColor =
             UIColor(red: 13/255.0, green: 77/255.0, blue: 162/255.0, alpha: 0.20)
    
             
        }
        else
        {
    //        self.proceedBtn.hidden = NO;
            self.ProceedBtn.isUserInteractionEnabled = true
            
            self.backSpaceButton.isUserInteractionEnabled = true
        }
        
    }
    
    
    
    func disableBackSpaceButton(){
        tapcount = 0

        
        for btn in BtnValue{
            btn.isUserInteractionEnabled = true
        }
        
        
        pinString = ""
        enterPinBtn.setTitle(NSLocalizedString("Enter your PIN", comment: " "), for: .normal)

        PinLabel.text = pinString
        backSpaceButton.isHidden = true
        ProceedBtn.isUserInteractionEnabled = false
        ProceedBtn.backgroundColor = UIColor(red: 13 / 255.0, green: 77 / 255.0, blue: 162 / 255.0, alpha: 0.20)
        
        

    }
    

    
    func textDeleted(){
        
        
        
        
        tapcount = tapcount - 1
        if (tapcount < 0){
            tapcount = 0
        }
        
        for btn in BtnValue {
            btn.isUserInteractionEnabled = true
        }
        if pinString != "" {
//            pinString = pinString.substring(to: (pinString.count - 1))
            
//            pinString = pinString?.count-1 as Int
            self.PinLabel.text = self.pinString
        }
        
        
    }
    

    



    @IBAction func enterPinButtonClicked(_ sender: Any) {
        self.animateViews()
        TouchIDView2.isHidden = false
        
    }
    
    
    
    func animateViews(){
        if alt{
            UIView.animate(withDuration: 0.3, animations: { [self] in
                    //            self.touchIDView2.hidden = NO;
                PinPadView.isHidden = false
                if faceID {
                        TouchIDView2.isHidden = false
                    } else {
                        TouchIDView2.isHidden = true
                    }
                    TouchIDView1.isHidden = true
                self.FooterView.isHidden = true
                self.SwipeUpFooterView.isHidden = false
                self.alt = false
        })
        }else{
            UIView.animate(withDuration: 0.3, animations: { [self] in
//                TouchIDViewBtn.isHidden = true
                enterPinBtn.setTitle(NSLocalizedString("Enter your PIN", comment: ""), for: .normal)
                pinString = ""
                PinLabel.text = pinString
                tapcount = 0
                backSpaceButton.isHidden = true
                
            
                
                ProceedBtn.isUserInteractionEnabled = false
                ProceedBtn.backgroundColor = UIColor(red: 13 / 255.0, green: 77 / 255.0, blue: 162 / 255.0, alpha: 0.20)
//
                TouchIDView1.isHidden = false
                PinPadView.isHidden = true

                
                self.FooterView.isHidden = false
                self.SwipeUpFooterView.isHidden = true
                self.alt = true
            
        })

       
    }
   
    }
    
    

    @IBAction func swipeUp(_ sender: Any) {
        self.swipeUpBottomView()
        
    }
    
    @available(iOS 13.0, *)
    
    @IBAction func proceedBtn(_ sender: Any) {
        enterPinBtn.isUserInteractionEnabled = false
        TouchIDViewBtn.isUserInteractionEnabled = false
        TouchIDBtn.isUserInteractionEnabled = false
        
        
        DispatchQueue.main.async(execute: { [self] in
            PinView.isHidden = true
            PinPadView.isHidden = true
           
        })
        
        
        let story = UIStoryboard(name: "Main", bundle: nil)
              let controller = story.instantiateViewController(identifier: "MyTabBarViewController") as! MyTabBarViewController
              let navigation = UINavigationController(rootViewController: controller)
              self.view.addSubview(navigation.view)
              self.addChild(navigation)
//              navigation.didMove(toParent: self)
        navigationController?.present(navigation, animated: true, completion: nil)

        
    }
    
//    MyTabBarViewController
}
