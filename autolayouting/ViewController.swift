//
//  ViewController.swift
//  autolayouting
//
//  Created by Manam on 23/03/22.
//

import UIKit


class ViewController: UIViewController {
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet var PayShopView: UIView!
    @IBOutlet var PayBillsView: UIView!
    @IBOutlet var TopUpView: UIView!
    @IBOutlet var SendView: UIView!
    @IBOutlet var MiddleView: UIView!
   
    @IBOutlet var OfferView: UIView!
    @IBOutlet var ApplyPlatinumView: UIView!
        @IBOutlet var ApplyLoanView: UIView!
    
    //MY SERVICE COLLECTIONVIEW
    @IBOutlet var firstCollectionView: UICollectionView!
    
 
    @IBOutlet var BillPaymentView: UIView!
    //@IBOutlet var BillPaymentView: UIView!
    
   // @IBOutlet var BillPaymentView: BillPaymentCollectionViewCell!
    
    @IBOutlet var secondCollectionView: UICollectionView!
    
    @IBOutlet var thirdCollectionView: UICollectionView!
    
    private var labels:String = "SavingAccount"
    
    
  //BILL PAYMENT
    @IBOutlet var More: UIButton!
    
    @IBOutlet var DownArrow: UIImageView!
    
   // @IBOutlet var ElectricityLabel: UILabel!
    
    
    
    
    
    
    
    
    
    var secondCollectionViewImages:[UIImage] = [UIImage(named: "myAccount.png")!,UIImage(named: "myBills.png")!,UIImage(named: "loans.png")!,UIImage(named: "agent.png")!,UIImage(named: "merchant.png")!,UIImage(named: "myDigitalId.png")!]
   let secondCollectionViewlabel=["My Account","My Bills","Loans","Agent","Merchant","My DigitalID"]

    
    var thirdCollectionViewImages:[UIImage] = [UIImage(named: "electricity.png")!,UIImage(named: "khanePani.png")!,UIImage(named: "internet.png")!,UIImage(named: "ksdrcCards.png")!,UIImage(named: "landline.png")!,UIImage(named: "tv.png")!,UIImage(named: "smartSchool.png")!,UIImage(named: "eLearning.png")!]
   // let thirdCollectionViewLabel=["Electricity","Khanepani","Internet","RC Card","Landline","TV","School","eLearning"]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        thirdCollectionView.delegate = self
        thirdCollectionView.dataSource = self
        secondCollectionView.delegate = self
        secondCollectionView.dataSource = self
      
        //collectionView.register(UINib(nibName: "FirstCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell1");
        //secondCollectionView.register(FirstAccountCollectionViewCell.self, forCellWithReuseIdentifier: "cell3")

        
                //MIDDLE VIEW
        PayShopView.layer.cornerRadius=15
        PayBillsView.layer.cornerRadius=15
        TopUpView.layer.cornerRadius=15
        SendView.layer.cornerRadius=15
        MiddleView.layer.cornerRadius=25
        
                //MY COLLECTION SERVICE VIEW
        
      
        
       
                
        //MyDigitalId.layer.shadowPath = UIBezierPath(rect: MyDigitalId.bounds).cgPath
      // MyDigitalId.layer.rasterizationScale=UIScreen.main.scale
        
        //OFFer View
        ApplyPlatinumView.layer.cornerRadius=18
        ApplyLoanView.layer.cornerRadius=18
        
        
        
        
        
      //BILL PAYMENTVIEW
        self.BillPaymentView.layer.cornerRadius=25
       //line to test
//        self.More.layer.cgColor = .red
        //
       
        self.More.layer.cornerRadius = 19.0
        self.More.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;
        self.More.layer.masksToBounds = false
        self.More.layer.shadowRadius = 3.0
        self.More.layer.shadowOffset = CGSize(width: 0, height: 3.0)

        self.More.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
        
        
        self.More.layer.shadowOpacity = 0.1
        self.More.layer.shadowPath = UIBezierPath(roundedRect:More.bounds, cornerRadius:More.layer.cornerRadius).cgPath
        
        
        
        
        
        
    }
    
    
    
    
    
   
}
extension ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView==firstCollectionView)
        {
            return 2
        }
        else if(collectionView==thirdCollectionView){
            return 8
        }
        return 6
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView==firstCollectionView){
        if indexPath.row == 0{
        
            let cell = firstCollectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as! FirstCollectionViewCell
            cell.withdrawlabel.text="WithDraw"
            cell.WithdrawView.layer.cornerRadius=20
             
            cell.McashLabel.text="MCash"
            cell.McashView.layer.cornerRadius = 20
             
             
            cell.videoBankingLabel.text="Video Bank"
            cell.VideoBankingView.layer.cornerRadius=20
             
             
            cell.Rupees.text="रू 24,600.50"
             
            cell.SavingAccountName.text="SavingAccountName"
            cell.SavingCardView.layer.cornerRadius = 9
            cell.FirstCell.layer.cornerRadius = 20
            cell.AccountNumber.text="AccountNumber"
           
             return cell
        }    else {
            let cell2 = firstCollectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! SecondCollectionViewCell
            cell2.Secondview.layer.cornerRadius=20
            
            cell2.AccountNumber.text="Account number"
            cell2.Rupees2.text="रू 2,34,850.00"
            cell2.fdAccount.text="fdAccount"
            cell2.fdCardView.layer.cornerRadius=20
            
            cell2.Detailsview.layer.cornerRadius=20
            cell2.Withdraw2View.layer.cornerRadius=20
            cell2.StatementView.layer.cornerRadius=20
            
            cell2.Detailslabel.text="Details"
            cell2.Withdraw2label.text="Withdraw"
            cell2.Statementlabel.text="Statement"
            
            
            return cell2
        }
            
        
        }
        //if(collectionView==secondCollectionView){
        //for _ in 0..<secondCollectionView.numberOfItems(inSection: 0){

                 //   let indexPath = NSIndexPath(row:row, section:0)
       else if(collectionView==secondCollectionView){
           
       // if indexPath.row==0{
                
                let cell3 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath as IndexPath) as! FirstAccountCollectionViewCell
            cell3.FirstAccount.layer.cornerRadius=19.0
                cell3.FirstAccountLabel.text = "My Account"
           let image = secondCollectionViewImages[indexPath.row]
           cell3.FirstAccountImage.image=image
           let label = secondCollectionViewlabel[indexPath.row]
           cell3.FirstAccountLabel.text=label
            cell3.self.layer.cornerRadius = 19.0
            cell3.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;
            cell3.self.layer.masksToBounds = false
            cell3.self.layer.shadowRadius = 19.0
            cell3.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

            cell3.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
            cell3.self.FirstAccount.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
            cell3.self.FirstAccount.layer.shadowRadius=3.0
            cell3.self.FirstAccount.layer.cornerRadius = 19.0
            cell3.self.FirstAccount.layer.masksToBounds = false
           
            cell3.self.layer.shadowOpacity = 0.1
            cell3.self.layer.shadowPath = UIBezierPath(roundedRect:cell3.bounds, cornerRadius:cell3.FirstAccount.layer.cornerRadius).cgPath
                            
                return cell3
       /*         }
                
                 if indexPath.row==1{
                let cell4 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell4", for: indexPath as IndexPath) as! MyBillsCollectionViewCell
                cell4.MyBIllsView.layer.cornerRadius=19
                cell4.MyBillsLabel.text="My Bills"
                    
                     cell4.self.layer.cornerRadius = 19.0
                     cell4.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;                    cell4.self.layer.masksToBounds = false
                     cell4.self.layer.shadowRadius = 19.0
                     cell4.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

                     cell4.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
                     cell4.self.MyBIllsView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
                     cell4.self.MyBIllsView.layer.shadowRadius=3.0
                     cell4.self.MyBIllsView.layer.cornerRadius = 19.0
                     //cell4.self.MyBIllsView.layer.borderColor = UIColor.clear.cgColor
                     cell4.self.MyBIllsView.layer.masksToBounds = false
                    
                     cell4.self.layer.shadowOpacity = 0.1
                     cell4.self.layer.shadowPath = UIBezierPath(roundedRect:cell4.bounds, cornerRadius:cell4.MyBIllsView.layer.cornerRadius).cgPath
                    
                   
                return cell4
                }
                 if indexPath.row==2{
            let cell5 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell5", for: indexPath as IndexPath) as! LoansCollectionViewCell
                cell5.LoansView.layer.cornerRadius=19
                cell5.LoansLabel.text="Loans"
                     cell5.self.layer.cornerRadius = 19.0
                     cell5.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;                    cell5.self.layer.masksToBounds = false
                     cell5.self.layer.shadowRadius = 19.0
                     cell5.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

                     cell5.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
                     cell5.self.LoansView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
                     cell5.self.LoansView.layer.shadowRadius=3.0
                     cell5.self.LoansView.layer.cornerRadius = 19.0
                     cell5.self.LoansView.layer.masksToBounds = false
                    
                     cell5.self.layer.shadowOpacity = 0.1
                     cell5.self.layer.shadowPath = UIBezierPath(roundedRect:cell5.bounds, cornerRadius:cell5.LoansView.layer.cornerRadius).cgPath
                     
                return cell5
                }
                if indexPath.row==3{
                     let cell6 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell6", for: indexPath as IndexPath) as! AgentCollectionViewCell
                cell6.AgentView.layer.cornerRadius=19
                cell6.AgentLabel.text="Agent"
                    cell6.self.layer.cornerRadius = 19.0
                    cell6.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;                    cell6.self.layer.masksToBounds = false
                    cell6.self.layer.shadowRadius = 19.0
                    cell6.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

                    cell6.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
                    cell6.self.AgentView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
                    cell6.self.AgentView.layer.shadowRadius=3.0
                    cell6.self.AgentView.layer.cornerRadius = 19.0
                    cell6.self.AgentView.layer.masksToBounds = false
                   
                    cell6.self.layer.shadowOpacity = 0.1
                    cell6.self.layer.shadowPath = UIBezierPath(roundedRect:cell6.bounds, cornerRadius:cell6.AgentView.layer.cornerRadius).cgPath
                return cell6
                }
                if indexPath.row==4{
                    let cell7 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell7", for: indexPath as IndexPath) as! MerchantCollectionViewCell
                cell7.MerchantView.layer.cornerRadius=19
                cell7.Merchantlabel.text="Merchant"
                    cell7.self.layer.cornerRadius = 19.0
                    cell7.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;                    cell7.self.layer.masksToBounds = false
                    cell7.self.layer.shadowRadius = 19.0
                    cell7.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

                    cell7.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
                    cell7.self.MerchantView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
                    cell7.self.MerchantView.layer.shadowRadius=3.0
                    cell7.self.MerchantView.layer.cornerRadius = 19.0
                    cell7.self.MerchantView.layer.masksToBounds = false
                   
                    cell7.self.layer.shadowOpacity = 0.1
                    cell7.self.layer.shadowPath = UIBezierPath(roundedRect:cell7.bounds, cornerRadius:cell7.MerchantView.layer.cornerRadius).cgPath
                return cell7
                }
                if indexPath.row==5{
                    let cell8 = secondCollectionView.dequeueReusableCell(withReuseIdentifier: "cell8", for: indexPath as IndexPath) as! MyDigitalIdCollectionViewCell
                cell8.MyDigitalIdView.layer.cornerRadius=19
                cell8.MyDigitalIdLabel.text="My Digital ID"
                    cell8.self.layer.cornerRadius = 19.0
                    cell8.self.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;                    cell8.self.layer.masksToBounds = false
                    cell8.self.layer.shadowRadius = 19.0
                    cell8.self.layer.shadowOffset = CGSize(width: 0, height: 3.0)

                    cell8.self.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
                    cell8.self.MyDigitalIdView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
                    cell8.self.MyDigitalIdView.layer.shadowRadius=3.0
                    cell8.self.MyDigitalIdView.layer.cornerRadius = 19.0
                    cell8.self.MyDigitalIdView.layer.masksToBounds = false
                   
                    cell8.self.layer.shadowOpacity = 0.1
                    cell8.self.layer.shadowPath = UIBezierPath(roundedRect:cell8.bounds, cornerRadius:cell8.MyDigitalIdView.layer.cornerRadius).cgPath
                   
                return cell8
                } */
        }
        if(collectionView==thirdCollectionView){
          //  if indexPath.row==0{
                let cell9 = thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell9", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell9.ElectricityView.layer.cornerRadius=31.5
            let image = thirdCollectionViewImages[indexPath.row]
            cell9.ElectricityImage.image=image
     //   let label = thirdCollectionViewLabel[indexPath.row]
        
        
                return cell9

        }
        return UICollectionViewCell()
    }
   /*         if indexPath.row==1{
                let cell10=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell10", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell10.KhanepaniView.layer.cornerRadius=31.5
                return cell10

            }
            if indexPath.row==2{
                let cell11=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell11", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell11.InternetView.layer.cornerRadius=31.5
                return cell11

            }
            if indexPath.row==3{
                let cell12=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell12", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell12.RcCardView.layer.cornerRadius=31.5
                return cell12

            }
            if indexPath.row==4{
                let cell13=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell13", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell13.LandlineView.layer.cornerRadius=31.5
                return cell13

            }
            if indexPath.row==5{
                let cell14=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell14", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell14.TvView.layer.cornerRadius=31.5
                return cell14

            }
            if indexPath.row==5{
                let cell15=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell15", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
                cell15.SchoolView.layer.cornerRadius=31.5
                return cell15

            }
         //   if indexPath.row==6{
                let cell16=thirdCollectionView.dequeueReusableCell(withReuseIdentifier: "cell16", for: indexPath as IndexPath) as! BillPaymentCollectionViewCell
        cell16.ElearningView.layer.cornerRadius=31.5
        
                return cell16
          //  }
      //  }
        
     //   return UICollectionViewCell()
        
    } */

    //for giving the  size for the collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView==secondCollectionView){
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
                    let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
                    let size:CGFloat = (secondCollectionView.frame.size.height - space) / 3.0
                    return CGSize(width: size, height: size)
        }
         if(collectionView==thirdCollectionView){
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
                    let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 31.0) + (flowayout?.sectionInset.left ?? 20.0) + (flowayout?.sectionInset.right ?? 19.0)
                    let size:CGFloat = (thirdCollectionView.frame.size.height - space) / 4.0
                    return CGSize(width: size, height: size)
            
            
        }
        return CGSize(width: self.view.frame.size.width - 100, height: 300)
       }
    // as wed (-50 to width) the cell will move towards left and as we increase height we would be able to see the items in the cell
    
    
}



