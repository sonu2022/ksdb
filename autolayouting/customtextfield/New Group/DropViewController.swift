////
////  ViewController.swift
////  MakeDropDown
////
////  Created by ems on 02/05/19.
////  Copyright © 2019 Majesco. All rights reserved.
////
//
//import UIKit
//
//class DropViewController: UIViewController {
//
//    @IBOutlet weak var testLabel: UILabel!
//    
//    var dropDownArray = NSMutableArray()
//
//    
//    let dropDown = MakeDropDown()
//    
//    var dropDownRowHeight: CGFloat = 50
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setUpGestures()
//        // Do any additional setup after loading the view, typically from a nib.
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        //Call this in viewDidAppear to get correct frame values
//        setUpDropDown()
//    }
//    
//    func setUpDropDown(){
//        dropDown.makeDropDownIdentifier = "DROP_DOWN_NEW"
//        dropDown.cellReusableIdentifier = "dropDownCell"
//        dropDown.makeDropDownDataSourceProtocol = self
//        dropDown.setUpDropDown(viewPositionReference: (testLabel.frame), offset: 2)
//        dropDown.nib = UINib(nibName: "DropDownTableViewCell", bundle: nil)
//        dropDown.setRowHeight(height: self.dropDownRowHeight)
//        self.view.addSubview(dropDown)
//    }
//    
//    
//    
//    
//    
//    func setUpGestures(){
//        self.testLabel.isUserInteractionEnabled = true
//        let testLabelTapGesture = UITapGestureRecognizer(target: self, action: #selector(testLabelTapped))
//        self.testLabel.addGestureRecognizer(testLabelTapGesture)
//    }
//    
//    
//    @objc func testLabelTapped(){
//        // Give height to drop down according to requirement
//        // In this it will show 5 rows in dropdown as per calculations
//        self.dropDown.showDropDown(height: self.dropDownRowHeight * 5)
//    }
//    
//}
//
//extension ViewController: MakeDropDownDataSourceProtocol{
//    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String) {
//        if makeDropDownIdentifier == "DROP_DOWN_NEW"{
//            let customCell = cell as! DropDownTableViewCell
//            customCell.countryNameLabel.text = self.dropDownArray[indexPos]
//            customCell.cityNameLabel.text = self.cityModelArr[indexPos].cityName
//            print("test")
//        }
//    }
//    
//    func numberOfRows(makeDropDownIdentifier: String) -> Int {
//        return self.cityModelArr.count
//    }
//    
//    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String) {
//        self.testLabel.text = "Country: \(self.cityModelArr[indexPos].countryName) City: \(self.cityModelArr[indexPos].cityName)"
//        self.dropDown.hideDropDown()
//    }
//    
//}
