//
//  ZMFloatingTextField.swift
//  autolayouting
//
//  Created by Manam on 02/05/22.
//

import UIKit

extension String {
  
    var isNotEmpty: Bool {
        return !isEmpty
    }
}


extension StringProtocol {
subscript(offset: Int) -> Character {
    self[index(startIndex, offsetBy: offset)]
  }
}

//--------------DROPDOWN--------------\\
protocol MakeDropDownDataSourceProtocol{
    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String)
    func numberOfRows(makeDropDownIdentifier: String) -> Int
    
    //Optional Method for item selection
    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String)
}

extension MakeDropDownDataSourceProtocol{
    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String) {}
}


    //------------------------------\\





@available(iOS 13.0, *)
class ZMFloatingTextField:ZMFormTextField,UITextFieldDelegate,SelectCOunterDelegate,UINavigationControllerDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource{
    
    
    
    
    
    
    var zmTextField: ZMFloatingTextField?
    
    
    @objc public enum FIELD_TYPE : Int {
        case LOOKUP
        case SC_NUMBER
        case GENERIC_DROPDOWN
        case TEXTFIELD
        case PACKAGES_SELECTION
        case GENDER_SELECTION
        case CALENDAR
        case BRANCH_LIST
    }
    
    
    enum SUB_TYPES: String {
        case URADTS
        case URA
        case UMEMEPREPAID
        case UMEMEPOSTPAID
        case UMEMENEWCONNECTION
        case PAYTV
        case NWSC
        case HIMACEMENT
    }
    
    var conterArray:[Counters] = []
    var data:[Elementz] = []
    var name:String?
    var value:String?
//    var elementId:String?
//    var elementValues:String?
//    let apiClass = ApiClass()
    var previousLength : Int = 0
    @objc public var dropDownIds :NSMutableArray = NSMutableArray()
    @objc public var dropDownValues : NSMutableArray = NSMutableArray()
    
//    var subFieldType : String = SUB_TYPES.ELECTRICITY.rawValue

    
    var subFieldType : String = SUB_TYPES.URA.rawValue
    var fieldType: Int = FIELD_TYPE.TEXTFIELD.rawValue
    var floatingLabel: UILabel = UILabel(frame: CGRect(x: 36, y: 35, width: 55, height: 14))
    var hintLabel: UILabel = UILabel(frame: CGRect.zero) // hint label
    var hintLabelHeight: CGFloat = 12 // hint label size
    var floatingLabelHeight: CGFloat = 14 // Default height of floating label
    var activityView:UIActivityIndicatorView?
    var ShowCounterViews : ShowCounterView?
    
    
    var elementplace:ElementDisplayName?
    var elemts:Elements?
    private let placeholderInsets = CGPoint(x: 36, y: 30)
    let ApiBillClass = ApiCalling()

    var dropDownArray = NSMutableArray()

    
                 //--------------DropDown----------------\\
    
    var dropDownRowHeight: CGFloat = 50
    var makeDropDownIdentifier: String = "DROP_DOWN"
       // Reuse Identifier of your custom cell
       var cellReusableIdentifier: String = "DROP_DOWN_CELL"
       // Table View
       var dropDownTableView: UITableView?
       var width: CGFloat = 0
       var offset:CGFloat = 0
       var makeDropDownDataSourceProtocol: MakeDropDownDataSourceProtocol?
       var nib: UINib?{
           didSet{
               dropDownTableView?.register(nib, forCellReuseIdentifier: self.cellReusableIdentifier)
           }
       }
       // Other Variables
       var viewPositionRef: CGRect?
       var isDropDownPresent: Bool = false
    
    
                    //-----------------------------\\
    
    
    
    
    
    
    
    
    
    @IBInspectable
    var _placeholder: String? // we cannot override 'placeholder'
    @IBInspectable
    var floatingLabelColor: UIColor = UIColor.black {
        didSet {
            self.floatingLabel.textColor = floatingLabelColor
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var activeBorderColor: UIColor = UIColor.blue
    @IBInspectable
    var floatingLabelFont: UIFont = UIFont.systemFont(ofSize: 14) {
        didSet {
            self.floatingLabel.font = self.floatingLabelFont
            self.font = self.floatingLabelFont
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var hintLabelColor: UIColor = UIColor(red: 157/255, green: 165/255, blue: 174/255, alpha: 1.0) {
        didSet {
            self.hintLabel.textColor = hintLabelColor
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var hintLabelText: String = "" {
        didSet {
            self.hintLabel.text = hintLabelText
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var hintLabelFont: UIFont = UIFont.systemFont(ofSize: 10) {
        didSet {
            self.hintLabel.font = self.hintLabelFont
            self.setNeedsDisplay()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    init(frame: CGRect, fieldType: FIELD_TYPE) {
        super.init(frame: frame)
             

        self.fieldType = fieldType.rawValue
        
        commonInit()
    }
    
    
    
   
    
    func commonInit()
    {
        
       
    //        floatingLabel.frame = placeholderRect(forBounds: bounds)


     
        
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self._placeholder = (self._placeholder != nil) ? self._placeholder : placeholder // Use our custom placeholder if none is set
        placeholder = self._placeholder // make sure the placeholder is shown
        
        self.floatingLabel = UILabel(frame: CGRect(x: 36, y: 35, width: frame.size.width, height: floatingLabelHeight))
        self.hintLabel = UILabel(frame: CGRect.zero)
        self.addHintLabel()
        self.addTarget(self, action: #selector(self.addFloatingLabel), for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.removeFloatingLabel), for: .editingDidEnd)
        self.addTarget(self, action: #selector(self.scnumberHandler), for: .editingChanged)
        
        if fieldType == FIELD_TYPE.LOOKUP.rawValue || fieldType == FIELD_TYPE.GENERIC_DROPDOWN.rawValue || fieldType == FIELD_TYPE.BRANCH_LIST.rawValue{
            self.delegate = self
//            let arrow = UIImageView(image: UIImage(named: "down-arrow-2.png"))
            
   
            
          
            
                let arrow = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                arrow.image = UIImage(named: "down-arrow-2.png")
            
            
            self.addSubview(arrow)
            
           
            
            
            arrow.translatesAutoresizingMaskIntoConstraints = false
          
            arrow.trailingAnchor.constraint(equalTo:  self.trailingAnchor, constant: -50).isActive = true
//            arrow.centerXAnchor.constraint(equalTo: self.floatingLabel.centerXAnchor).isActive = true
            
            arrow.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1).isActive = true
            arrow.widthAnchor.constraint(equalToConstant: 20).isActive = true
            arrow.heightAnchor.constraint(equalToConstant: 20).isActive = true
         
            

            self.rightViewMode = UITextField.ViewMode.always    
        }
        else if fieldType == FIELD_TYPE.CALENDAR.rawValue
        {
//            let datePickerView = UIDatePicker()
//            datePickerView.datePickerMode = .date
            
            let datePickerView:UIDatePicker = UIDatePicker()
            if #available(iOS 13.4, *) {
                datePickerView.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }

            // choose the mode you want
            datePickerView.datePickerMode = UIDatePicker.Mode.date

           
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            self.inputView = datePickerView
            
            // datepicker toolbar setup
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneDatePickerPressed))

            // if you remove the space element, the "done" button will be left aligned
            // you can add more items if you want
            toolBar.setItems([space, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            toolBar.sizeToFit()

            self.inputAccessoryView = toolBar
//            self.inputView = datePickerView
//            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
    }
    
    @objc func doneDatePickerPressed(){
        self.endEditing(true)
    }
    
    func onDatePickerValueChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        self.text = dateFormatter.string(from: datePicker.date)
    }

    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        self.text = dateFormatter.string(from: sender.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
    func selectedConterDetails(names: String,values: String) {
        self.text = names
        
        self.dropDownSelectedValue = values
        
        
        print("print the names and values\(names),\(values)")
        self.addFloatingLabel()
    }
    
    
    
    //-------------added for dropdown---------------\\
    @objc func testLabelTapped(){
        // Give height to drop down according to requirement
        // In this it will show 5 rows in dropdown as per calculations
      showDropDown(height: self.dropDownRowHeight * 5)
    }
    
    
                //------------------------\\
    
    
    

    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool   {
        if fieldType == FIELD_TYPE.GENERIC_DROPDOWN.rawValue  {


            
          
            
            
            
            
            
            
//            let urlss : String = " https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerJsonFile/ "
//            let token : String = "TEST:FABBAWfHpfLmBSiKEp7h"

            //This is where we do the API Call
//            apiClass.PostApiMethod(token: token, urls:urlss, comp: { [self] Response in


                //-------Parameters--------\\

//                let jsonbeanDic: NSDictionary = ["accessCode": "EJqg7Op4VHSd2Uta1Z9QVk+8Cets8E2QoCGqVKk0lvZTvIBoaYduKtM6B4jJ7ep56oxOYfooPVLHAK76QlJf81+EBdCm7WZEHvrCwx6MWaP61oMJRdLCCl3FDJ0pLrFeM2SK8W/z9Fhh+1CxsbWC52bgVrOpSoZg0Zd3/ZsISdCILkkj8/UsS3GrpVTugViJY7X1znm358L6YfRa8MkBeiqIT91c25zkKFR+K2wXC+O+x8jj6QWZpj4wEWAl64tIvw3tFkOdZ+60RpTk6BMbjMDcfjPuAwqdoXUbZ4ouVRVxxmHDrArMpYlS5rePQjc0Pp1rVEM3UQTAx8iEYEQFtw==",
//                                                 "mobileNumber": "256757883769"
//]
//
//
//        //        print("billerList\(jsonbeanDic)")
//
//                let dataDic: NSDictionary = ["UTIL_BILLER_CODE": "NWSC_PAY"]
//
//                let deviceDic:NSDictionary = [ "deviceId": "6b0d015a-5051-4bfb-b5bb-b9455b8eadde",
//
//                                               "model": "",
//
//                                               "os": "",
//
//                                               "ip": "",
//
//                                               "location": ""]
//
//                let mainDic: NSDictionary = ["serviceName":"BILLER_JSON_FILE","requestId":"25675788376912092022185647889","newLoginBean":jsonbeanDic,"data":dataDic,"device":deviceDic]

                //--------------------------\\






//                ApiBillClass.API(Url: urlss, parameters: mainDic, httpMethodType: HTTPMethod.PUT, success:{jsonData,response in
//
//                    let bill = BillerJsons(dictionary: jsonData as NSDictionary)
//
//                    if((bill?.status!) != nil)
//                    {
//                        if bill?.data == nil{
//                            let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Unable to get information from billerlist, Please try again", comment: ""), preferredStyle: .alert)
//
//                            let cancel = UIAlertAction(title: "Close", style: .cancel) { _ in
//                                Swift.debugPrint("alert has occured")
//                            }
//                            alert.addAction(cancel)
//                            return
//                        }
//
//
//                    }
//
//
//
//
//
//                }, Error:{data, response in
//
//
//
//
//
//
//                })









            //This is where we do the API Call
//            apiClass.PostApiMethod(token: token, urls:urlss, comp: { [self] Response in



//                ApiBillClass.API(Url: urlss, parameters: mainDic, httpMethodType: HTTPMethod.PUT, success:{jsonData,response in
//
//                                        let bill = Response(dictionary: jsonData as NSDictionary)
//
//
//                    if((bill?.status) != nil)
//                {
//                        if bill?.counters == nil || bill?.counters!.count == 0 {
//                        self.activityView?.stopAnimating()
//                        let alertController=UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Unable to Fetch Counter Information, Please try again after sometime", comment: ""), preferredStyle: .alert)
//                        let Cancelaction=UIAlertAction(title: "Close", style: .cancel){ _ in
//                            Swift.debugPrint("alert has occured")
//                        }
//                        alertController.addAction(Cancelaction)
//
//                        return
//                    }
//
//
//
//                        self.conterArray = (bill?.counters)!
//                        if (bill?.counters?.count)! > 0 {
//                        DispatchQueue.main.async {
            
//
//            let storyBoards : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                      let KhanepaniViewControllers = storyBoards.instantiateViewController(withIdentifier: "KhanepaniViewController") as? KhanepaniViewController
//
//            self.dropDownArray = KhanepaniViewControllers!.dropDownArray
//            print("ULA ULA \(self.dropDownArray)")
            
            
            
            
         
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            self.ShowCounterViews = storyBoard.instantiateViewController(withIdentifier: "ShowCounterView") as? ShowCounterView

//            self.ShowCounterViews?.dropDownArray = self.dropDownArray
            
//            self.ShowCounterViews?.delegate
            
             let value = Shared.shared.stringValue
                   
            self.ShowCounterViews?.dropDownArray = value
            self.ShowCounterViews?.delegate = self
            
            
  //we cannot use present to show the showcounterview becae this class is created without a uiviewcontroller. presnt can be only used if its uiviewcontroller class.
                            //therefore we used the below code to display the value
         UIApplication.shared.keyWindow?.rootViewController?.present(self.ShowCounterViews!, animated: true, completion: nil)
//
//
//
//                            }
//
//
//                    }
//
//
////                        else {
////                    self.activityView?.stopAnimating()
////                    let alertController=UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Unable to Fetch Counter Information, Please try again after sometime", comment: ""),preferredStyle: .alert)
////
////            }
//                }
//
//
//
//        }, Error:{data, response in
//
//        })

            }
        
       
        
        return false

    }
    
    
    
    
    
    
                                   
    
    // Add a floating label to the view on becoming first responder
    @objc func addFloatingLabel() {
        if self.text == "" || self.fieldType == FIELD_TYPE.LOOKUP.rawValue || self.fieldType == FIELD_TYPE.GENERIC_DROPDOWN.rawValue  {
            self.floatingLabel.textColor = floatingLabelColor
            self.floatingLabel.font = floatingLabelFont
            self.floatingLabel.text = self._placeholder
            self.floatingLabel.layer.backgroundColor = UIColor.white.cgColor
            self.floatingLabel.clipsToBounds = true
            self.floatingLabel.frame = CGRect(x: 36, y: 35, width: self.frame.size.width, height: self.floatingLabelHeight)
            self.layer.borderColor = self.activeBorderColor.cgColor
            self.addSubview(self.floatingLabel)
            self.floatingLabel.translatesAutoresizingMaskIntoConstraints = false
            
//            rightView?.bottomAnchor.constraint(equalTo:floatingLabel.bottomAnchor, constant: 0).isActive = true
            
            self.floatingLabel.bottomAnchor.constraint(equalTo:self.topAnchor, constant: 60).isActive = true // Place our label 10pts above the text field
            self.floatingLabel.leadingAnchor.constraint(equalTo:self.leadingAnchor, constant: 36).isActive = true
//
//            self.floatingLabel.widthAnchor.constraint(equalTo:self.widthAnchor,constant: frame.size.width).isActive = true
//
//            self.floatingLabel.heightAnchor.constraint(equalTo:self.heightAnchor,constant: floatingLabelHeight).isActive = true
//


            
            
            
            
            
            
            // Remove the placeholder
            self.placeholder = ""
        }
        
        self.setNeedsDisplay()
    }
    
    
    @objc func removeFloatingLabel() {
        if self.text == "" {
            UIView.animate(withDuration: 0.13) {
                self.floatingLabel.removeFromSuperview()

                self.floatingLabel.frame = CGRect(x: 36, y: 35, width: self.frame.size.width, height: self.floatingLabelHeight)

                
                
                
                
                //                self.floatingLabel.frame = self.placeholderRect(forBounds: self.bounds)




//                let frame = CGRect(origin: CGPoint(x: 36, y: 0), size: CGSize(width: self.frame.size.width, height: self.floatingLabelHeight))
//
//                self.floatingLabel.frame = frame.insetBy(dx: self.placeholderInsets.x, dy: self.placeholderInsets.y)






//                self.floatingLabel.leadingAnchor.constraint(equalTo:self.leadingAnchor, constant: 36).isActive = true
//                self.floatingLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
//                self.floatingLabel.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
//                self.floatingLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
//

//                self.floatingLabel.leadingAnchor.constraint(equalTo:self.leadingAnchor, constant: 36).isActive = true




                //self.subviews.forEach{ $0.removeFromSuperview() }
                self.setNeedsDisplay()

            }

            self.placeholder = self._placeholder

        }
        self.layer.borderColor = UIColor.blue.cgColor

    }
    
    
    @objc func scnumberHandler() {
        if self.fieldType == FIELD_TYPE.SC_NUMBER.rawValue {
            if self.previousLength  < self.text?.count ?? 0
            {
                if self.text?.count == 4
                {
                    
                    // the error was gone because i added an extention at top
                    let mySubstring = self.text?[4]
                    if mySubstring != "." {
                        var txt = self.text!
//                        txt.insert(String: ".", ind: 3)
                        txt.insert(contentsOf: ".", at: 3 as Any as! String.Index)
                        self.text = txt
                        
                    }
                }
                else if self.text?.count == 3 || self.text?.count == 6 {
                    self.text = "\(self.text!)."
                }
                
            }
            self.previousLength = self.text?.count ?? 0
        }
    }
    
    
    @objc func addHintLabel() {
        self.hintLabel.textColor = hintLabelColor
        self.hintLabel.font = hintLabelFont
        self.hintLabel.text = hintLabelText
//        self.hintLabel.layer.backgroundColor = UIColor.white.cgColor
        self.hintLabel.translatesAutoresizingMaskIntoConstraints = false
        self.hintLabel.clipsToBounds = true
        self.hintLabel.frame = CGRect(x: 36, y: 35, width: self.frame.size.width, height: self.hintLabelHeight)
        self.layer.borderColor = self.activeBorderColor.cgColor
        self.addSubview(self.hintLabel)
        
//        self.hintLabel.bottomAnchor.constraint(equalTo:self.bottomAnchor, constant: 20).isActive = true // Place our label 10pts below the text field
        self.setNeedsDisplay()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addFloatingLabel()
        self.addBottomBorder()
        self.addHintLabel()
        self.scnumberHandler()
        floatingLabel.frame = placeholderRect(forBounds: bounds)

        
        //        self.removeFloatingLabel()
//        self.floatingLabel.frame = CGRect(x: 36, y: 0, width: self.frame.size.width, height: self.floatingLabelHeight)
//        if self.text == nil {
//            self.floatingLabel.frame = CGRect(x: 36, y: 10, width: self.frame.size.width, height: self.floatingLabelHeight)

//        }
       
        

    }
    
//    func countersName(_ name: String, value: String) {
//        self.text = name
//        self.dropDownSelectedValue = value
//        self.addFloatingLabel()
//    }
    
    
    
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 36, y: self.frame.size.height + 10, width: self.frame.size.width-80, height: 1)
        bottomLine.backgroundColor = UIColor(red: 36/255, green: 50/255, blue: 67/255, alpha: 1.0).cgColor
        borderStyle = .none
        
        
        layer.addSublayer(bottomLine)
        
     
        
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect.init(x:36, y:35, width: self.frame.size.width-100, height:55)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if isFirstResponder || text!.isNotEmpty {
            return CGRect(x: 36, y: 10, width: bounds.width, height: floatingLabelHeight)
        }
        else {
            return textRect(forBounds: bounds)
        }
    }

    
    
    
    
    
//-----------------------------DropDown---------------------------\\
    
    
    
    
    // Make Table View Programatically
        
        func setUpDropDown(viewPositionReference: CGRect,  offset: CGFloat){
            self.addBorders()
            self.addShadowToView()
            self.frame = CGRect(x: viewPositionReference.minX, y: viewPositionReference.maxY + offset, width: 0, height: 0)
            dropDownTableView = UITableView(frame: CGRect(x: self.frame.minX, y: self.frame.minY, width: 0, height: 0))
            self.width = viewPositionReference.width
            self.offset = offset
            self.viewPositionRef = viewPositionReference
            dropDownTableView?.showsVerticalScrollIndicator = false
            dropDownTableView?.showsHorizontalScrollIndicator = false
            dropDownTableView?.backgroundColor = .white
            dropDownTableView?.separatorStyle = .none
            dropDownTableView?.delegate = self
            dropDownTableView?.dataSource = self
            dropDownTableView?.allowsSelection = true
            dropDownTableView?.isUserInteractionEnabled = true
            dropDownTableView?.tableFooterView = UIView()
            self.addSubview(dropDownTableView!)
            
        }
    
    // Shows Drop Down Menu
       func showDropDown(height: CGFloat){
           if isDropDownPresent{
               self.hideDropDown()
           }else{
               isDropDownPresent = true
               self.frame = CGRect(x: (self.viewPositionRef?.minX)!, y: (self.viewPositionRef?.maxY)! + self.offset, width: width, height: 0)
               self.dropDownTableView?.frame = CGRect(x: 0, y: 0, width: width, height: 0)
               self.dropDownTableView?.reloadData()
               
               UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.05, options: .curveLinear
                   , animations: {
                   self.frame.size = CGSize(width: self.width, height: height)
                   self.dropDownTableView?.frame.size = CGSize(width: self.width, height: height)
               })
           }
           
       }
   
    
    //Hides DropDownMenu
        func hideDropDown(){
            isDropDownPresent = false
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveLinear
                , animations: {
                self.frame.size = CGSize(width: self.width, height: 0)
                self.dropDownTableView?.frame.size = CGSize(width: self.width, height: 0)
            })
        }
        
    //Sets Row Height of your Custom XIB
       func setRowHeight(height: CGFloat){
           self.dropDownTableView?.rowHeight = height
           self.dropDownTableView?.estimatedRowHeight = height
       }
    
    // Use this method if you want change height again and again
        // For eg in UISearchBar DropDownMenu
        func reloadDropDown(height: CGFloat){
            self.frame = CGRect(x: (self.viewPositionRef?.minX)!, y: (self.viewPositionRef?.maxY)!
                + self.offset, width: width, height: 0)
            self.dropDownTableView?.frame = CGRect(x: 0, y: 0, width: width, height: 0)
            self.dropDownTableView?.reloadData()
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.05, options: .curveLinear
                , animations: {
                self.frame.size = CGSize(width: self.width, height: height)
                self.dropDownTableView?.frame.size = CGSize(width: self.width, height: height)
            })
        }
    
    
    // Removes DropDown Menu
        func removeDropDown(){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveLinear
                , animations: {
                self.dropDownTableView?.frame.size = CGSize(width: 0, height: 0)
            }) { (_) in
                self.removeFromSuperview()
                self.dropDownTableView?.removeFromSuperview()
            }
        }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (makeDropDownDataSourceProtocol?.numberOfRows(makeDropDownIdentifier: self.makeDropDownIdentifier) ?? 0)

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = (dropDownTableView?.dequeueReusableCell(withIdentifier: self.cellReusableIdentifier) ?? UITableViewCell())
               makeDropDownDataSourceProtocol?.getDataToDropDown(cell: cell, indexPos: indexPath.row, makeDropDownIdentifier: self.makeDropDownIdentifier)
               
               return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            makeDropDownDataSourceProtocol?.selectItemInDropDown(indexPos: indexPath.row, makeDropDownIdentifier: self.makeDropDownIdentifier)
        }
    
    
}

extension UIView{
    func addBorders(borderWidth: CGFloat = 0.2, borderColor: CGColor = UIColor.lightGray.cgColor){
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }
    
    func addShadowToView(shadowRadius: CGFloat = 2, alphaComponent: CGFloat = 0.6) {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: alphaComponent).cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 2)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1
    }
}
