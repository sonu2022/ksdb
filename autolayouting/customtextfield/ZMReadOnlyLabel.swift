import Foundation
import UIKit
@objc class ZMReadOnlyLabel : UIView {
    
    var titleLabel: UILabel = UILabel(frame: CGRect.zero)
    var detailLabel: UILabel = UILabel(frame: CGRect.zero)
    var titleLabelHeight: CGFloat = 12
    var detailLabelHeight: CGFloat = 25
    
    @IBInspectable //157 165 174
    var titleLabelColor: UIColor =  UIColor(red: 157/255, green: 165/255, blue: 174/255, alpha: 1.0) {
        didSet {
            self.titleLabel.textColor = titleLabelColor
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var titleLabelFont: UIFont = UIFont(name: "Axiforma-Regular", size: 11.0)! {
        didSet {
            self.titleLabel.font = self.titleLabelFont
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var titleLabelText: String = "" {
        didSet {
            self.titleLabel.text = titleLabelText
            self.setNeedsDisplay()
        }
    }
    @IBInspectable //36 50 67
    var detailLabelColor: UIColor = UIColor(red: 36/255, green: 50/255, blue: 67/255, alpha: 1.0) {
        didSet {
            self.detailLabel.textColor = detailLabelColor
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var detailLabelText: String = "" {
        didSet {
            self.detailLabel.text = detailLabelText
            self.setNeedsDisplay()
        }
    }
    @IBInspectable
    var detailLabelFont: UIFont = UIFont(name: "Axiforma-SemiBold", size: 14.0)! {
        didSet {
            self.detailLabel.font = self.detailLabelFont
            self.setNeedsDisplay()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
   func commonInit() {
        
        self.titleLabel = UILabel(frame: CGRect.zero)
        self.detailLabel = UILabel(frame: CGRect.zero)
        
        
        self.titleLabel.textColor = titleLabelColor
        self.titleLabel.font = titleLabelFont
        self.titleLabel.text = titleLabelText
        self.titleLabel.layer.backgroundColor = UIColor.clear.cgColor
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.clipsToBounds = true
        self.titleLabel.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.titleLabelHeight)
        self.addSubview(self.titleLabel)
        self.titleLabel.topAnchor.constraint(equalTo:
            self.topAnchor, constant: 5).isActive = true
        
        
        self.detailLabel.textColor = detailLabelColor
        self.detailLabel.font = detailLabelFont
        self.detailLabel.text = detailLabelText
        self.detailLabel.layer.backgroundColor = UIColor.clear.cgColor
        self.detailLabel.translatesAutoresizingMaskIntoConstraints = false
        self.detailLabel.clipsToBounds = true
        self.detailLabel.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.detailLabelHeight)
        self.addSubview(self.detailLabel)
        self.detailLabel.topAnchor.constraint(equalTo:
            self.titleLabel.bottomAnchor, constant: 10).isActive = true
    }
    
}
