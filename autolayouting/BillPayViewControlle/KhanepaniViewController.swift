//
//  KhanepaniViewController.swift
//  autolayouting
//
//  Created by Manam on 25/04/22.
//
import UIKit
import TextFieldEffects
import MessageUI
import SwiftUI
import DropDown
import Realm


//---------to store value globally in shared and access it everywhere--------------\\
final class Shared {
     static let shared = Shared() //lazy init, and it only runs once

     var stringValue = NSMutableArray()
     var boolValue   : Bool!
    var primaryKeyType : String!
    var meterNumber : String!
    
    var elementId:String!

}
//-----------------------------------------\\



struct DefaultsKeys {
    static let keyOne = "firstStringKey"
    static let keyTwo = "secondStringKey"
}




extension String {
  
    var isNotEmptysss: Bool {
        return !isEmpty
    }
}




@available(iOS 13.0, *)
class KhanepaniViewController: UIViewController, SelectCOunterDelegate, URLSessionDelegate, UITextFieldDelegate, UIScrollViewDelegate {
   
//    var elementValues : String?
//    var elementId : String?
//    let apiClass = ApiClass()
    var conterArray:[Counters] = []
    
    
    var elementArray:[Elements] = []
    
    
    
    var FileName:String?
      @IBOutlet var backButton: UIButton!
        @IBOutlet var khanepaniLabel: UILabel!
        @IBOutlet var khanepaniView: UIView!
        @IBOutlet var khanepaniImagr: UIImageView!
    var khanepaniLabelObj:String?
    var khanepaniImageObj:UIImage?
    var khanepaniViewObj:UIColor?
    @IBOutlet var myView: UIView!
    
    
    //nxt button
    var nextButton : UIButton!
    let nextButtonHeightRatio : CGFloat = 0.16


    @IBOutlet var mainStackView: UIStackView!
    var constraints = [NSLayoutConstraint]()
    @objc var currentPage:Int = 1
    var formModel : BillerBase?
    var submissionFields:NSMutableArray = NSMutableArray()
    var displayOnCompletionList : NSMutableArray = NSMutableArray()
    var elementDictionary : Dictionary = [String:Elements]()
    var rpKey: String!
    
    var elementplace:ElementDisplayName?
    var elemts:Elements?
    
    
    @objc var billerType : String!
    @objc var HeaderTitle : String!
    
    
    @objc var amountSelection : Bool = false
    let dropDownHeight : CGFloat = 55
    let textFieldHeight : CGFloat = 55;
    
    enum ELEMENT_TYPES:String{
        case INPUT_TEXTFIELD
        case AMOUNT_TEXTFIELD
        case INFO_BANNER
        case LOOKUP
        case SC_NUMBER
        case LIST
        case LIST_SINGLE
        case SECRET_TEXTFIELD
        case GENERIC_DROPDOWN
        case AMOUNT_DROPDOWN
        case LARGE_SELECTION_LIST
        case READ_ONLY_LABEL
        case HIDDEN_VARIABLE
        case NON_EDITABLE_LABEL
        case MORE_POPUP
        case NEXT_BUTTON
        case SUBMIT_BUTTON
        case PACKAGES_SELECTION
        case PKGLOOKUP
    }
    
    let mainTextColor : UIColor = UIColor(red: 51/255, green: 69/255, blue: 86/255, alpha: 1.0)
//    let placeHolderTextColor : UIColor = UIColor(red: 168/255, green: 177/255, blue: 189/255, alpha: 1.0)
    let placeHolderTextColor : UIColor = UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
    let mainFont : UIFont = UIFont.systemFont(ofSize: 14.0)
    var amountField : ZMFloatingTextField!
    var variableParams : Dictionary = [String: Any]()
    @objc public var formFileType : String!

    
    var LanguageManager :Bool = false
    var laidOut : Bool = false
    var isContinuation : Bool = false
    var fromFavs:Bool = false
    var fromFavsProcessed :Bool = false
    var fromBills :Bool = false
    @objc var favRequestParams : Dictionary = [String: String]()
    var requestParams : Dictionary = [String: String]()

    
    
    var ShowCounterViews : ShowCounterView?
    let data:Data? = nil
    var dropDownArray = NSMutableArray()
    
    
  

   

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       


        

        let customBackButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem = customBackButton
        
        
        
        khanepaniView.layer.cornerRadius=31.5
        khanepaniImagr.image=khanepaniImageObj
//        khanepaniLabel.text=khanepaniLabelObj
        khanepaniView.backgroundColor=khanepaniViewObj
        navigationController?.isNavigationBarHidden=true
        khanepaniLabel.text = ""
        
        
//        view.addSubview(mainStackView)
          
        
        
        print("billerDetails === ",self.loadJson(filename: "Bp_PAYTV"))
//      var pages = formModel?.pages
    
        
        
        
       
        if (currentPage==1){
//            let formLoader : BillerBase = BillerBase(dictionary: self.loadJson(filename: "Khanepani")!)!
            formModel = BillerBase(dictionary: self.loadJson(filename: "Bp_PAYTV")!)
           
            
            //this makes the remaining pages have an heading label of payment details
            
            if (billerType == "Type_RC")
            {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
            
            }
            else{
                self.khanepaniLabel.text = HeaderTitle
            }
        }
        else if currentPage == 2 {
            if (billerType == "Type_1")
            {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
            }else if (billerType == "Type_2")
            {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
            }
            else
            {
                 self.khanepaniLabel.text = HeaderTitle
            }
        }
        else if currentPage == 3 {
            if (billerType == "Type_2")
            {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
            }else
            {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
            }
        }else
        {
                self.khanepaniLabel.text = NSLocalizedString("Payment Details", comment: "")
        }
        

        
     
     
    }
    
    
    
    public override func viewWillLayoutSubviews() {
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        
        if !laidOut {
            layoutFormFields()
            if fromFavs && !fromFavsProcessed || fromBills
            {
                for item in submissionFields {
                   if let field = item as? ZMFloatingTextField {
                       
                        if field.fieldType == ZMFloatingTextField.FIELD_TYPE.GENERIC_DROPDOWN.rawValue
//                            || field.fieldType == ZMFloatingTextField.FIELD_TYPE.DROPDOWN.rawValue
                            
                        {
                            // Do Nothing
                        }
                        else {
                            field.text = favRequestParams[field.formId]
                        }
                        
                    }
                }
                fromFavsProcessed = true
            }
            laidOut = true
        }
        
        
        
        
    }
    //converting the json to dictionary
    func loadJson(filename fileName: String) -> NSDictionary? {

        var json_result :NSDictionary?
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else { return nil }
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                json_result = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
              } catch {
                   // handle error
              }
        
        return json_result
    }
    
    
    
    @available(iOS 13.0, *)
    func layoutFormFields(){
        //accessing the pages in formmodel
        if let pages : Pages = formModel!.pages?[currentPage-1] as? Pages {
            for layout : Layouts in (pages.layouts as? [Layouts])! {
                if layout.layoutColumns == 1 {
                    layoutSingle(layout: layout)

            }
//                else if layout.layoutColumns == 2 {
//                    layoutDouble(layout: layout)
//                }
    }
            //Check if more pages are available
            if currentPage < formModel!.pages!.count {
                          let buttonView : UIView = UIView()
                          constraints.append(buttonView.heightAnchor.constraint(equalToConstant: -30.0+(self.view.frame.size.width * nextButtonHeightRatio)))
                          buttonView.backgroundColor = UIColor.clear
                          mainStackView.addArrangedSubview(buttonView);
                          mainStackView.setCustomSpacing(20, after: buttonView)
          
                          buttonView.translatesAutoresizingMaskIntoConstraints = false
                          nextButton = UIButton()
                          nextButton.translatesAutoresizingMaskIntoConstraints = false
                          buttonView.addSubview(nextButton)
                          nextButton.centerXAnchor.constraint(equalTo: buttonView.centerXAnchor).isActive = true
                          nextButton.centerYAnchor.constraint(equalTo: buttonView.centerYAnchor,constant: -222).isActive = true
                          nextButton.heightAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
                          nextButton.widthAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
                          nextButton.layer.cornerRadius = (self.view.frame.size.width * nextButtonHeightRatio) / 2.0
                          nextButton.backgroundColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 1.0)
                          nextButton.setImage(UIImage(named: "right-chevron.png"), for: .normal)
                          nextButton.imageView?.contentMode = .scaleAspectFit
                          nextButton.addTarget(self, action: #selector(buttonClicked1(sender:)), for: .touchUpInside)
          
                          nextButton.layer.shadowColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 0.18).cgColor
                          nextButton.layer.shadowOffset = CGSize(width: 0, height: 7.0)
                          nextButton.layer.shadowOpacity = 1.0
                          nextButton.layer.shadowRadius = 17.0
                          nextButton.layer.masksToBounds = false
                
            }
    
    }
    
    }
    

    
    
    func layoutSingle(layout : Layouts){
        
        //to access the elements in the layout section
        for elements : Elements in (layout.elements as? [Elements])!{
            //checking the layut coloumn to call methods to layut forms
        
            if elements.displayOnCompletion ?? false {
                displayOnCompletionList.add(elements)
        }
            
            
            
            switch elements.elementType{
                
                
                
            case ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue, ELEMENT_TYPES.LOOKUP.rawValue,ELEMENT_TYPES.GENERIC_DROPDOWN.rawValue:
                
                var type : ZMFloatingTextField.FIELD_TYPE = ZMFloatingTextField.FIELD_TYPE.TEXTFIELD
//                var type2: ZMFloatingTextField.FIELD_TYPE = ZMFloatingTextField.FIELD_TYPE.TEXTFIELD

                
                
                if elements.elementType == ELEMENT_TYPES.GENERIC_DROPDOWN.rawValue {
                    type = ZMFloatingTextField.FIELD_TYPE.GENERIC_DROPDOWN
        
                }
                if elements.elementType == ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue {
                    type = ZMFloatingTextField.FIELD_TYPE.TEXTFIELD
                }

                
                let textField : ZMFloatingTextField = ZMFloatingTextField(frame: CGRect(x: 40, y: 40, width: self.view.frame.width-40, height: 55), fieldType: type )
                
                let textfield2 : ZMFloatingTextField = ZMFloatingTextField(frame: CGRect(x: 40, y: 80, width: self.view.frame.width-40, height: 55), fieldType: type)
                
                
                
                textField.formId = elements.self.elementId!


                
                
                if elements.subType == ZMFloatingTextField.SUB_TYPES.URA.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.URA.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.URA.rawValue

                    
                }
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.URA.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.URADTS.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.URADTS.rawValue

                }
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.UMEMEPREPAID.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMEPREPAID.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMEPREPAID.rawValue
                }
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.UMEMEPOSTPAID.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMEPOSTPAID.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMEPOSTPAID.rawValue
                    
                }
                
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.UMEMENEWCONNECTION.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMENEWCONNECTION.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.UMEMENEWCONNECTION.rawValue
                    
                }
                
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.PAYTV.rawValue {
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.PAYTV.rawValue
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.PAYTV.rawValue
                    
                }
                
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.NWSC.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.NWSC.rawValue
//                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.NWSC.rawValue
                    
                }
                
                else if elements.subType == ZMFloatingTextField.SUB_TYPES.HIMACEMENT.rawValue {
                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.HIMACEMENT.rawValue
                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.HIMACEMENT.rawValue
                
                }
  
                
                if elements.elementType == ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue{
                    
                    
                    
                    
                    
                    Shared.shared.primaryKeyType = elements.primaryKeyType

                    Shared.shared.boolValue = elements.primaryData
                    
                    for item in submissionFields {
                    if let field = item as? ZMFloatingTextField {
                        requestParams[field.formId] = field.text
                        print("ololololololo\(field.text)")
                    }
                    
                    }
                    
                   
                    

                    
                    
                   

                    
                    
                    
                    
                    
                    
                    
                   
                   

                }
                
                
                

                
                if elements.elementType == ELEMENT_TYPES.GENERIC_DROPDOWN.rawValue {
                  
                    
                    
                    Shared.shared.elementId = elements.elementId
                    
                     self.convertStringtoArray(elementValue: elements.elementValues)
                   
                    textField.dropDownValues = NSMutableArray(array:Shared.shared.stringValue)

                    
                    
                  


                    
                }

                textField.floatingLabelColor = placeHolderTextColor
                textField.textColor = UIColor(red: 18/255.0, green: 47/255.0, blue: 89/255.0, alpha: 1.0)
                textField.floatingLabelFont = UIFont.systemFont(ofSize: 15.0,weight: .medium)
//                textField.floatingLabelFont = UIFont(name: "GalanoGrotesque-Medium", size: CGFloat(11.0))
//                textField.font = UIFont.systemFont(ofSize: 15,weight: .medium)//UIFont(name: element.fontName ?? "", size: CGFloat(element.fontSize ?? ""))
                
                
                textField.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))
                
                
                textField._placeholder = elements.elementPlaceHolder?.en
            
                
                textField.floatingLabel.frame = CGRect(x: 36, y: 30, width:50, height: 14)
        
                mainStackView.addArrangedSubview(textField)

                mainStackView.setCustomSpacing(50, after: textField)
                    submissionFields.add(textField)

                
                textfield2.floatingLabelColor = placeHolderTextColor
                textfield2.textColor = UIColor.red
                textfield2.floatingLabelFont = UIFont.systemFont(ofSize: 15.0,weight: .medium)
                textfield2.font = UIFont.systemFont(ofSize: 15,weight: .medium)//UIFont(name: element.fontName ?? "", size: CGFloat(element.fontSize ?? ""))
                
                textfield2.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))

                
                
                
//                textfield3.floatingLabelColor = placeHolderTextColor
//                textfield3.textColor = UIColor.red
//                textfield3.floatingLabelFont = UIFont.systemFont(ofSize: 15.0,weight: .medium)
//                textfield3.font = UIFont.systemFont(ofSize: 15,weight: .medium)//UIFont(name: element.fontName ?? "", size: CGFloat(element.fontSize ?? ""))
//
//                textfield3.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))

                
                
                constraints.append(textfield2.topAnchor.constraint(equalTo: textField.bottomAnchor,constant: 20))
                
//                constraints.append(textfield3.topAnchor.constraint(equalTo: textField.bottomAnchor,constant: 40))
//                mainStackView.addArrangedSubview(textfield2)
                mainStackView.setCustomSpacing(130, after: textfield2)

                submissionFields.add(textfield2)
//                textField._placeholder = "Select counter"
            

             
                    elementDictionary[textField.formId] = elements
                elementDictionary[textfield2.formId] = elements

                
                
//                elementDictionary[textfield3.formId] = elements
//
//                mainStackView.setCustomSpacing(80, after: textfield3)
//
//                submissionFields.add(textfield3)
//
                
                
            case ELEMENT_TYPES.READ_ONLY_LABEL.rawValue:
                let label : ZMReadOnlyLabel = ZMReadOnlyLabel(frame: CGRect(x: 40, y: 10, width: self.view.frame.width-40, height: 60))
//                label.titleLabelText = element.elementDisplayName.en
              
                constraints.append(label.heightAnchor.constraint(equalToConstant: 60))
                mainStackView.addArrangedSubview(label);
                mainStackView.setCustomSpacing(20, after: label)
                
                
            case ELEMENT_TYPES.HIDDEN_VARIABLE.rawValue:
                let textField : ZMFormTextField = ZMFormTextField(frame: CGRect.zero)
                textField.formId = elements.elementId!

                
                if elements.doesSubmit!
                {
                    submissionFields.add(textField)
                    elementDictionary[textField.formId] = elements
                }
                
                
//      
                
            default :
                let x : Int = 0
                
            
    
    
    
    
        }
    }
    
  
    }
    
   
    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String) {
        if makeDropDownIdentifier == "DROP_DOWN_NEW"{
            let customCell = cell as! DropDownTableViewCell

            customCell.countryNameLabel.text = self.dropDownArray[indexPos] as? String
            
        }}
    
    
    
    
    
    
    
    
  
    func selectedConterDetails(names : String,values :String) {
        
        
        print("elementId ==",values)
        self.khanepaniLabel.text = values
//        self.khanepaniLabel.text=elementValues

    }
    
    
    
    
    @IBAction func backButton(_ sender: Any) {
self.navigationController?.popViewController(animated: true)
        let customBackButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = customBackButton
        
    }
    
   

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationItem.setHidesBackButton(true, animated: true)}


    @available(iOS 13.0, *)
    @objc func buttonClicked1(sender: UIButton!) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PaymentDetailsViewController = story.instantiateViewController(withIdentifier: "PaymentDetailsViewController") as? PaymentDetailsViewController
        
      PaymentDetailsViewController!.khanepaniPaymentImageObj = khanepaniImageObj
        PaymentDetailsViewController!.khanepaniPaymentLabelObj = "Payment Details"
        PaymentDetailsViewController!.khanepaniPaymentViewObj = UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0)
        
        
//        let primaryDataValue = Shared.shared.boolValue
        
        
        let primaryKeyTypeValue =  Shared.shared.primaryKeyType
        
        var meterValue = Shared.shared.meterNumber
        
        let elementIdName = Shared.shared.elementId
        let defaults =  UserDefaults.standard

        
       
        for item in submissionFields {
        if let field = item as? ZMFloatingTextField {
            
            if  field.fieldType == ZMFloatingTextField.FIELD_TYPE.TEXTFIELD.rawValue {
            
            meterValue = field.text
            
            let primaryValue: NSMutableDictionary = [ primaryKeyTypeValue as Any : field.text as Any]

            defaults.setValue(primaryValue, forKey:DefaultsKeys.keyOne)

            if let stringOne = defaults.dictionary(forKey: DefaultsKeys.keyOne) {
                print("the primary dictionary\(stringOne)")
                

            }
                defaults.synchronize()
                
            
            }
            else if field.fieldType == ZMFloatingTextField.FIELD_TYPE.GENERIC_DROPDOWN.rawValue{
              
                let additionalData: NSMutableDictionary = [elementIdName as Any:field.text as Any]
                defaults.setValue(additionalData, forKey:DefaultsKeys.keyTwo)
                
                if let stringTwo = defaults.dictionary(forKey: DefaultsKeys.keyTwo) {
                    print("The additionalData is\(stringTwo)")
                    break


                }
                    defaults.synchronize()
                    

//                print("The additionalData is\(String(describing: additionalData))")

                
            }
            
            



         




       }
        }
        
        
      
        
        
//        let defaults = UserDefaults.standard
//        let name = defaults.string(forKey: "username")
//        print("The primaryValue is\(primaryValue)")
        
        
        
        
        
//        if primaryDataValue == nil{
//                        let additionalData: NSDictionary = [:]
//            print("The additionalData is\(additionalData)")
//
//        }
//        else{
//
//
//            let primaryValue: NSDictionary = [ primaryKeyTypeValue as Any : meterValue as Any]
//            print("The primaryValue is\(primaryValue)")
//        }
    
      

        
        
        
        
        self.navigationController?.pushViewController(PaymentDetailsViewController!, animated: true)

    }
    
    
    func convertStringtoArray(elementValue: Any?) -> NSMutableArray{
        

        let elementKeyArray = NSMutableArray()
        let elementValueArray = NSMutableArray()

        
        let elementsArray:[String] = elementValue as! [String]
        
        
        for item in elementsArray {
            let values = (item as AnyObject).debugDescription.components(separatedBy:"^")
            
            print("the values is \(values[0])")
            let firstName = values[0]
            let lastName = values[1]
            
            elementKeyArray.add(firstName)
            elementValueArray.add(lastName)
            
            
            
        }
        //value stored globally in Shared
        Shared.shared.stringValue = elementKeyArray
        return Shared.shared.stringValue
        
        
    }
    
   
}

