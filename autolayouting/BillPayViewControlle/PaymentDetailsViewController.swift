//
//  PaymentDetailsViewController.swift
//  autolayouting
//
//  Created by Manam on 25/04/22.
//

import UIKit
import TextFieldEffects

@available(iOS 13.0, *)
class PaymentDetailsViewController: UIViewController {
    @IBOutlet var paymentLabel: UILabel!
    
   
    
    
    @IBOutlet var backbutton: UIButton!
    
    @IBOutlet var paymentImageView: UIView!
    
    @IBOutlet var paymentkhanepaniImage: UIImageView!
    
  
    
    
//    @IBOutlet var khanepaniPaymentView: UIView!
    
    
    var khanepaniPaymentLabelObj:String?
    var khanepaniPaymentImageObj:UIImage?
    var khanepaniPaymentViewObj:UIColor?

    var payElement:Elements?
    var elementArray:[Elements] = []
    var displayOnCompletionList : NSMutableArray = NSMutableArray()
    @objc var currentPage:Int = 1
    var formModel : BillerBase?
    var Proceed : UIButton!

    @IBOutlet var curveyView: CustomizedViewController!
    
    
    
    
    private var scrollView: UIScrollView = {
            let scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: 428, height:900)
//       scrollView.backgroundColor = UIColor(red: 13/255.0, green: 77/255.0, blue: 162/255.0, alpha: 1.0)
        return scrollView
        }()
    
    private var khanepaniScrollView: UIView = {
        let khanepaniScrollView = UIView()
        khanepaniScrollView.backgroundColor = .white
        khanepaniScrollView.frame = CGRect(x: 0, y: 0, width: 428, height:900)
        return khanepaniScrollView
    }()
    
    
    private var CustomerName: HoshiTextField = {
        let CustomerName = HoshiTextField(frame: CGRect(x: 46, y:405 , width: 261, height: 29))
        CustomerName.placeholderFontScale = 1.1
        CustomerName.textColor=UIColor(red: 37/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        CustomerName.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        CustomerName.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        CustomerName.placeholder="Customer Name"
        CustomerName.tag=2
        CustomerName.tintColor = UIColor.clear
        return CustomerName
    }()
    
    
    
    
    private var mobileNumber: HoshiTextField = {
        let mobileNumber = HoshiTextField(frame: CGRect(x: 46, y:405 , width: 261, height: 29))
        mobileNumber.placeholderFontScale = 1.1
        mobileNumber.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        mobileNumber.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        mobileNumber.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        mobileNumber.placeholder="Mobile Number"
        mobileNumber.tag=2
        mobileNumber.tintColor = UIColor.clear
        return mobileNumber
    }()
    
    
    private var address: HoshiTextField = {
        let address = HoshiTextField(frame: CGRect(x: 46, y:405 , width: 261, height: 29))
        address.placeholderFontScale = 1.1
        address.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        address.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        address.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        address.placeholder="Address"
        address.tag=2
        address.tintColor = UIColor.clear
        return address
    }()
    
    
    
    private var totalDueAmount: HoshiTextField = {
        let totalDueAmount = HoshiTextField(frame: CGRect(x: 46, y:405 , width: 261, height: 29))
        totalDueAmount.placeholderFontScale = 1.1
        totalDueAmount.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        totalDueAmount.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        totalDueAmount.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        totalDueAmount.placeholder="Total Due Amount"
        totalDueAmount.tag=2
        totalDueAmount.tintColor = UIColor.clear
        return totalDueAmount
    }()
    
    
    
    private var more:UIButton!={
        let more = UIButton(frame: CGRect(x: 0, y: 0, width: 78, height: 33))
        more.addTarget(self, action:#selector(moreButtonClicked), for: .touchUpInside)
        
        more.setTitle("More", for: .normal)
        more.tintColor=UIColor(red: 237/255.0, green: 27/255.0, blue: 48/255.0, alpha: 1.0)
        more.layer.borderColor=UIColor(red: 237/255.0, green: 27/255.0, blue: 48/255.0, alpha: 1.0).cgColor
        more.setTitleColor(.red, for: .normal)
        more.layer.borderWidth = 1
        
        more.layer.cornerRadius=6
        return more
    }()
    
    
    private var Amount:HoshiTextField! = {
       let Amount = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 100))
        Amount.placeholderFontScale=1.1
        Amount.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        Amount.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        Amount.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        Amount.placeholder="Amount"
        Amount.tag=2
        Amount.tintColor = UIColor.clear
                return Amount
    }()
    
    
    private var Rupee:UILabel! = {
       let Rupee = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        Rupee.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        Rupee.backgroundColor = UIColor(red: 234/255.0, green: 236/255.0, blue: 238/255.0, alpha: 1.0)
        Rupee.text = "रू"
        Rupee.tintColor = UIColor(red: 51/255.0, green: 69/255.0, blue: 86/255.0, alpha: 1.0)
        Rupee.textAlignment = .center
        Rupee.tag=2
                return Rupee
    }()
    
    
    
    
    private var AmountLineLabel:UILabel! = {
    let AmountLineLabel = UILabel(frame: CGRect(x: 46, y: 400, width: 334, height: 1))
        AmountLineLabel.backgroundColor = UIColor(red: 33/255.0, green: 55/255.0, blue: 82/255.0, alpha: 1.0)
    return AmountLineLabel
    }()
    
    
    
    private var proccedToPay:UIView! = {
        let proccedToPay = UIView(frame: CGRect(x: 0, y: 480, width:414, height: 300))
        proccedToPay.backgroundColor = UIColor(red: 238/255.0, green: 241/255.0, blue: 246/255.0, alpha: 1.0)
        return proccedToPay
    }()
    
    
    
    private var fromAccount:UILabel! = {
       let fromAccount = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        fromAccount.textColor=UIColor.black
        fromAccount.text = "From Account"

        fromAccount.font = UIFont.systemFont(ofSize: 20, weight: .semibold)

        fromAccount.tintColor = UIColor.black
                return fromAccount
    }()
    
    
    private var accountInfo:UIView! = {
        let accountInfo = UIView(frame: CGRect(x: 0, y: 0, width:140, height: 140))
        accountInfo.backgroundColor = UIColor.white
        return accountInfo
    }()
    
    
    private var walletImage:UIImageView! = {
       let walletImage = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 40, height: 40))

        walletImage.image = UIImage(named: "wallet.png")

        return walletImage
    }()
    
    
    private var KsGoji:UILabel! = {
       let KsGoji = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        KsGoji.textColor=UIColor.black
        KsGoji.text = "KS Goji Account"

        KsGoji.font = UIFont.systemFont(ofSize: 20, weight: .medium)

        KsGoji.tintColor = UIColor.black
                return KsGoji
    }()
    
    
    
    private var pass:UILabel! = {
       let pass = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        pass.textColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        pass.text = "*****001"

        pass.font = UIFont.systemFont(ofSize: 17, weight: .medium)

        pass.tintColor = UIColor.black
                return pass
    }()
    
    
    private var ruppesSymbol:UILabel! = {
       let ruppesSymbol = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        ruppesSymbol.textColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        ruppesSymbol.text = "रू"

        ruppesSymbol.font = UIFont.systemFont(ofSize: 12, weight: .bold)

        ruppesSymbol.tintColor = UIColor.black
                return ruppesSymbol
        
        
        
    }()
    
    
    
    
    private var acctRuppes:UILabel! = {
       let acctRuppes = UILabel(frame: CGRect(x: 0, y: 0, width: 46, height: 28))
        acctRuppes.textColor=UIColor.black
        acctRuppes.text = "8672.40"

        acctRuppes.font = UIFont.systemFont(ofSize: 17, weight: .bold)

        acctRuppes.tintColor = UIColor.black
                return acctRuppes
        
            }()
    
    private var checkMark:UIImageView!={
        let checkMark = UIImageView(frame: CGRect(x: 20, y: 20, width: 100, height: 60))
//        more.addTarget(self, action:#selector(buttonClicked1), for: .touchUpInside)
//        checkMark.image = UIImage(systemName: "checkmark.png")
     
            checkMark.image = UIImage(named: "check-2.png")
        
        checkMark.tintColor = UIColor.white
        
        checkMark.contentMode = .center

        
        checkMark.backgroundColor=UIColor(red: 19/255.0, green: 80/255.0, blue: 160/255.0, alpha: 1.0)
        
        checkMark.layer.cornerRadius=6
        return checkMark
    }()
    
    
    
    
    
    private var payButton:UIButton!={
        let payButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
//        more.addTarget(self, action:#selector(buttonClicked1), for: .touchUpInside)
        
        payButton.setTitle("PROCEED TO PAY", for: .normal)
        payButton.backgroundColor=UIColor(red: 19/255.0, green: 80/255.0, blue: 160/255.0, alpha: 1.0)
        payButton.setTitleColor(.white, for: .normal)
        
        payButton.layer.cornerRadius=6
        return payButton
    }()
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        

        
        curveyView.layer.cornerRadius = 28


        
        paymentImageView.layer.cornerRadius = 30
        
//        paymentkhanepaniImage.image=khanepaniPaymentImageObj
//        paymentLabel.text=khanepaniPaymentLabelObj
        
//        paymentImageView.backgroundColor=khanepaniPaymentViewObj
        navigationController?.isNavigationBarHidden=true
        
        
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(khanepaniScrollView)

        
        khanepaniScrollView.addSubview(CustomerName)
        CustomerName.translatesAutoresizingMaskIntoConstraints=false
        khanepaniScrollView.addSubview(mobileNumber)
        mobileNumber.translatesAutoresizingMaskIntoConstraints=false
        khanepaniScrollView.addSubview(address)
        address.translatesAutoresizingMaskIntoConstraints=false
        khanepaniScrollView.addSubview(totalDueAmount)
        totalDueAmount.translatesAutoresizingMaskIntoConstraints=false
        
        
        khanepaniScrollView.addSubview(more)
        more.translatesAutoresizingMaskIntoConstraints=false
        
        
        
        Amount.addSubview(Rupee)
        Rupee.translatesAutoresizingMaskIntoConstraints=false
        
        
        
        khanepaniScrollView.addSubview(Amount)
        Amount.translatesAutoresizingMaskIntoConstraints = false
        
        
        khanepaniScrollView.addSubview(AmountLineLabel)
        AmountLineLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        khanepaniScrollView.addSubview(proccedToPay)
        proccedToPay.translatesAutoresizingMaskIntoConstraints = false
        proccedToPay.layer.cornerRadius = 28
        
        
        
        proccedToPay.addSubview(fromAccount)
        fromAccount.translatesAutoresizingMaskIntoConstraints  = false
        
        
        
        
        proccedToPay.addSubview(accountInfo)
        accountInfo.translatesAutoresizingMaskIntoConstraints  = false
        accountInfo.layer.cornerRadius = 11.5
        
        
        proccedToPay.addSubview(payButton)
        payButton.translatesAutoresizingMaskIntoConstraints  = false

        accountInfo.addSubview(walletImage)
        walletImage.translatesAutoresizingMaskIntoConstraints = false
        
        accountInfo.addSubview(KsGoji)
        KsGoji.translatesAutoresizingMaskIntoConstraints = false
        
        
        accountInfo.addSubview(pass)
        pass.translatesAutoresizingMaskIntoConstraints = false
        
        
        accountInfo.addSubview(ruppesSymbol)
        ruppesSymbol.translatesAutoresizingMaskIntoConstraints = false
        
        accountInfo.addSubview(acctRuppes)
        acctRuppes.translatesAutoresizingMaskIntoConstraints = false
        
        
        accountInfo.addSubview(checkMark)
        checkMark.translatesAutoresizingMaskIntoConstraints = false
        checkMark.layer.cornerRadius =  12.5
        
        
      
        //scroll view
        NSLayoutConstraint.activate([

        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0),
        scrollView.topAnchor.constraint(equalTo: paymentImageView.bottomAnchor, constant: 0.0),
               scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -0.0),
               scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 100.0),

        scrollView.widthAnchor.constraint(equalTo: khanepaniScrollView.widthAnchor),

               ])
        
        //view with scroll view
        
        khanepaniScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:1000)
        khanepaniScrollView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant:0.0).isActive=true
        khanepaniScrollView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0.0).isActive=true
        khanepaniScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive=true
        khanepaniScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive=true
        khanepaniScrollView.heightAnchor.constraint(equalTo: khanepaniScrollView.heightAnchor).isActive=true

        
        
        
        
        
        CustomerName.topAnchor.constraint(equalTo: khanepaniScrollView.topAnchor, constant: 40).isActive=true
        CustomerName.leftAnchor.constraint(equalTo: khanepaniScrollView.leftAnchor, constant: 36).isActive=true
        CustomerName.widthAnchor.constraint(equalToConstant: 261).isActive=true
        CustomerName.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        
        
        mobileNumber.topAnchor.constraint(equalTo: CustomerName.bottomAnchor, constant: 55).isActive=true
        mobileNumber.leftAnchor.constraint(equalTo: khanepaniScrollView.leftAnchor, constant: 36).isActive=true
        mobileNumber.widthAnchor.constraint(equalToConstant: 261).isActive=true
        mobileNumber.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        
        address.topAnchor.constraint(equalTo: mobileNumber.bottomAnchor, constant: 55).isActive=true
        address.leftAnchor.constraint(equalTo: khanepaniScrollView.leftAnchor, constant: 36).isActive=true
        address.widthAnchor.constraint(equalToConstant: 261).isActive=true
        address.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        totalDueAmount.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 55).isActive=true
        totalDueAmount.leftAnchor.constraint(equalTo: khanepaniScrollView.leftAnchor, constant: 36).isActive=true
        totalDueAmount.widthAnchor.constraint(equalToConstant: 261).isActive=true
        totalDueAmount.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        more.topAnchor.constraint(equalTo: totalDueAmount.bottomAnchor, constant: 70).isActive=true
        more.trailingAnchor.constraint(equalTo: khanepaniScrollView.trailingAnchor, constant: -32).isActive=true
        more.widthAnchor.constraint(equalToConstant: 78).isActive=true
        more.heightAnchor.constraint(equalToConstant: 33).isActive=true
        
        
        
        Rupee.trailingAnchor.constraint(equalTo: Amount.leadingAnchor, constant: -10).isActive=true
//        Rupee.centerXAnchor.constraint(equalTo: Amount.centerXAnchor, constant: 16).isActive=true
        Rupee.topAnchor.constraint(equalTo: Amount.topAnchor, constant: 24).isActive=true
        Rupee.widthAnchor.constraint(equalToConstant: 40).isActive=true
        Rupee.heightAnchor.constraint(equalToConstant: 34).isActive=true
        
        
        
        Amount.topAnchor.constraint(equalTo: more.bottomAnchor, constant: 52).isActive=true
        Amount.leadingAnchor.constraint(equalTo: khanepaniScrollView.leadingAnchor, constant: 94).isActive=true
        Amount.widthAnchor.constraint(equalToConstant: 300).isActive=true
        Amount.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        
        AmountLineLabel.topAnchor.constraint(equalTo: Amount.bottomAnchor, constant: 25).isActive=true
        AmountLineLabel.leadingAnchor.constraint(equalTo: khanepaniScrollView.leadingAnchor, constant: 36).isActive=true
      AmountLineLabel.rightAnchor.constraint(equalTo: khanepaniScrollView.rightAnchor, constant: -36).isActive=true

        AmountLineLabel.widthAnchor.constraint(equalToConstant: 334).isActive=true
        AmountLineLabel.heightAnchor.constraint(equalToConstant: 1).isActive=true
        
        
        
        
        
        proccedToPay.topAnchor.constraint(equalTo: AmountLineLabel.bottomAnchor, constant: 55.0).isActive=true
        proccedToPay.leadingAnchor.constraint(equalTo:khanepaniScrollView.leadingAnchor,constant: 0.0).isActive=true
        proccedToPay.rightAnchor.constraint(equalTo:khanepaniScrollView.rightAnchor,constant: 0.0).isActive=true
        proccedToPay.heightAnchor.constraint(equalTo: proccedToPay.heightAnchor,constant: 400).isActive=true
//        proccedToPay.widthAnchor.constraint(equalTo: proccedToPay.widthAnchor,constant: 414).isActive=true
        proccedToPay.bottomAnchor.constraint(equalTo: khanepaniScrollView.bottomAnchor, constant:0.0).isActive=true
        
        
        
        
        fromAccount.topAnchor.constraint(equalTo: proccedToPay.topAnchor, constant: 33).isActive=true

        fromAccount.leftAnchor.constraint(equalTo: accountInfo.leftAnchor, constant: 0).isActive=true
        fromAccount.widthAnchor.constraint(equalToConstant: 261).isActive=true
        fromAccount.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        
        
        accountInfo.topAnchor.constraint(equalTo: fromAccount.bottomAnchor, constant: 22.0).isActive = true
//        accountInfo.leftAnchor.constraint(equalTo: proccedToPay.leftAnchor, constant: 36).isActive = true
        accountInfo.centerXAnchor.constraint(equalTo: proccedToPay.centerXAnchor).isActive =  true
        accountInfo.widthAnchor.constraint(equalToConstant: proccedToPay.frame.width-60).isActive=true
        accountInfo.heightAnchor.constraint(equalToConstant: 110).isActive=true
        
        
        walletImage.topAnchor.constraint(equalTo: accountInfo.topAnchor, constant: 26).isActive=true
        walletImage.leadingAnchor.constraint(equalTo: accountInfo.leadingAnchor, constant: 20).isActive = true
        
        walletImage.widthAnchor.constraint(equalToConstant: 50).isActive=true
        walletImage.heightAnchor.constraint(equalToConstant: 50).isActive=true
        
        
        KsGoji.topAnchor.constraint(equalTo: accountInfo.topAnchor, constant: 16).isActive=true

        KsGoji.leftAnchor.constraint(equalTo: walletImage.rightAnchor, constant: 14).isActive=true
        KsGoji.widthAnchor.constraint(equalToConstant: 140).isActive=true
        KsGoji.heightAnchor.constraint(equalToConstant: 29).isActive=true
        KsGoji.adjustsFontSizeToFitWidth = true
        
        
        
        pass.topAnchor.constraint(equalTo: KsGoji.bottomAnchor, constant: -3).isActive=true

        pass.leftAnchor.constraint(equalTo: KsGoji.leftAnchor, constant: 0).isActive=true
        pass.widthAnchor.constraint(equalToConstant: 80).isActive=true
        pass.heightAnchor.constraint(equalToConstant: 29).isActive=true
        
        
        ruppesSymbol.topAnchor.constraint(equalTo: pass.bottomAnchor, constant: -3).isActive=true

        ruppesSymbol.leftAnchor.constraint(equalTo: pass.leftAnchor, constant: 0).isActive=true
        ruppesSymbol.widthAnchor.constraint(equalToConstant: 10).isActive=true
        ruppesSymbol.heightAnchor.constraint(equalToConstant: 16).isActive=true
        
        
        acctRuppes.topAnchor.constraint(equalTo: pass.bottomAnchor, constant: -3).isActive=true

        acctRuppes.leftAnchor.constraint(equalTo: ruppesSymbol.rightAnchor, constant: 0).isActive=true
        acctRuppes.widthAnchor.constraint(equalToConstant: 100).isActive=true
        acctRuppes.heightAnchor.constraint(equalToConstant: 16).isActive=true
        
        
        
        
        checkMark.topAnchor.constraint(equalTo: KsGoji.bottomAnchor, constant: 0).isActive=true

        checkMark.rightAnchor.constraint(equalTo: accountInfo.rightAnchor, constant: -25).isActive=true
        checkMark.widthAnchor.constraint(equalToConstant: 47).isActive=true
        checkMark.heightAnchor.constraint(equalToConstant: 27).isActive=true
        
        
        
        
        payButton.topAnchor.constraint(equalTo: accountInfo.bottomAnchor, constant: 34.0).isActive = true
//        payButton.leftAnchor.constraint(equalTo: proccedToPay.leftAnchor, constant: 36).isActive = true
        
        payButton.centerXAnchor.constraint(equalTo: proccedToPay.centerXAnchor).isActive =  true
        payButton.widthAnchor.constraint(equalToConstant: proccedToPay.frame.width-60).isActive=true
        payButton.heightAnchor.constraint(equalToConstant: 60).isActive=true
        
        
        
        
        
        
//        func layoutFormFields(){
//            //accessing the pages in formmodel
//            if let pages : Pages = formModel!.pages?[currentPage-1] as? Pages {
//                for layout : Layouts in (pages.layouts)! {
//                    if layout.layoutColumns == 1 {
//                        layoutSingle(layout: layout)
//
//                }
//
//        }
//
//        }
//
//        }
        
        
        
//        func layoutSingle(layout : Layouts){
//
//
//            for elements : Elements in (layout.elements)!{
//                if elements.displayOnCompletion ?? false {
//                    displayOnCompletionList.add(elements)
//            }
//
//                CustomerName.text = elements.variableId
//                CustomerName.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))
//
//
//        }
//    }
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)


    }
    
    @objc func moreButtonClicked(){
        let story = UIStoryboard(name: "Main", bundle: nil)
        let DueBill = story.instantiateViewController(withIdentifier: "DueBillViewController") as? DueBillViewController
        self.navigationController?.pushViewController(DueBill!, animated: true)
        
    }

}
