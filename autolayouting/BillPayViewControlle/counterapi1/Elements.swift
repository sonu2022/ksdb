/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Elements {
	public var elementId : String?
	public var elementType : String?
	public var subType : String?
	public var inputType : String?
	public var variableId : String?
	public var fontName : String?
	public var fontSize : Int?
	public var elementDisplayName : ElementDisplayName?
	public var elementPlaceHolder : ElementPlaceHolder?
	public var defaultValue : String?
	public var elementValues : Any?
	public var doesSubmit : Bool?
	public var mandatory : Bool?
	public var minCharacters : Int?
	public var maxCharacters : Int?
	public var regexRequired : Bool?
	public var regex : String?
	public var errorMessage : ErrorMessage?
    
    public var primaryData : Bool?
    public var primaryKeyType : String?
    
	public var popupRequried : Bool?
	public var popupLayout : String?
	public var variable : Bool?
    public var displayOnCompletion : Bool?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let elements_list = Elements.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Elements Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Elements]
    {
        var models:[Elements] = []
        for item in array
        {
            models.append(Elements(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let elements = Elements(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Elements Instance.
*/
	required public init?(dictionary: NSDictionary) {

		elementId = dictionary["elementId"] as? String
		elementType = dictionary["elementType"] as? String
		subType = dictionary["subType"] as? String
		inputType = dictionary["inputType"] as? String
		variableId = dictionary["variableId"] as? String
		fontName = dictionary["fontName"] as? String
		fontSize = dictionary["fontSize"] as? Int
		if (dictionary["elementDisplayName"] != nil)
        { elementDisplayName = ElementDisplayName(dictionary: dictionary["elementDisplayName"] as! NSDictionary) }
        if (dictionary["elementPlaceHolder"] != nil && dictionary["elementPlaceHolder"] as! NSObject != NSNull()) { elementPlaceHolder = ElementPlaceHolder(dictionary: dictionary["elementPlaceHolder"] as! NSDictionary) }
		defaultValue = dictionary["defaultValue"] as? String
        elementValues = dictionary["elementValues"] 
		doesSubmit = dictionary["doesSubmit"] as? Bool
		mandatory = dictionary["mandatory"] as? Bool
		minCharacters = dictionary["minCharacters"] as? Int
		maxCharacters = dictionary["maxCharacters"] as? Int
		regexRequired = dictionary["regexRequired"] as? Bool
		regex = dictionary["regex"] as? String
        if (dictionary["errorMessage"] != nil && dictionary["errorMessage"] as! NSObject != NSNull()) { errorMessage = ErrorMessage(dictionary: dictionary["errorMessage"] as! NSDictionary) }
        
        primaryData = dictionary["primaryData"] as? Bool
        primaryKeyType = dictionary["primaryKeyType"] as? String
        
		popupRequried = dictionary["popupRequried"] as? Bool
		popupLayout = dictionary["popupLayout"] as? String
		variable = dictionary["variable"] as? Bool
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.elementId, forKey: "elementId")
		dictionary.setValue(self.elementType, forKey: "elementType")
		dictionary.setValue(self.subType, forKey: "subType")
		dictionary.setValue(self.inputType, forKey: "inputType")
		dictionary.setValue(self.variableId, forKey: "variableId")
		dictionary.setValue(self.fontName, forKey: "fontName")
		dictionary.setValue(self.fontSize, forKey: "fontSize")
		dictionary.setValue(self.elementDisplayName?.dictionaryRepresentation(), forKey: "elementDisplayName")
		dictionary.setValue(self.elementPlaceHolder?.dictionaryRepresentation(), forKey: "elementPlaceHolder")
		dictionary.setValue(self.defaultValue, forKey: "defaultValue")
		dictionary.setValue(self.elementValues, forKey: "elementValues")
		dictionary.setValue(self.doesSubmit, forKey: "doesSubmit")
        
        dictionary.setValue(self.primaryData, forKey: "primaryData")
        dictionary.setValue(self.primaryKeyType, forKey: "primaryKeyType")
        
		dictionary.setValue(self.mandatory, forKey: "mandatory")
		dictionary.setValue(self.minCharacters, forKey: "minCharacters")
		dictionary.setValue(self.maxCharacters, forKey: "maxCharacters")
		dictionary.setValue(self.regexRequired, forKey: "regexRequired")
		dictionary.setValue(self.regex, forKey: "regex")
		dictionary.setValue(self.errorMessage?.dictionaryRepresentation(), forKey: "errorMessage")
		dictionary.setValue(self.popupRequried, forKey: "popupRequried")
		dictionary.setValue(self.popupLayout, forKey: "popupLayout")
		dictionary.setValue(self.variable, forKey: "variable")

		return dictionary
	}

}
