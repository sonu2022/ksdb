//
//  ShowClassesView.swift
//  apirest
//
//  Created by Manam on 11/03/22.
//

import UIKit
import DropDown

@objc protocol SelectCOunterDelegate {
    
    func selectedConterDetails(names: String,values:String)
    
    
}



class ShowCounterView: UIViewController,UISearchBarDelegate,UINavigationControllerDelegate,UISearchDisplayDelegate, UITableViewDataSource,UITableViewDelegate {
 
    

    @IBOutlet var class_table: UITableView!
    @IBOutlet var searchBar: UISearchBar!
   
    
    @IBOutlet var CounterSearchBarView: UIView!
    
    var conterArray:[String] = []
    var filteredArray = NSMutableArray()
    var searchbarValue:String!
    
    var dropDownArray = NSMutableArray()

    
    
    
    var isSearching: Bool? = false
    @objc var delegate:SelectCOunterDelegate? = nil
        override func viewDidLoad() {
        super.viewDidLoad()
            CounterSearchBarView.layer.cornerRadius = 25
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.backgroundColor = UIColor.white
            } else {
                // Fallback on earlier versions
            }
            searchBar.backgroundImage = UIImage()
            searchBar.layer.borderWidth = 0.2
            searchBar.layer.borderColor = UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0).cgColor
            searchBar.layer.cornerRadius = 20
            searchBar.backgroundColor = UIColor.white
            self.class_table.delegate = self
            self.class_table.dataSource = self
            self.searchBar.delegate = self
            self.searchBar.showsCancelButton = true
            filteredArray=dropDownArray

          
            
    }
    
    
    
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    
        
    

   func counterSearchBar(counterSearchBar:UISearchBar, textDidchange searchText:String){
        print("searchText\(searchText)")
//       counterSearchBar(searchText: searchText)

    }
        
    func searchBarSearchButtonClicked(_ counterSearchBar: UISearchBar) {
        print("searchText \(String(describing: counterSearchBar.text))")
       }
    
    func counterSearchBar(_ counterSearchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText == ",searchText)
        
//        filteredArray = dropDownArray.filter({($0.shared.lowercased().contains(searchText.lowercased())) as! Bool})
       

        self.isSearching = true
        class_table.reloadData()
        if searchText == "" || searchText.count == 0 { // rmoving filter when text is empty on search bar
            self.isSearching = false
            class_table.reloadData()

        }

    }
    func searchBarCancelButtonClicked(_ counterSearchBar: UISearchBar) {
        // rmoving filter when cancel button on search bar
        self.isSearching = false
        counterSearchBar.text = ""
        class_table.reloadData()
    }
            


//extension ShowCounterView: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching! {
            return filteredArray.count
        }else{
            return self.dropDownArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")

            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }

        if isSearching! {
            cell?.textLabel?.text = filteredArray[indexPath.row] as! String
        }else{
            cell?.textLabel?.text = dropDownArray[indexPath.row] as! String
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearching! {
        let className = filteredArray[indexPath.row]
            let class_id = filteredArray[indexPath.row]

            self.delegate?.selectedConterDetails(names: className as! String, values: class_id as! String)
        }else{
            let className = dropDownArray[indexPath.row]
            let class_id = dropDownArray[indexPath.row]

            self.delegate?.selectedConterDetails(names: className as! String, values: class_id as! String)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
}
    
