//
//  CustomizedViewController.swift
//  autolayouting
//
//  Created by Manam on 19/05/22.
//

import UIKit
@IBDesignable



class CustomizedViewController: UIView {
    private var cornerValue : CGFloat = 30.0
    let rect : CGRect! = nil
    let topLeftRadius: CGFloat = 0.0
let topRightRadius: CGFloat = 0.0, bottomLeftRadius: CGFloat = 0.0, bottomRightRadius: CGFloat = 0.0
    
    
        private var shapeLayer:CAShapeLayer?
        private func addShape(){
        
            let shapeLayer = CAShapeLayer()
            shapeLayer.path=createPath()
            shapeLayer.strokeColor=UIColor.clear.cgColor
            shapeLayer.fillColor=UIColor.white.cgColor
            shapeLayer.lineWidth=1.0
            
            if let oldShapeLayer = self.shapeLayer{
                self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
            }else{
                self.layer.insertSublayer(shapeLayer, at: 0)
            }
            self.shapeLayer=shapeLayer
            
        }
        
        
        
        override func draw(_ rect:CGRect){
            self.addShape()
            
        }
        
        func createPath()->CGPath{
            let height: CGFloat = 27.0

                        let circleMarginFromBottom: CGFloat = 5.0

                        //let circleRadius: CGFloat = 37.0 - 2.5

                        let path = UIBezierPath()

                        

                        let centerWidth = self.frame.width / 2

                        path.move(to: CGPoint(x: cornerValue, y: 0)) // start top left

                        path.addLine(to: CGPoint(x: (centerWidth - height * 1.5)-15, y: 0)) // the beginning of the trough

                        

                        //Attempt to draw a semi-circle resulted in a bowl shape ;)

                        path.addCurve(to: CGPoint(x: centerWidth, y:  -(height-circleMarginFromBottom)),

                                      controlPoint1: CGPoint(x: (centerWidth - 40), y: 0), controlPoint2: CGPoint(x: centerWidth - 25, y: -(height-circleMarginFromBottom)))

                        

                        path.addCurve(to: CGPoint(x: (centerWidth + height * 1.5)+15, y: 0),

                                      controlPoint1: CGPoint(x: centerWidth + 25, y: -(height-circleMarginFromBottom)), controlPoint2: CGPoint(x: (centerWidth + 40), y: 0))

                        

                        path.addLine(to: CGPoint(x: self.frame.width-cornerValue, y: 0))

                        path.addArc(withCenter: CGPoint(x: self.frame.width-cornerValue, y: cornerValue), radius: cornerValue, startAngle: toRadians(degrees: 270.0), endAngle: toRadians(degrees: 0.0), clockwise: true)

                        

                        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))

                        

                        path.addLine(to: CGPoint(x: 0, y: self.frame.height))

                        

                        path.addArc(withCenter: CGPoint(x: cornerValue, y: cornerValue), radius: cornerValue, startAngle: toRadians(degrees: 180.0), endAngle: toRadians(degrees: 270.0), clockwise: true)

                        path.close()

                        

                        

                        return path.cgPath
           
        }
    
    
    func toRadians(degrees: CGFloat) -> CGFloat {

            return degrees * CGFloat(M_PI) / 180.0

        }
    
    
        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
               guard !clipsToBounds && !isHidden && alpha > 0 else { return nil }
               for member in subviews.reversed() {
                   let subPoint = member.convert(point, from: self)
                   guard let result = member.hitTest(subPoint, with: event) else { continue }
                   return result
               }
               return nil
           }
        
      
        
    

}
