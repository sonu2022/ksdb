//
//  DueBillViewController.swift
//  autolayouting
//
//  Created by Manam on 17/05/22.
//

import UIKit
import TextFieldEffects

class DueBillViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var dueBillsView: UIView!
    
    @IBOutlet var dueBillsLabel: UILabel!
    
    var displayOnCompletionList : NSMutableArray = NSMutableArray()
    @objc var currentPage:Int = 1
    var formModel : BillerBase?
    
    private var duebills:UIView! = {
        let duebills = UIView(frame: CGRect(x: 0, y: 0, width:414, height: 300))
//        duebills.backgroundColor = UIColor(red: 238/255.0, green: 241/255.0, blue: 246/255.0, alpha: 1.0)
        duebills.layer.cornerRadius = 24
        duebills.layer.borderWidth = 0.4
        duebills.layer.borderColor = UIColor(red: 198/255.0, green: 200/255.0, blue: 205/255.0, alpha: 1.0).cgColor
        return duebills
    }()
    
    
    private var currentMonthDue:HoshiTextField! = {
       let currentMonthDue = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        currentMonthDue.placeholderFontScale=0.9
        currentMonthDue.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        currentMonthDue.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        currentMonthDue.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        currentMonthDue.placeholder="Current Month Dues"
        currentMonthDue.tintColor = UIColor.clear
                return currentMonthDue
    }()
    
    private var currentMonthFine:HoshiTextField! = {
       let currentMonthFine = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        currentMonthFine.placeholderFontScale=0.9
        currentMonthFine.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        currentMonthFine.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        currentMonthFine.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        currentMonthFine.placeholder="Current Month Fine"
        currentMonthFine.textAlignment = .right
        currentMonthFine.tintColor = UIColor.clear
                return currentMonthFine
    }()
    
    
    private var currentMonthDiscount:HoshiTextField! = {
       let currentMonthDiscount = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        currentMonthDiscount.placeholderFontScale=0.9
        currentMonthDiscount.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        currentMonthDiscount.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        currentMonthDiscount.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        currentMonthDiscount.placeholder="Current Month Discount"
        currentMonthDiscount.tintColor = UIColor.clear
                return currentMonthDiscount
    }()
    
    
    private var PreviousDues:HoshiTextField! = {
       let PreviousDues = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        PreviousDues.placeholderFontScale=0.9
        PreviousDues.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        PreviousDues.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        PreviousDues.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        PreviousDues.placeholder="Previous Dues"
        PreviousDues.textAlignment = .right
        PreviousDues.tintColor = UIColor.clear
                return PreviousDues
    }()
    
    
    
    
    private var TotalAdvanceAmount:HoshiTextField! = {
       let TotalAdvanceAmount = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        TotalAdvanceAmount.placeholderFontScale=0.9
        TotalAdvanceAmount.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        TotalAdvanceAmount.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        TotalAdvanceAmount.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        TotalAdvanceAmount.placeholder="Total Advance Amount"
        TotalAdvanceAmount.tintColor = UIColor.clear
                return TotalAdvanceAmount
    }()
    
    
    
    
    
    private var ToatalDues:HoshiTextField! = {
       let ToatalDues = HoshiTextField(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        ToatalDues.placeholderFontScale=0.9
        ToatalDues.textColor=UIColor(red: 36/255.0, green: 50/255.0, blue: 67/255.0, alpha: 1.0)
        ToatalDues.placeholderColor=UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
        ToatalDues.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        ToatalDues.placeholder="Toatal Dues"
        ToatalDues.textAlignment = NSTextAlignment.right
        ToatalDues.tintColor = UIColor.clear
                return ToatalDues
    }()
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        
        dueBillsView.layer.cornerRadius = 24.5
        
        dueBillsView.addSubview(duebills)
        duebills.translatesAutoresizingMaskIntoConstraints = false
     
        
        duebills.addSubview(currentMonthDue)
        currentMonthDue.translatesAutoresizingMaskIntoConstraints = false
        
        
        duebills.addSubview(currentMonthFine)
        currentMonthFine.translatesAutoresizingMaskIntoConstraints = false
        
        
        duebills.addSubview(currentMonthDiscount)
        currentMonthDiscount.translatesAutoresizingMaskIntoConstraints = false
        
        
        duebills.addSubview(PreviousDues)
        PreviousDues.translatesAutoresizingMaskIntoConstraints = false
        
        duebills.addSubview(TotalAdvanceAmount)
        TotalAdvanceAmount.translatesAutoresizingMaskIntoConstraints = false
        
        duebills.addSubview(ToatalDues)
        ToatalDues.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        
        
        
        
        
        
        duebills.topAnchor.constraint(equalTo: dueBillsLabel.bottomAnchor, constant: 25).isActive = true
//        accountInfo.leftAnchor.constraint(equalTo: proccedToPay.leftAnchor, constant: 36).isActive = true
        duebills.centerXAnchor.constraint(equalTo: dueBillsView.centerXAnchor).isActive =  true
        duebills.widthAnchor.constraint(equalToConstant:350).isActive=true
        duebills.heightAnchor.constraint(equalToConstant: 250).isActive=true
     
        
        
        
        currentMonthDue.topAnchor.constraint(equalTo: duebills.topAnchor, constant: 24).isActive=true
        currentMonthDue.leadingAnchor.constraint(equalTo: duebills.leadingAnchor, constant: 18).isActive=true
        currentMonthDue.widthAnchor.constraint(equalToConstant: 100).isActive=true
        currentMonthDue.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        
        
        currentMonthFine.topAnchor.constraint(equalTo: duebills.topAnchor, constant: 24).isActive=true
        currentMonthFine.centerXAnchor.constraint(equalTo: currentMonthDue.centerXAnchor, constant: 170).isActive=true
        currentMonthFine.trailingAnchor.constraint(equalTo: duebills.trailingAnchor, constant: -21).isActive = true
        currentMonthFine.widthAnchor.constraint(equalToConstant: 100).isActive=true
        currentMonthFine.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        
        
        
        currentMonthDiscount.topAnchor.constraint(equalTo: currentMonthDue.bottomAnchor, constant: 40).isActive=true
        currentMonthDiscount.leadingAnchor.constraint(equalTo: duebills.leadingAnchor, constant: 18).isActive=true
        currentMonthDiscount.widthAnchor.constraint(equalToConstant: 100).isActive=true
        currentMonthDiscount.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        PreviousDues.topAnchor.constraint(equalTo: currentMonthFine.bottomAnchor, constant: 40).isActive=true
        PreviousDues.centerXAnchor.constraint(equalTo: currentMonthDiscount.centerXAnchor, constant: 160).isActive=true
        PreviousDues.trailingAnchor.constraint(equalTo: duebills.trailingAnchor, constant: -21).isActive=true
        PreviousDues.widthAnchor.constraint(equalToConstant: 100).isActive=true
        PreviousDues.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        TotalAdvanceAmount.topAnchor.constraint(equalTo: currentMonthDiscount.bottomAnchor, constant: 43).isActive=true
        TotalAdvanceAmount.leadingAnchor.constraint(equalTo: duebills.leadingAnchor, constant: 18).isActive=true
        TotalAdvanceAmount.widthAnchor.constraint(equalToConstant: 100).isActive=true
        TotalAdvanceAmount.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
        
        ToatalDues.topAnchor.constraint(equalTo: PreviousDues.bottomAnchor, constant: 43).isActive=true
        ToatalDues.trailingAnchor.constraint(equalTo: duebills.trailingAnchor, constant: -21).isActive=true
        ToatalDues.centerXAnchor.constraint(equalTo: TotalAdvanceAmount.centerXAnchor, constant: 160).isActive=true

        ToatalDues.widthAnchor.constraint(equalToConstant: 100).isActive=true
        ToatalDues.heightAnchor.constraint(equalToConstant: 40).isActive=true
        
         
        
        
    }
    

  
 
    
    
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

   
}
