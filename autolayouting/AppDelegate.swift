//
//  AppDelegate.swift
//  autolayouting
//
//  Created by Manam on 22/03/22.
//

import UIKit
import UserNotifications
import Realm
import RealmSwift
import IQKeyboardManagerSwift


@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    var window: UIWindow?
    var badgeCount = 0

    let realm = try! Realm()
    
    let order = Order()
    var objectNotificationToken: NotificationToken?
    let table = TableViewCell()

    
    



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

//        UNUserNotificationCenter.current().delegate = self
//        IQKeyboardManager.shared.enable = true


        askForPushNotification()

        //justadded now
//        let tabBarController = UITabBarController()
//        tabBarController.tabBar.unselectedItemTintColor = .systemBlue
//        UITabBar.appearance().unselectedItemTintColor = .systemBlue
        
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1)  {
                    var nextID = 0
                    migration.enumerateObjects(ofType: Order.className()) { oldObject, newObject in
                        newObject!["name"] = nextID
                        nextID += 1
                    }
                }
            }
        )
        Realm.Configuration.defaultConfiguration = config
        
        
        UIApplication.shared.registerForRemoteNotifications()
        
        
        
//        if UserDefaults.standard.object(forKey: "Language") != nil && UserDefaults.standard.object(forKey: "Language") as! String == "hi"
//        {
//            UserDefaults.standard.set("hi", forKey: "Language")
//        }
//        else
//        {
//            UserDefaults.standard.set("en", forKey: "Language")
//        }
    
        
        
        // the change language section//
        
        
        
        

        return true
    }

    
//    func languageWillChange(notification:NSNotification){
//           let targetLang = notification.object as! String
//        UserDefaults.standard.set(targetLang, forKey: "selectedLanguage")
//        Bundle.setLanguage(targetLang)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE") , object: targetLang)
//       }
//    
    
    
    
    

    
   

    
    func askForPushNotification() {

            let center = UNUserNotificationCenter.current()

            center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in

                if granted{
                    UNUserNotificationCenter.current().delegate = self

                }
                
                
                // Enable or disable features based on authorization.

            }
     }

    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Push notification received in foreground\(notification.request.content.userInfo)")
//        let userInfo = notification.request.content.userInfo
//        print(userInfo["aps"])
        let userInfo = notification.request.content.userInfo

            guard
                let aps = userInfo[AnyHashable("aps")] as? NSDictionary,
                let alert = aps["alert"] as? NSDictionary,
                let body = alert["body"] as? String,
                let title = alert["title"] as? String
                else {
                    // handle any error here
                    return
                }

            print("Title: \(title) \nBody:\(body)")

       
                try! realm.write {
                    order.name = title
                     order.subtitle = body
                    
                    print("realm answer is \(order.name)")
                    print("realm answer is \(order.subtitle)")

                    
                    realm.add(order)
                    
                        
                }
        
        
        
        
        
        completionHandler([.alert, .sound, .badge])
        
        
    }

    
    
    
    
    
    
    
    func incrementBadgeNumberBy(badgeNumberIncrement: Int)
    {
        let currentBadgeNumber = UIApplication.shared.applicationIconBadgeNumber
        let updatedBadgeNumber = currentBadgeNumber + badgeNumberIncrement
        if (updatedBadgeNumber > 0)
        {
            UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
        }
        else
        {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    
    
    
    
    func application(
      _ application: UIApplication,
      didReceiveRemoteNotification userInfo: [AnyHashable : Any],
      fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

          print("Recived: \(userInfo)")
          NotificationCenter.default.post(name:NSNotification.Name(rawValue: "ReceivedNotifications") , object: nil, userInfo: userInfo)

          //just added with notif
          NotificationCenter.default.post(name: NSNotification.Name("TestNotification"),object: nil)

          
                completionHandler(.newData)
          
          
      }
    
    
    
    
    
    
    
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//
////        let aps = userInfo[AnyHashable("aps")]! as! NSDictionary
//
//
//        print("Recived: \(userInfo)")
//
//        completionHandler(.newData)
//
////                let msg = userInfo[mutableSetValue(forKey: "aps")] as? String
////               print("GET NOTIFICATION, userInfo: \(userInfo)")
////
////               completionHandler(.newData)
//
//
//
//
//
//
//
//
//
//
//
////        let msg = userInfo[mutableSetValue(forKey: "aps")] as? String
////
////
////
////
////
////        print("mesaage   :\(msg)")
////        print("Message:\(String(describing: msg))")
////
////        UserDefaults.standard.set(msg, forKey: "ReceivedNotifications")
//////        UserDefaults.standard.string(forKey:"ReceivedNotifications")
////
////
////
////        print("message is for : \(String(describing: msg))")
////
//        //This is how you save object in NSUserDefaults
//
//    }
    
    
    
    
    
    
    
    
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
//        // Print notification payload data
//
//        badgeCount = badgeCount + 1
//        self.incrementBadgeNumberBy(badgeNumberIncrement: badgeCount)
//
//        print("Push notification received response: \(data)")
//
//    }
//
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {

        let userInfo = response.notification.request.content.userInfo
        if UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive {
                    NotificationCenter.default.post( name: NSNotification.Name(rawValue: "ReceivedNotifications"), object: nil, userInfo:userInfo)
            
            
        }
            
        print("Push notification received response: \(userInfo)")

            completionHandler()
            
         }

    
  
    

    
    
    
    

    // MARK: UISceneSession Lifecycle

        @available(iOS 13.0, *)
        func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

        @available(iOS 13.0, *)
        func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


   

   









   


    }





//firebase below



    // Mark : UNUserNotificationCenter methods

    // method 1- Will register app on apns to receieve token
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
////        print(tokenString(deviceToken)) // this method will convert token "Data" to string format
//    }
//
//    // Method: 2 - Failed registration. Explain why.
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("Failed to register for remote notification :\(error.localizedDescription)")
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        print("will get called when the apps is in the foreground and we want to show banner")
//
//        completionHandler([.alert, .sound, .badge])
//    }
//
//    // TRIGGERED when an user taps on an notification
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("will get called when user taps on notification")
//        completionHandler()
//    }

//extension Bundle {
//    class func setLanguage(_ language: String?) {
//    }
//}











