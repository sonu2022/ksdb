//
//  MyQRCodeViewController.swift
//  autolayouting
//
//  Created by Manam on 13/07/22.
//

import UIKit
import AVFoundation

class MyQRCodeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,AVCaptureMetadataOutputObjectsDelegate {
    
    

    @IBOutlet var shareView: UIView!
    
    @IBOutlet var RecieveMoneyView: UIView!
    
    
    @IBOutlet var qrtableView: UITableView!
    
    
    
    @IBOutlet var Qrcodemainlabel: UILabel!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shareView.layer.cornerRadius = 11
        shareView.layer.borderWidth = 1.2
        shareView.layer.borderColor = UIColor(red: 220/255.0, green: 228/255.0, blue: 243/255.0, alpha: 0.66).cgColor
        
        
        RecieveMoneyView.layer.cornerRadius = 28
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            print("Failed to get the camera device")
            return
        }
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        var error:NSError?
        
//        let input: AnyObject! = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice ?? "")

        let input: AVCaptureDeviceInput
               
        
        
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
        } catch {
        return
        }
        if(error != nil){
            // If any error occurs, simply log the description of it and don't continue any more.

            print("\(String(describing: error?.localizedDescription))")
            return
        }
        // Initialize the captureSession object.

        captureSession = AVCaptureSession()
    
        // Set the input device on the capture session.

        captureSession?.addInput(input as AVCaptureInput)
        
        //intialize a capturemetadataoutput and set it to the output device to the capture session.
        let captureMetaDataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetaDataOutput)
        
        // Set delegate and use the default dispatch queue to execute the call back
        //self is set as delegate so that we can use captureMetadataOutput
        
        captureMetaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetaDataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        

        
        //intialize the videopreview layer and add it to the view
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        
//     to the video capture by calling the startRunning method of the capture
        captureSession?.startRunning()
        
        // Move the message label to the top view
//      //  view.bringSubviewToFront(qrlab)
        
        
        
        
        
        ///////read the qr code//////////
        
//        The UIView is invisible on screen as the size of the qrCodeFrameView object is set to zero by default
        qrCodeFrameView = UIView()
        qrCodeFrameView?.layer.borderColor = UIColor.darkGray.cgColor
        qrCodeFrameView?.layer.borderWidth = 2
        view.addSubview(qrCodeFrameView!)
    
        view.bringSubviewToFront(qrCodeFrameView!)
        
        
        
        
        
        
        
        
        
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!,didOutputMetadataObjects metadataObjects:[AnyObject]!,fromConnection connection: AVCaptureConnection!){
        
        if metadataObjects == nil || metadataObjects.count == 0{
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR code is detected")
            return
        }
//        An AVMetadataMachineReadableCodeObject is a single detected      machine readable code in an image
        let metaDataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metaDataObj.type == AVMetadataObject.ObjectType.qr {
           //if the found meta data is equal to the qr code then update status label
            
            
            
            //by calling the transforedmetadataobject of viewpreview layer the meatadata object's visual proprties is converted to layer coordinates
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metaDataObj as AVMetadataMachineReadableCodeObject)  as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds
            
            
            if metaDataObj.stringValue != nil
               {
                Qrcodemainlabel.text = metaDataObj.stringValue
                
               }
        }
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        
    }
    

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target:nil , action: nil)
        
        

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let view = qrtableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! QrTableViewCell
        return view
    }

}




//To perform a real time capture we need to instantiate an AVCaptureSession object with input set to the AVcapturedevice for video capturing


// AVCaptureSession object is used to coordinate the flow of data from the video input device to outputs

// AVCaptureMetaDataOutput class is the core part of QR code reading.


//AVCaptureMetadataOutputObjectsDelegate protocol is used to intercept any metadata found in the input device (that is the QR code captured by the device’s camera) and translate it to a human readable format.


//display the video captured by the device’s camera on screen. This can be done using a AVCaptureVideoPreviewLayer, which actually is a CALayer



//metadataObjects of the method is an Array object, which contains all the metadata objects have been read
