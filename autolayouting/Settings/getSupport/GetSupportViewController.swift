//
//  GetSupportViewController.swift
//  autolayouting
//
//  Created by Manam on 06/07/22.
//

import UIKit

class GetSupportViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   
    
    
    
    @IBOutlet var QueriesReleatedCollectionView: UICollectionView!
    
    
    
    
    
    var queriesReleatedCollectionView = ["Wallet","Banking","Bill Payments","Bookings"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationItem.setHidesBackButton(true, animated: true)

        
        
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! GetSupportCollectionViewCell
        
        let queriesReleated = queriesReleatedCollectionView[indexPath.row]
        cell.Label.text = queriesReleated
        cell.contentView.layer.borderWidth = 2.0
        cell.contentView.layer.cornerRadius = 12
        cell.contentView.layer.borderColor = UIColor(red: 238/255.0, green: 241/255.0, blue: 246/255.0, alpha: 1.0).cgColor
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 4
//
                  let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                  let totalSpace = flowLayout.sectionInset.left
                      + flowLayout.sectionInset.right
                      + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 3))

                  let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

               return CGSize(width: size, height: 50)
    
    
    }
    
    
}
