////
////  File.swift
////  autolayouting
////
////  Created by Manam on 29/06/22.
////
//
//import UIKit
//
//
//private let appleLanguagesKey = "AppleLanguages"
//
//
//enum Language: String {
//    
//    case english = "en"
//    case hindi = "hi"
//    
//    
//    
//    
//    static var language: Language {
//        get {
//            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
//                let language = Language(rawValue: languageCode) {
//                return language
//            } else {
//                let preferredLanguage = NSLocale.preferredLanguages[0] as String
//                let index = preferredLanguage.index(
//                    preferredLanguage.startIndex,
//                    offsetBy: 2
//                )
//                guard let localization = Language(
//                    rawValue: preferredLanguage.substring(to: index)
//                    ) else {
//                        return Language.english
//                }
//                
//                return localization
//            }
//        }
//        set {
//            guard language != newValue else {
//                return
//            }
//            
//            //change language in the app
//            //the language will be changed after restart
//            UserDefaults.standard.set([newValue.rawValue], forKey: appleLanguagesKey)
//            UserDefaults.standard.synchronize()
//            
//            
//            
//            //initialize the app from scratch
//            //show initial view controller
//            //so it seems like the is restarted
//            //NOTE: do not localize storboards
//            //After the app restart all labels/images will be set
//            //see extension String below
//            UIApplication.shared.windows[0].rootViewController = UIStoryboard(
//                name: "Main",
//                bundle: nil
//                ).instantiateInitialViewController()
//        }
//    }
//}
//
//
////extension String{
//////    func LocalizableString(loc:String)->String{
//////        //to get the path to language
//////        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
//////        let bundle = Bundle(path: path!)
//////        //Return localizabe string using main bundle if one is not specified
//////        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: " ")
//////
//////    }
////    
////    var localized: String {
////        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
////    }
////
////}
//
//extension Bundle {
//    //Here magic happens
//    //when you localize resources: for instance Localizable.strings, images
//    //it creates different bundles
//    //we take appropriate bundle according to language
//    static var localizedBundle: Bundle {
//        let languageCode = Language.language.rawValue
//        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
//            return Bundle.main
//        }
//        return Bundle(path: path)!
//    }
//}
