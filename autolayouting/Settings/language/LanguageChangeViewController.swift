//
//  LanguageChangeViewController.swift
//  autolayouting
//
//  Created by Manam on 22/06/22.
//

import UIKit


protocol ChangeLanguageDelegate: AnyObject {
    func changeLanguageHindi()
    func changeLanguageEnglish() 
    
}




class LanguageChangeViewController: UIViewController {
    
    
//    weak var langugeManager: ChangeLanguageDelegate?
    
    weak var langugeManager: ChangeLanguageDelegate?
    {
    didSet{
        

            self.langugeManager?.changeLanguageEnglish()
        
    
            self.langugeManager?.changeLanguageHindi()
    

    }

    }

    
    
    
    @IBOutlet var changeLanguageButton: UIButton!
    
    @IBOutlet var changeLanguaugeLabel: UILabel!
    
    @IBOutlet var englishView: UIView!
    
    @IBOutlet var englishTickBox: UIButton!
    
    
    @IBOutlet var englishButton: UIButton!
    
    @IBOutlet var backButtonOutlet: UIButton!
    
    @IBOutlet var hindiView: UIView!
    
    @IBOutlet var hindiTickBox: UIButton!
    
    @IBOutlet var hindiButton: UIButton!
    
    
    var lang:LanguageTableViewCell?

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        englishTickBox.backgroundColor = UIColor.white
        englishTickBox.layer.borderWidth = 1.5
        englishTickBox.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
        englishTickBox.layer.cornerRadius = 14
        englishView.layer.borderWidth = 1.5
        englishView.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
        englishView.layer.cornerRadius = 12
        
        
        
        
        hindiTickBox.backgroundColor = UIColor.white
        hindiTickBox.layer.borderWidth = 1.5
        hindiTickBox.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
        hindiTickBox.layer.cornerRadius = 14
        hindiView.layer.borderWidth = 1.5
        hindiView.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
        hindiView.layer.cornerRadius = 12
        
        
        
        
        changeLanguageButton.layer.cornerRadius = 16
        

       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: true)
    
        

    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//            super.viewWillDisappear(animated)
//        if self.isBeingDismissed {
//                self.langugeManager?.changeLanguage(true)
//            
//            }
//        }
    

    

    
    
    
    @IBAction func backButton(_ sender: UIButton) {
        print("You tapped the button")
    
        self.navigationController?.popViewController(animated: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)


    
    }
    
   
    
    @IBAction func englishButtonAction(_ sender: Any) {
        
        
        
            englishView.layer.borderWidth = 2.5
            englishView.layer.borderColor = UIColor(red: 3/255.0, green: 49/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            englishTickBox.backgroundColor = UIColor(red: 3/255.0, green: 49/255.0, blue: 67/255.0, alpha: 1.0)
            englishTickBox.setImage(UIImage(named: "check-2"), for: .normal)

            hindiTickBox.backgroundColor = UIColor.white
            hindiTickBox.layer.borderWidth = 1.5
            hindiTickBox.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor

            
            hindiView.layer.borderWidth = 1.5
            hindiView.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
            
            
            lang?.ChangePinLabel.text = "Change Pin".LocalizableString(loc: "en")
            changeLanguaugeLabel.text = "Change Language".LocalizableString(loc: "en")
        
//        langugeManager?.changeLanguageEnglish()

        
    }
    
    
    @IBAction func hindiButtonAction(_ sender: Any) {
        
        
        
            hindiView.layer.borderWidth = 2.5
            hindiView.layer.borderColor = UIColor(red: 3/255.0, green: 49/255.0, blue: 67/255.0, alpha: 1.0).cgColor
            hindiTickBox.backgroundColor = UIColor(red: 3/255.0, green: 49/255.0, blue: 67/255.0, alpha: 1.0)
    
            hindiTickBox.setImage(UIImage(named: "check-2"), for: .normal)

            lang?.ChangePinLabel.text = "Change Pin".LocalizableString(loc: "hi")
            lang?.logoutLabel.text = "Logout".LocalizableString(loc: "hi")
            changeLanguaugeLabel.text = "Change Language".LocalizableString(loc: "hi")
            
            englishTickBox.backgroundColor = UIColor.white
            englishTickBox.layer.borderWidth = 1.6
            englishTickBox.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
            
            englishView.layer.borderWidth = 1.6
        englishView.layer.borderColor = UIColor(red: 207/255.0, green:  207/255.0, blue:  207/255.0, alpha: 1.0).cgColor
     
      
//        langugeManager?.changeLanguageHindi()
//
        
    }
    
    
    

    
    
    
    
    
    @IBAction func changeLanguageActionManager(_ sender: Any) {

        if hindiButton?.isSelected ?? false && changeLanguageButton.isTouchInside{
                    
        langugeManager?.changeLanguageHindi()
            
        }
        
        
        else if englishButton?.isSelected ?? false && changeLanguageButton.isTouchInside{
            langugeManager?.changeLanguageEnglish()

        }
        
        self.navigationController?.popViewController(animated: true)


        
        
        
//        else{
//            langugeManager?.changeLanguageEnglish(true)
//            self.navigationController?.popViewController(animated: true)
//        }

//        switch changeLanguageButton{
//    case  hindiButton.isTouchInside:
//            langugeManager?.changeLanguageHindi(true)
//          self.navigationController?.popViewController(animated: true)
//            break
//
//        case englishButton.isSelected:
//            langugeManager?.changeLanguageEnglish(true)
//            self.navigationController?.popViewController(animated: true)
//            break
//
//        default:
//            print("Error")
//        }
        
            
            
        
        
    }
    
    

    
    
    
    
    

}
extension String{
    func LocalizableString(loc:String)->String{
        //to get the path to language
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        //Return localizabe string using main bundle if one is not specified
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: " ")
        
    }
}
