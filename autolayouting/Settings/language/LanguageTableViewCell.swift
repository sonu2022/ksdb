//
//  LanguageTableViewCell.swift
//  autolayouting
//
//  Created by Manam on 22/06/22.
//

import UIKit

class LanguageTableViewCell: UITableViewCell, ChangeLanguageDelegate {
   
    
    
    
    @IBOutlet var TouchLabel: UILabel!
    
    
    @IBOutlet var languageSettinglabel: UILabel!
    
    
    @IBOutlet var ChangePinLabel: UILabel!
    
    
    @IBOutlet var getSupportLabel: UILabel!
    
    
    
    @IBOutlet var logoutLabel: UILabel!
    
    var lang:LanguageChangeViewController = LanguageChangeViewController()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lang.langugeManager = self
        
        
        
        
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)



    }
    
    
//    //called for delegation purpose
      func prepare(for segue: UIStoryboardSegue!, sender: AnyObject!) {
          if(segue.identifier == "changelanguage"){
                  let displayVC = segue.destination as! LanguageChangeViewController
                  displayVC.langugeManager = self
              
          }
      }
    
//    func prepare(for segue: UIStoryboardSegue!, sender: AnyObject!) {
//        guard
//                   let nav = segue.destination as? UINavigationController,
//                   let classBVC = nav.topViewController as? LanguageChangeViewController
//               else { return }
//               classBVC.langugeManager = self
//           }
    
    
    func changeLanguageHindi() {
        


        languageSettinglabel?.text = "Language".LocalizableString(loc: "hi")

        logoutLabel?.text = "Logout".LocalizableString(loc: "hi")
//
        getSupportLabel?.text = "Get Support".LocalizableString(loc: "hi")
        
        ChangePinLabel?.text = "Change Pin".LocalizableString(loc: "hi")
//        languageSettinglabel.text = "Language".LocalizableString(loc: "hi")
//        logoutLabel.text = "Logout".LocalizableString(loc: "hi")
//        getSupportLabel.text = "Get Support".LocalizableString(loc:"hi")
//        TouchLabel.text = "Face".LocalizableString(loc:"hi")

    }
    
    func changeLanguageEnglish() {
        
        
        languageSettinglabel?.text = "Language".LocalizableString(loc: "en")
        logoutLabel?.text = "Logout".LocalizableString(loc: "en")
        getSupportLabel?.text  = "Get Support".LocalizableString(loc: "en")
        ChangePinLabel?.text = "Change Pin".LocalizableString(loc: "en")
        


    }
  
    
//    func changeLanguage(_ change:Bool) {
//
//
//
//        print("The value the answerr is")
//
////         TouchLabel.text = "Edit".LocalizableString(loc: "hi")
////         languageSettinglabel.text = "Edit".LocalizableString(loc: "hi")
//
////         ChangePinLabel.text = "Change Pin".LocalizableString(loc: "hi")
//
////         getSupportLabel.text = "Edit".LocalizableString(loc: "hi")
//
//
////         logoutLabel.text = "Edit".LocalizableString(loc: "hi")
//
//
//    }
    
    

}
//extension String{
//    func LocalizableString(loc:String)->String{
//        //to get the path to language
//        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
//        let bundle = Bundle(path: path!)
//        //Return localizabe string using main bundle if one is not specified
//        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: " ")
//
//    }
//}
