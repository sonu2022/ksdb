//
//  LanguageViewController.swift
//  autolayouting
//
//  Created by Manam on 21/06/22.
//

import UIKit

class LanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ChangeLanguageDelegate {
       
    
   
    var lang:LanguageChangeViewController = LanguageChangeViewController()
    var lang2:LanguageTableViewCell?
    
    
    
   
    
    @IBOutlet var editLabel: UILabel!
    
    
    
    @IBOutlet var mailLabel: UILabel!
    
    @IBOutlet var userDefaultLabel: UILabel!
    
    
    @IBOutlet var tableView: UITableView!
    
    
    
    
    private var blueView: UIView = {
        let blueView =  UIView(frame: CGRect(x: 0, y: 0, width:414, height: 600))
        blueView.backgroundColor = UIColor(red: 3/255.0, green: 46/255.0, blue: 77/255.0, alpha: 1.0)
        return blueView
    }()
    
    private var QrView: UIView = {
        let QrView =  UIView(frame: CGRect(x: 0, y: 0, width:312, height: 60))
        QrView.backgroundColor = UIColor.white
        return QrView
    }()
    
    
    private var QrButton: UIButton = {
        let QrButton =  UIButton(frame: CGRect(x: 0, y: 0, width:312, height: 60))
        QrButton.backgroundColor = UIColor.clear
        QrButton.addTarget(self, action: #selector(qrButtonClicked), for:.touchUpInside)
        return QrButton
    }()
    
    
    private var qrCode:UIImageView! = {
       let qrCode = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 20, height: 20))

        qrCode.image = UIImage(named: "qr-code-6.png")

        return qrCode
    }()
    

    
    private var qrLabel:UILabel! = {
       let qrLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 28))
        qrLabel.textColor = UIColor(red: 3/255.0, green: 46/255.0, blue: 77/255.0, alpha: 1.0)
        qrLabel.text = "My QR Code"

        qrLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)

        qrLabel.tintColor = UIColor.black
                return qrLabel
        
            }()
    

    
    
    @IBOutlet var languageButton: UIButton!
    

    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(blueView)
        blueView.translatesAutoresizingMaskIntoConstraints = false
        
        blueView.topAnchor.constraint(equalTo: mailLabel.bottomAnchor, constant: 70).isActive = true
        blueView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0).isActive = true
        blueView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0).isActive = true
        blueView.heightAnchor.constraint(equalToConstant: 600).isActive = true
        blueView.clipsToBounds = true
        blueView.layer.cornerRadius = 34
        blueView.layer.maskedCorners = [.layerMinXMinYCorner]
        
    
        
        
        
        
        
        
        
        blueView.addSubview(QrView)
        QrView.translatesAutoresizingMaskIntoConstraints = false
        
        QrView.topAnchor.constraint(equalTo: blueView.topAnchor, constant: 45).isActive = true
        QrView.centerXAnchor.constraint(equalTo: blueView.centerXAnchor).isActive = true
        QrView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        QrView.widthAnchor.constraint(equalToConstant: 360).isActive = true

        QrView.layer.cornerRadius = 24
        
        
        
        
        QrView.addSubview(qrCode)
        qrCode.translatesAutoresizingMaskIntoConstraints = false
        
        qrCode.leadingAnchor.constraint(equalTo: QrView.leadingAnchor, constant: 94).isActive = true
        qrCode.centerYAnchor.constraint(equalTo: QrView.centerYAnchor).isActive = true
        qrCode.heightAnchor.constraint(equalToConstant: 20).isActive = true
        qrCode.widthAnchor.constraint(equalToConstant: 20).isActive = true

        
        QrView.addSubview(qrLabel)
        qrLabel.translatesAutoresizingMaskIntoConstraints = false
        
        qrLabel.centerYAnchor.constraint(equalTo: QrView.centerYAnchor).isActive = true
        qrLabel.centerXAnchor.constraint(equalTo: QrView.centerXAnchor).isActive = true
        
        
        
        
        blueView.addSubview(QrButton)
        QrButton.translatesAutoresizingMaskIntoConstraints = false
        QrButton.topAnchor.constraint(equalTo: QrView.topAnchor, constant:0).isActive = true
        QrButton.bottomAnchor.constraint(equalTo: QrView.bottomAnchor, constant: 0).isActive = true
        QrButton.leadingAnchor.constraint(equalTo:QrView.leadingAnchor,constant: 0).isActive = true
        QrButton.trailingAnchor.constraint(equalTo:QrView.trailingAnchor, constant: 0 ).isActive = true
        
        
        
        

        
        blueView.addSubview(tableView)
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.backgroundColor = UIColor(red: 240/255.0, green: 243/255.0, blue: 248/255.0, alpha: 1.0)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.layer.cornerRadius = 34
        tableView.layer.maskedCorners = [.layerMinXMinYCorner]

        
        
        lang.langugeManager = self
        
        
//        editLabel.text = "Edit".LocalizableString(loc: "hi")

    }
    
    
    
    @objc func qrButtonClicked(){
        
        let story = UIStoryboard(name: "Profile", bundle: nil)

        let settings = story.instantiateViewController(withIdentifier: "MyQRCodeViewController") as? MyQRCodeViewController
        self.navigationController?.pushViewController(settings!, animated: true)
        
        
        
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationItem.setHidesBackButton(true, animated: true)

        
        
    }
    
          

    
   
    
    

    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        let customBackButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
                    navigationItem.backBarButtonItem = customBackButton
   
    }
    
    
    @IBAction func languageButtonAction(_ sender: Any) {
        
        let story = UIStoryboard(name: "Profile", bundle: nil)

        let settings = story.instantiateViewController(withIdentifier: "changelanguage") as? LanguageChangeViewController
        self.navigationController?.pushViewController(settings!, animated: true)

        
        
    }
    
    
    
    
    @IBAction func getSupportAction(_ sender: Any) {
        let story = UIStoryboard(name: "Profile", bundle: nil)

        let settings = story.instantiateViewController(withIdentifier: "GetSupportViewController") as? GetSupportViewController
        self.navigationController?.pushViewController(settings!, animated: true)
        
        
    
        
    }
    
    
    
    
    
    
    
    
    
    
    
    //called for delegation purpose
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//              if let vc2 = segue.destination as? LanguageChangeViewController{
//                  vc2.langugeManager = self
//              }
//          }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "changelanguage"){
                let displayVC = segue.destination as! LanguageChangeViewController
                displayVC.langugeManager = self
        }
    }
    
    
    
    func changeLanguageHindi() {
        
        
        
//        let indexPath = IndexPath()

        
        
//        let view = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LanguageTableViewCell

//        view.TouchLabel.text = "Face ID".LocalizableString(loc: "hi")
        
        
        
        editLabel?.text = "Edit".LocalizableString(loc: "hi")
        
        
        
//        view.logoutLabel.text = "Logout".LocalizableString(loc: "hi")
//        view.languageSettinglabel.text = "Language".LocalizableString(loc: "hi")
        
        
//        lang2?.logoutLabel.text = "Logout".LocalizableString(loc: "hi")

        dismiss(animated: true,completion: nil)

    }
    
    
    func changeLanguageEnglish() {
        editLabel.text = "Edit".LocalizableString(loc: "en")

    }
    
    
    
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let view = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! LanguageTableViewCell
//        if view.isSelected{
//            let story = UIStoryboard(name: "Main", bundle: nil)
//            let DueBill = story.instantiateViewController(withIdentifier: "changelanguage") as? LanguageChangeViewController
//            self.navigationController?.pushViewController(DueBill!, animated: true)
//
//        }
//
//    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return 5
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        

        let view = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LanguageTableViewCell
        
        
         if indexPath.row == 1{
            let view = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! LanguageTableViewCell
//             view.languageSettinglabel.text = "Language".LocalizableString(loc: "hi")
             return view
        }
        else  if indexPath.row == 2{

            let view = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! LanguageTableViewCell
//            view.ChangePinLabel.text = "Change Pin".LocalizableString(loc: "hi")
            
            return view

        }
        else  if indexPath.row == 3{
            let view = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! LanguageTableViewCell
//            view.getSupportLabel.text = "Get Support".LocalizableString(loc: "hi")
            return view

        }
        else  if indexPath.row == 4{
            let view = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! LanguageTableViewCell
//            view.logoutLabel.text = "Logout".LocalizableString(loc: "hi")
            
            

            return view

        }
        
        
        
        return view
        
    }
    
    
    
   
    
    
    
    
    
    
    
    
    
}



