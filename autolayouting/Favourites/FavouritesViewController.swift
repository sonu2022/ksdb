//
//  FavouritesViewController.swift
//  autolayouting
//
//  Created by Manam on 07/07/22.
//

import UIKit

class FavouritesViewController: UIViewController,ChangeLanguageDelegate {
   
    @IBOutlet var noFavouritesYet: UILabel!
    
    @IBOutlet var theNextOpt: UILabel!
    @IBOutlet var favouriteViewController: UIView!
    
    
    
    
    
    var lang:LanguageChangeViewController = LanguageChangeViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        favouriteViewController.layer.cornerRadius = 24

        lang.langugeManager = self
        
    }
    
    
    //called for delegation purpose
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
              if let vc2 = segue.destination as? LanguageChangeViewController{
                  vc2.langugeManager = self
              }
          }
    
    
    
    
    func changeLanguageHindi() {
        noFavouritesYet.text = "No Favourites".LocalizableString(loc: "hi")
        theNextOpt.text = "Below Text For the favourites".LocalizableString(loc: "hi")
    }
    
    func changeLanguageEnglish() {
        noFavouritesYet.text = "No Favourites".LocalizableString(loc: "en")
        theNextOpt.text = "Below Text For the favourites".LocalizableString(loc: "en")
    }
    
  

}
