/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class DisplayForm {
	public var billerId : String?
	public var billerName : String?
	public var billerParamName : String?
	public var accountRequestParamName : String?
	public var accountServiceName : String?
	public var walletServiceName : String?
	public var validationMsg : String?
	public var successMsg : String?
	public var needPresentment : Bool?
	public var pages : Array<Pagess>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let displayForm_list = DisplayForm.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of DisplayForm Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [DisplayForm]
    {
        var models:[DisplayForm] = []
        for item in array
        {
            models.append(DisplayForm(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let displayForm = DisplayForm(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: DisplayForm Instance.
*/
	required public init?(dictionary: NSDictionary) {

		billerId = dictionary["billerId"] as? String
		billerName = dictionary["billerName"] as? String
		billerParamName = dictionary["billerParamName"] as? String
		accountRequestParamName = dictionary["accountRequestParamName"] as? String
		accountServiceName = dictionary["accountServiceName"] as? String
		walletServiceName = dictionary["walletServiceName"] as? String
		validationMsg = dictionary["validationMsg"] as? String
		successMsg = dictionary["successMsg"] as? String
		needPresentment = dictionary["needPresentment"] as? Bool
        if (dictionary["pages"] != nil) { pages = Pagess.modelsFromDictionaryArray(array: dictionary["pages"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.billerId, forKey: "billerId")
		dictionary.setValue(self.billerName, forKey: "billerName")
		dictionary.setValue(self.billerParamName, forKey: "billerParamName")
		dictionary.setValue(self.accountRequestParamName, forKey: "accountRequestParamName")
		dictionary.setValue(self.accountServiceName, forKey: "accountServiceName")
		dictionary.setValue(self.walletServiceName, forKey: "walletServiceName")
		dictionary.setValue(self.validationMsg, forKey: "validationMsg")
		dictionary.setValue(self.successMsg, forKey: "successMsg")
		dictionary.setValue(self.needPresentment, forKey: "needPresentment")

		return dictionary
	}

}
