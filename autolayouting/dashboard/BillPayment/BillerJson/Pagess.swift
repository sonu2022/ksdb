/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Pagess {
	public var pageNo : Int?
	public var layouts : Array<Layout>?
	public var showRecentPaymentsList : Bool?
	public var showProceedToPaymentOption : Bool?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let pages_list = Pages.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Pages Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Pagess]
    {
        var models:[Pagess] = []
        for item in array
        {
            models.append(Pagess(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let pages = Pages(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Pages Instance.
*/
	required public init?(dictionary: NSDictionary) {

		pageNo = dictionary["pageNo"] as? Int
        if (dictionary["layouts"] != nil) { layouts = Layout.modelsFromDictionaryArray(array: dictionary["layouts"] as! NSArray) }
		showRecentPaymentsList = dictionary["showRecentPaymentsList"] as? Bool
		showProceedToPaymentOption = dictionary["showProceedToPaymentOption"] as? Bool
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.pageNo, forKey: "pageNo")
		dictionary.setValue(self.showRecentPaymentsList, forKey: "showRecentPaymentsList")
		dictionary.setValue(self.showProceedToPaymentOption, forKey: "showProceedToPaymentOption")

		return dictionary
	}

}
