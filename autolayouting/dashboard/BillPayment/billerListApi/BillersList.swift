/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class BillersList {
	public var id : Int?
	public var billerName : String?
	public var billerCode : String?
	public var billerdesc : String?
	public var active : Bool?
	public var packageType : String?
	public var accountNumber : String?
	public var validationRequired : Bool?
	public var username : String?
	public var password : String?
	public var token : String?
	public var endpoint : String?
	public var fetchEndpoint : String?
	public var presentmentReq : Bool?
	public var validationReq : Bool?
	public var notificationReq : Bool?
	public var postOnline : Bool?
	public var billerType : String?
	public var tpPaymentUrl : String?
	public var tpFetchUrl : String?
	public var tempId : String?
	public var ccCode : String?
	public var providerName : String?
	public var serviceId : String?
	public var filePath : String?
	public var imagePath : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let billersList_list = BillersList.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of BillersList Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [BillersList]
    {
        var models:[BillersList] = []
        for item in array
        {
            models.append(BillersList(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let billersList = BillersList(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: BillersList Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		billerName = dictionary["billerName"] as? String
		billerCode = dictionary["billerCode"] as? String
		billerdesc = dictionary["billerdesc"] as? String
		active = dictionary["active"] as? Bool
		packageType = dictionary["packageType"] as? String
		accountNumber = dictionary["accountNumber"] as? String
		validationRequired = dictionary["validationRequired"] as? Bool
		username = dictionary["username"] as? String
		password = dictionary["password"] as? String
		token = dictionary["token"] as? String
		endpoint = dictionary["endpoint"] as? String
		fetchEndpoint = dictionary["fetchEndpoint"] as? String
		presentmentReq = dictionary["presentmentReq"] as? Bool
		validationReq = dictionary["validationReq"] as? Bool
		notificationReq = dictionary["notificationReq"] as? Bool
		postOnline = dictionary["postOnline"] as? Bool
		billerType = dictionary["billerType"] as? String
		tpPaymentUrl = dictionary["tpPaymentUrl"] as? String
		tpFetchUrl = dictionary["tpFetchUrl"] as? String
		tempId = dictionary["tempId"] as? String
		ccCode = dictionary["ccCode"] as? String
		providerName = dictionary["providerName"] as? String
		serviceId = dictionary["serviceId"] as? String
		filePath = dictionary["filePath"] as? String
		imagePath = dictionary["imagePath"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.billerName, forKey: "billerName")
		dictionary.setValue(self.billerCode, forKey: "billerCode")
		dictionary.setValue(self.billerdesc, forKey: "billerdesc")
		dictionary.setValue(self.active, forKey: "active")
		dictionary.setValue(self.packageType, forKey: "packageType")
		dictionary.setValue(self.accountNumber, forKey: "accountNumber")
		dictionary.setValue(self.validationRequired, forKey: "validationRequired")
		dictionary.setValue(self.username, forKey: "username")
		dictionary.setValue(self.password, forKey: "password")
		dictionary.setValue(self.token, forKey: "token")
		dictionary.setValue(self.endpoint, forKey: "endpoint")
		dictionary.setValue(self.fetchEndpoint, forKey: "fetchEndpoint")
		dictionary.setValue(self.presentmentReq, forKey: "presentmentReq")
		dictionary.setValue(self.validationReq, forKey: "validationReq")
		dictionary.setValue(self.notificationReq, forKey: "notificationReq")
		dictionary.setValue(self.postOnline, forKey: "postOnline")
		dictionary.setValue(self.billerType, forKey: "billerType")
		dictionary.setValue(self.tpPaymentUrl, forKey: "tpPaymentUrl")
		dictionary.setValue(self.tpFetchUrl, forKey: "tpFetchUrl")
		dictionary.setValue(self.tempId, forKey: "tempId")
		dictionary.setValue(self.ccCode, forKey: "ccCode")
		dictionary.setValue(self.providerName, forKey: "providerName")
		dictionary.setValue(self.serviceId, forKey: "serviceId")
		dictionary.setValue(self.filePath, forKey: "filePath")
		dictionary.setValue(self.imagePath, forKey: "imagePath")

		return dictionary
	}

}