//
//  MyServiceCollectionViewCell.swift
//  autolayouting
//
//  Created by Manam on 30/03/22.
//

import UIKit

class MyServiceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var myServiceCollectionview: UIView!
    

    @IBOutlet var myServiceCollectionViewImage: UIImageView!
    @IBOutlet var myServiceCollectionViewLabel: UILabel!
}
