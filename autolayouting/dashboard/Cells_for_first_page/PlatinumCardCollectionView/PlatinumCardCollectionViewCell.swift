//
//  PlatinumCardCollectionViewCell.swift
//  autolayouting
//
//  Created by Manam on 31/03/22.
//

import UIKit

class PlatinumCardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var platinumCardView: UIView!
    
    @IBOutlet var platinumCardImage: UIImageView!
    
    @IBOutlet var platinumCardLabel: UILabel!
    
    
    
    
}
