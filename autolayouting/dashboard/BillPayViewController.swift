////
////  BillPayViewController.swift
////  autolayouting
////
////  Created by Manam on 12/09/22.
////
//
//import UIKit
//import SDWebImage
//
//class BillPayViewController: UIViewController {
//    
//    var billPayLabelObj:String?
//    var billPayImageObj:UIImage?
//    var billPayViewObj:UIColor?
//    
//    @IBOutlet var billPayLabel: UILabel!
//    
//    @IBOutlet var billPayImageView: UIView!
//    
//    @IBOutlet var billPayImage: UIImageView!
//    
//    @IBOutlet var billPayStack: UIStackView!
//    
//    
//   
//    
//    
//    enum ELEMENT_TYPES:String{
//        case INPUT_TEXTFIELD
//        case AMOUNT_TEXTFIELD
//        case INFO_BANNER
//        case LOOKUP
//        case SC_NUMBER
//        case LIST
//        case LIST_SINGLE
//        case SECRET_TEXTFIELD
//        case DROPDOWN
//        case AMOUNT_DROPDOWN
//        case LARGE_SELECTION_LIST
//        case READ_ONLY_LABEL
//        case HIDDEN_VARIABLE
//        case NON_EDITABLE_LABEL
//        case MORE_POPUP
//        case NEXT_BUTTON
//        case SUBMIT_BUTTON
//        case PACKAGES_SELECTION
//        case PKGLOOKUP
//    }
//    
//    
//    var LanguageManager :Bool = false
//    var laidOut : Bool = false
//    var isContinuation : Bool = false
//    var fromFavs:Bool = false
//    var fromFavsProcessed :Bool = false
//    var fromBills :Bool = false
//    @objc var favRequestParams : Dictionary = [String: String]()
//    let mainTextColor : UIColor = UIColor(red: 51/255, green: 69/255, blue: 86/255, alpha: 1.0)
//    let placeHolderTextColor : UIColor = UIColor(red: 168/255.0, green: 177/255.0, blue: 189/255.0, alpha: 1.0)
//    let mainFont : UIFont = UIFont.systemFont(ofSize: 14.0)
//    var amountField : ZMFloatingTextField!
//    var variableParams : Dictionary = [String: Any]()
//    @objc public var formFileType : String!
//    @objc var billerType : String!
//    @objc var HeaderTitle : String!
//    @objc var amountSelection : Bool = false
//    let dropDownHeight : CGFloat = 55
//    var FileName:String?
//    let textFieldHeight : CGFloat = 55
//    var nextButton : UIButton!
//    let nextButtonHeightRatio : CGFloat = 0.16
//    var constraints = [NSLayoutConstraint]()
//    var submissionFields:NSMutableArray = NSMutableArray()
//    var displayOnCompletionList : NSMutableArray = NSMutableArray()
//    var elementDictionary : Dictionary = [String:Elements]()
//    var rpKey: String!
//    
//    var formModel : BillerBase?
//    var elementplace:ElementDisplayName?
//    var elemts:Elements?
//    @objc var currentPage:Int = 1
//
//    var conterArray:[Counters] = []
//    var elementArray:[Elements] = []
//    
//
//    
//
//    
//    
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        billPayImage.layer.cornerRadius = 31.5
//        billPayImage.image = billPayImageObj
//        billPayLabel.text = billPayLabelObj
//        navigationController?.isNavigationBarHidden=true
//        
//        
//        self.billPayImageView.layer.cornerRadius = 31.5
//        self.billPayImageView.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;
//        self.billPayImageView.layer.masksToBounds = false
//        self.billPayImageView.layer.shadowRadius = 3.0
//        self.billPayImageView.layer.shadowOffset = CGSize(width: 0, height: 3.0)
//
//        self.billPayImageView.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
//        
//        
//        self.billPayImageView.layer.shadowOpacity = 0.1
//        self.billPayImageView.layer.shadowPath = UIBezierPath(roundedRect:billPayImageView.bounds, cornerRadius:billPayImageView.layer.cornerRadius).cgPath
//        
//        
//        
//        let buttonView : UIView = UIView()
//        constraints.append(buttonView.heightAnchor.constraint(equalToConstant: -30.0+(self.view.frame.size.width * nextButtonHeightRatio)))
//        buttonView.backgroundColor = UIColor.clear
//        billPayStack.addArrangedSubview(buttonView);
//        billPayStack.setCustomSpacing(20, after: buttonView)
//        
//        buttonView.translatesAutoresizingMaskIntoConstraints = false
//        nextButton = UIButton()
//        nextButton.translatesAutoresizingMaskIntoConstraints = false
//        buttonView.addSubview(nextButton)
//        nextButton.centerXAnchor.constraint(equalTo: buttonView.centerXAnchor).isActive = true
//        nextButton.centerYAnchor.constraint(equalTo: buttonView.centerYAnchor,constant: -222).isActive = true
//        nextButton.heightAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
//        nextButton.widthAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
//        nextButton.layer.cornerRadius = (self.view.frame.size.width * nextButtonHeightRatio) / 2.0
//        nextButton.backgroundColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 1.0)
//        nextButton.setImage(UIImage(named: "right-chevron.png"), for: .normal)
//        nextButton.imageView?.contentMode = .scaleAspectFit
////        nextButton.addTarget(self, action: #selector(buttonClicked1(sender:)), for: .touchUpInside)
//        
//        nextButton.layer.shadowColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 0.18).cgColor
//        nextButton.layer.shadowOffset = CGSize(width: 0, height: 7.0)
//        nextButton.layer.shadowOpacity = 1.0
//        nextButton.layer.shadowRadius = 17.0
//        nextButton.layer.masksToBounds = false
//        
//        
//        
//        
//
//
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = true
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
//        self.navigationItem.setHidesBackButton(true, animated: true)
//        
//        
//        
//        
//    }
//    
//    
//
//    @IBAction func backAction(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//                let customBackButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
//                navigationItem.backBarButtonItem = customBackButton
//                
//    }
//    
//
//    
//    
//    func layoutFormFields(){
//        //accessing the pages in formmodel
//        if let pages : Pagess = formModel?.pages?[currentPage-1] as? Pagess {
//            for layout : Layout in (pages.layouts as? [Layout])! {
////                if layout.layoutColumns == 1 {
////                    layoutSingle(layout: layout)
////
////            }
////                else if layout.layoutColumns == 2 {
////                    layoutDouble(layout: layout)
////                }
//    }
//            //Check if more pages are available
//            if currentPage < formModel!.pages!.count {
//                let buttonView : UIView = UIView()
//                constraints.append(buttonView.heightAnchor.constraint(equalToConstant: -30.0+(self.view.frame.size.width * nextButtonHeightRatio)))
//                buttonView.backgroundColor = UIColor.clear
//                billPayStack.addArrangedSubview(buttonView);
//                billPayStack.setCustomSpacing(20, after: buttonView)
//
//                buttonView.translatesAutoresizingMaskIntoConstraints = false
//                nextButton = UIButton()
//                nextButton.translatesAutoresizingMaskIntoConstraints = false
//                buttonView.addSubview(nextButton)
//                nextButton.centerXAnchor.constraint(equalTo: buttonView.centerXAnchor).isActive = true
//                nextButton.centerYAnchor.constraint(equalTo: buttonView.centerYAnchor,constant: -222).isActive = true
//                nextButton.heightAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
//                nextButton.widthAnchor.constraint(equalToConstant: self.view.frame.size.width * nextButtonHeightRatio).isActive = true
//                nextButton.layer.cornerRadius = (self.view.frame.size.width * nextButtonHeightRatio) / 2.0
//                nextButton.backgroundColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 1.0)
//                nextButton.setImage(UIImage(named: "right-chevron.png"), for: .normal)
//                nextButton.imageView?.contentMode = .scaleAspectFit
////                nextButton.addTarget(self, action: #selector(buttonClicked1(sender:)), for: .touchUpInside)
//
//                nextButton.layer.shadowColor = UIColor(red: 13/255, green: 77/255, blue: 162/255, alpha: 0.18).cgColor
//                nextButton.layer.shadowOffset = CGSize(width: 0, height: 7.0)
//                nextButton.layer.shadowOpacity = 1.0
//                nextButton.layer.shadowRadius = 17.0
//                nextButton.layer.masksToBounds = false
//
//            }
//
//    }
//
//    }
////
////
//    public override func viewWillLayoutSubviews() {
//        billPayStack.translatesAutoresizingMaskIntoConstraints = false
//
//        if !laidOut {
//            layoutFormFields()
//            if fromFavs && !fromFavsProcessed || fromBills
//            {
//                for item in submissionFields {
//                   if let field = item as? ZMFloatingTextField {
//
//                        if field.fieldType == ZMFloatingTextField.FIELD_TYPE.LOOKUP.rawValue || field.fieldType == ZMFloatingTextField.FIELD_TYPE.DROPDOWN.rawValue
//
//                        {
//                            // Do Nothing
//                        }
//                        else {
//                            field.text = favRequestParams[field.formId]
//                        }
//
//                    }
//                }
//                fromFavsProcessed = true
//            }
//            laidOut = true
//        }
//
//
//
//
//    }
//    
//    
//    
//    func layoutSingle(layout : Layouts){
//        
//        //to access the elements in the layout section
//        for elements : Elementz in (layout.elements as? [Elementz])!{
//            //checking the layut coloumn to call methods to layut forms
//        
//            if elements.displayOnCompletion ?? false {
//                displayOnCompletionList.add(elements)
//        }
//            
//            
//            
//            switch elements.elementType{
//                
//            case ELEMENT_TYPES.LOOKUP.rawValue, ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue:
////                ELEMENT_TYPES.SC_NUMBER.rawValue, ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue, ELEMENT_TYPES.DROPDOWN.rawValue
//                
//                var type : ZMFloatingTextField.FIELD_TYPE = ZMFloatingTextField.FIELD_TYPE.LOOKUP
//                let type2: ZMFloatingTextField.FIELD_TYPE = ZMFloatingTextField.FIELD_TYPE.TEXTFIELD
//                
//                if elements.elementType == ELEMENT_TYPES.LOOKUP.rawValue {
//                    type = ZMFloatingTextField.FIELD_TYPE.LOOKUP
//        
//                }
//                if elements.elementType == ELEMENT_TYPES.INPUT_TEXTFIELD.rawValue {
//                    type = ZMFloatingTextField.FIELD_TYPE.TEXTFIELD
//                }
////                else if elements.elementType == ELEMENT_TYPES.SC_NUMBER.rawValue {
////                    type = ZMFloatingTextField.FIELD_TYPE.SC_NUMBER
////                }
////                else if elements.elementType == ELEMENT_TYPES.DROPDOWN.rawValue {
////                    type = ZMFloatingTextField.FIELD_TYPE.DROPDOWN
////                }
//                
//                
//                let textField : ZMFloatingTextField = ZMFloatingTextField(frame: CGRect(x: 40, y: 40, width: self.view.frame.width-40, height: 55), fieldType: type )
//                
//                let textfield2 : ZMFloatingTextField = ZMFloatingTextField(frame: CGRect(x: 40, y: 80, width: self.view.frame.width-40, height: 55), fieldType: type2)
//                
//                
//                textField.formId = elements.self.elementId!
//                
////                textField.placeholder = elementplace?.en
////                textField._placeholder = elementplace?.en
//                
////                textField._placeholder = "fggfdgfdv"
//               
//
//                
////
////                if elements.subType == ZMFloatingTextField.SUB_TYPES.ELECTRICITY.rawValue {
////                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.ELECTRICITY.rawValue
////                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.ELECTRICITY.rawValue
////
////
////                }
////                else {
////                    textField.subFieldType = ZMFloatingTextField.SUB_TYPES.KHANEPANI.rawValue
////                    textfield2.subFieldType = ZMFloatingTextField.SUB_TYPES.KHANEPANI.rawValue
////
////                }
////                if elements.elementType == ELEMENT_TYPES.DROPDOWN.rawValue {
////                    textField.dropDownValues = NSMutableArray(array: elemts!.elementValues)
////                    textField.dropDownIds = NSMutableArray(array: elemts!.elementId)
////                }
//                textField.floatingLabelColor = placeHolderTextColor
//                textField.textColor = UIColor(red: 18/255.0, green: 47/255.0, blue: 89/255.0, alpha: 1.0)
//                textField.floatingLabelFont = UIFont.systemFont(ofSize: 15.0,weight: .medium)
////                textField.floatingLabelFont = UIFont(name: "GalanoGrotesque-Medium", size: CGFloat(11.0))
////                textField.font = UIFont.systemFont(ofSize: 15,weight: .medium)//UIFont(name: element.fontName ?? "", size: CGFloat(element.fontSize ?? ""))
//                
//                
//                textField.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))
//                textField._placeholder = elements.elementPlaceHolder?.en
//                textField.floatingLabel.frame = CGRect(x: 36, y: 30, width:50, height: 14)
//        
//                billPayStack.addArrangedSubview(textField)
//
//                billPayStack.setCustomSpacing(38, after: textField)
//                    submissionFields.add(textField)
//
//                
//                
//                
//                
//                textfield2.floatingLabelColor = placeHolderTextColor
//                textfield2.textColor = UIColor.red
//                textfield2.floatingLabelFont = UIFont.systemFont(ofSize: 15.0,weight: .medium)
//                textfield2.font = UIFont.systemFont(ofSize: 15,weight: .medium)//UIFont(name: element.fontName ?? "", size: CGFloat(element.fontSize ?? ""))
//                
//                textfield2.font = UIFont(name: elements.fontName ?? " ", size: CGFloat(elements.fontSize ?? 0))
//
//                
//                
//                constraints.append(textfield2.topAnchor.constraint(equalTo: textField.bottomAnchor,constant: 40))
////                mainStackView.addArrangedSubview(textfield2)
//                billPayStack.setCustomSpacing(80, after: textfield2)
//
//                submissionFields.add(textfield2)
//                elementDictionary[textfield2.formId] = elements
////                textField._placeholder = "Select counter"
//            
//
//             
//                    elementDictionary[textField.formId] = elements
//              
//                
//                
//                
////
//                
//            default :
//                let x : Int = 0
//                
//            
//    
//    
//    
//    
//        }
//    }
//    
//  
//    }
//    
//    
//    
//    
//    
//    
//}
