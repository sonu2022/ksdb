//
//  ViewController3.swift
//  autolayouting
//
//  Created by Manam on 08/04/22.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet var imageVc3: UIImageView!
    
    @IBOutlet var labelVc3: UILabel!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var viewVc3: UIView!
    
    var imageObj: UIImage?
    var lableObj: String?
    var viewObj:UIColor?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        viewVc3.layer.cornerRadius=21.5
        imageVc3.image = imageObj
        labelVc3.text = lableObj
        viewVc3.backgroundColor=viewObj
        navigationController?.isNavigationBarHidden=true
        
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    } 
  
}
