//
//  ViewController2.swift
//  autolayouting
//
//  Created by Manam on 30/03/22.
//

import UIKit
import grpc
import SwiftUI
import Alamofire
import AlamofireImage
import SDWebImage

@objc protocol SelectApiBillPayDelegate{
    func selectedApiBillPay(billerName:String)
}

class ViewController2: UIViewController,ChangeLanguageDelegate {
  
    

    
    
    
    
    
    
    @IBOutlet var secondView: UIView!
    
    
    
    @IBOutlet var accountCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet var AccountCollectionView: UICollectionView!
    //ACCOUNT COLLECTION VIEW
  var firstCollectionViewAccountLabel = ["Savings Account","FD Account","Savings Account"]
    var firstCollectionViewAccountNumber
    = ["xxxx764810","xxxx764909","xxxx764810"]
    var firstCollectionViewRupees =
    ["रू 24,600.50","रू 2,34,850.00","रू 24,600.50"]
    var firstCollectionViewMapImage:[UIImage] = [UIImage(named: "worldMap.png")!,UIImage(named: "greenMap.png")!,UIImage(named: "worldMap.png")!]
    var firstCollectionViewSavingCardImage:[UIImage] = [UIImage(named: "savingsCardIcon.png")!,UIImage(named: "fdCard.png")!,UIImage(named: "savingsCardIcon.png")!]
    
    var mapImageColor : [UIColor] = [UIColor(red: 13/255.0, green: 77/255.0, blue: 162/255.0, alpha: 1.0),UIColor(red: 107/255.0, green: 165/255.0, blue: 28/255.0, alpha: 1.0),UIColor(red: 13/255.0, green: 77/255.0, blue: 162/255.0, alpha: 1.0)]
        
    var iconviewColor : [UIColor] = [UIColor(red: 50/255.0, green: 96/255.0, blue: 155/255.0, alpha: 1.0),UIColor(red: 121/255.0, green: 163/255.0, blue: 66/255.0, alpha: 1.0),UIColor(red: 50/255.0, green: 96/255.0, blue: 155/255.0, alpha: 1.0)]
    
     
    
    let savingsCard : [[String:String]] = [["name":"Video Bank","icon":"videobanking.png"], ["name":"Withdraw","icon":"withdraw.png"],["name":"MCash","icon":"mcash.png"]]
    
    let fdCard : [[String:String]] = [["name":"Details","icon":"fdDetails.png"], ["name":"Withdraw","icon":"withdraw.png"],["name":"Statement","icon":"statement.png"]]
    
    
    
    let eCard : [[String:String]] = [["name":"Video Bank","icon":"videobanking.png"], ["name":"Withdraw","icon":"withdraw.png"],["name":"MCash","icon":"mcash.png"]]
    
       
    var cardOptions : [[[String:String]]] = []
    
    //MiddleCollectionView
    
    
   
   
   
    
    @IBOutlet var middleCollectionView: UICollectionView!
    
    
    var middleCollectionViewImage :[UIImage] = [UIImage(named: "ksdsend.png")!,UIImage(named: "ksdrecharge.png")!,UIImage(named: "ksdpayPasal.png")!,UIImage(named: "KSBDpayBills.png")!]
    
    var middlecollectionColor : [UIColor] = [UIColor(red: 23/255.0, green: 81/255.0, blue: 157/255.0, alpha: 1.0),UIColor(red: 132/255.0, green: 189/255.0, blue: 53/255.0, alpha: 1.0),UIColor(red: 247/255.0, green: 179/255.0, blue: 20/255.0, alpha: 1.0),UIColor(red: 238/255.0, green: 57/255.0, blue: 127/255.0, alpha: 1.0)]
    var middleCollectionViewSendLable = ["Send","Topup","Pay Shop","Pay Bills"]
    
    
    //MY SERVICE COLLECTIONVIEW
        
    @IBOutlet var MyserviceBigView: UIView!
    @IBOutlet var MyServiceCollectionView: UICollectionView!
    var myServiceCollectionViewImages:[UIImage] = [UIImage(named: "myAccount.png")!,UIImage(named: "myBills.png")!,UIImage(named: "loans.png")!,UIImage(named: "agent.png")!,UIImage(named: "merchant.png")!,UIImage(named: "myDigitalId.png")!]
   let myServiceCollectionViewlabel=["My Account","My Bills","Loans","Agent","Merchant","My DigitalID"]
    
    //PLATINUMCARD COLLECTIONVIEW
    @IBOutlet var platinumCardCollectionView: UICollectionView!
    
    var platinumCardCollectionViewImages:[UIImage] = [UIImage(named: "untitled1.png")!,UIImage(named: "fdhand.png")!]
    
    let platinumCardCollectionViewlabel=["Appy Platinum Credit Card","My Appy Personal Loan With Best Rate"]
    
    
    var platinumLabelColor : [UIColor] = [UIColor(red: 30/255.0, green: 65/255.0, blue: 79/255.0, alpha: 1.0),UIColor(red: 78/255.0, green: 129/255.0, blue: 43/255.0, alpha: 1.0)]
    
    var platinumviewColor : [UIColor] = [UIColor(red: 221/255.0, green: 239/255.0, blue: 246/255.0, alpha: 1.0),UIColor(red: 211/255.0, green: 240/255.0, blue: 191/255.0, alpha: 1.0)]
    
    
    
    //Bill PAYMENT COLLECTIONVIEW
    @IBOutlet var BillPayCollectionView: UICollectionView!
    
    @IBOutlet var outerBillPayView: UIView!
    
    
    var BillPayCollectionViewImages:[UIImage] = [UIImage(named: "electricity.png")!,UIImage(named: "khanePani.png")!,UIImage(named: "internet.png")!,UIImage(named: "ksdrcCards.png")!,UIImage(named: "landline.png")!,UIImage(named: "tv.png")!,UIImage(named: "smartSchool.png")!,UIImage(named: "eLearning.png")!]
    
    let BillPayCollectionViewLabel=["Electricity","Khanepani","Internet","RC Card","Landline","TV","School","eLearning"]
    
    
    
    
     
    @IBOutlet var MoreView: UIView!
    
    
    @IBOutlet var billPaymentView: UIView!
    
    
    @IBOutlet var apiBillPaymentCollectionView: UICollectionView!
    
    
    
    let ApiBillClass = ApiCalling()
    
    
    
    
    var request_id:Int?
    var Service_name:String?
    var biller_name:String?
    var channel:String?
    var inst:Int?
    var billPaymentArray = [BillersList]()
    
    var billPaymentArray2 = [BillersList]()
    var billerList:BillersList?
    
    weak var BillPay: SelectApiBillPayDelegate?
    
    

  


//
//    var billerImagePath = ["http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=URA.png", "http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME%20NW%20CONNECTIOON.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=NWSC.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=PAY%20TV.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME%20PREPAID.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME%20POST%20PAID.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=HIMA%20CEMENT.png","http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=URA%20DTS.png"]
    
    
    
    
    

//    var billerImagePath2:[UIImage] = [UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=URA.png")!,UIImage(named: "http://bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME NW CONNECTIOON.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=NWSC.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=PAY TV.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME PREPAID.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=UMEME POST PAID.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=HIMA CEMENT.png")!,UIImage(named: "http:bp-ug-mobi-test.apps.dev.aro.kcbgroup.com/billPayments/billerImages?ref=URA DTS.png")!]

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        cardOptions.append(savingsCard)
        cardOptions.append(fdCard)
        cardOptions.append(eCard)


        accountCollectionHeight.constant = self.view.frame.size.width*0.8
        AccountCollectionView.delegate=self
        AccountCollectionView.dataSource=self
        middleCollectionView.delegate=self
        middleCollectionView.dataSource=self
       
        MyServiceCollectionView.delegate=self
        MyServiceCollectionView.dataSource=self
        self.MyserviceBigView.layer.cornerRadius=25
        platinumCardCollectionView.dataSource=self
        platinumCardCollectionView.delegate=self
        BillPayCollectionView.delegate=self
        BillPayCollectionView.dataSource=self
        
        apiBillPaymentCollectionView.dataSource = self
        apiBillPaymentCollectionView.delegate = self
        billPaymentView.layer.cornerRadius = 25
        
        
        //Add gestures
//        let leftSwipeGest = UISwipeGestureRecognizer(target: self, action: #selector(ViewController2.leftSwipeGesture))
//        leftSwipeGest.direction = UISwipeGestureRecognizer.Direction.left
//        self.AccountCollectionView.addGestureRecognizer(leftSwipeGest)

//
//
//        let rightSwipeGest = UISwipeGestureRecognizer(target: self, action: #selector(ViewController2.rightSwipeGesture))
//        rightSwipeGest.direction = UISwipeGestureRecognizer.Direction.right
//        self.AccountCollectionView.addGestureRecognizer(rightSwipeGest)
//
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//        AccountCollectionView.isPagingEnabled=true
     //   self.outerBillPayView.layer.cornerRadius=25
        
        self.MoreView.layer.cornerRadius = 17.0
        self.MoreView.layer.borderColor = UIColor(red: 0/190.0, green: 0/200.0, blue: 0/212.0, alpha: 0.27).cgColor;
        self.MoreView.layer.masksToBounds = false
        self.MoreView.layer.shadowRadius = 3.0
        self.MoreView.layer.shadowOffset = CGSize(width: 0, height: 3.0)

        self.MoreView.layer.shadowColor = UIColor(red: 0/216.0, green: 0/222.0, blue: 0/231.0, alpha: 1.0).cgColor
        
        
        self.MoreView.layer.shadowOpacity = 0.1
        self.MoreView.layer.shadowPath = UIBezierPath(roundedRect:MoreView.bounds, cornerRadius:MoreView.layer.cornerRadius).cgPath
        
        
        secondView.layer.cornerRadius=25
        
        //billpayment
        outerBillPayView.layer.cornerRadius=25
        
        
        
        
        
//        apiBilllerJsonCall()
        
        
        
    }
    
    //-------gotoprofile--------//
    
    
//    @IBAction func profileSegueButton(_ sender: Any) {
//        performSegue(withIdentifier: "gotoProfile", sender: self)
//        
//    }
//    
//--------------SSL PINNING--------------//
   
//qwerfsb
    
    
    
    
 
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
   
    @objc func cardOptions_1(sender:UIButton){
        print("buttonPressed !")
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3
       
        ViewController3?.lableObj = sender.titleLabel?.text
        ViewController3?.imageObj = sender.imageView?.image
        ViewController3!.viewObj = UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0)
        

        self.navigationController?.pushViewController(ViewController3!, animated: true)
    }
    
    @objc func cardOptions_2(sender:UIButton){
        print("buttonPressed !")
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3

        ViewController3?.lableObj = sender.titleLabel?.text
        ViewController3?.imageObj = sender.imageView?.image
        ViewController3!.viewObj = UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0)


        self.navigationController?.pushViewController(ViewController3!, animated: true)
    }
    
    
    
    
    
    @objc func cardOptions_3(sender:UIButton){
        print("buttonPressed !")
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3

        ViewController3?.lableObj = sender.titleLabel?.text
                ViewController3?.imageObj = sender.imageView?.image
        ViewController3!.viewObj = UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0)


        self.navigationController?.pushViewController(ViewController3!, animated: true)
    }
    
    func changeLanguageHindi() {
        
       
        

        
        
        
        
    }
    
    func changeLanguageEnglish() {
        
    }
   
    
//    func apiBilllerJsonCall(){
//
//        let jsonbeanDic: NSDictionary = [:]
//        let dataDic: NSDictionary = ["UTIL_BILLER_CODE":"NWSC_PAY"]
//        let mainDicz: NSDictionary = ["serviceName":"BILLER_JSON_FILE","requestId":"123456789012","newLoginBean":jsonbeanDic,"data":dataDic]
//
//
//        BillerJsonApi.PutBillerJson(mainDic: mainDicz,isCertificatePinning: true) { BillerJsons in
//
//
//
//        }
//
//    }
    
    
    
    
    

    }
    
   
@available(iOS 13.0, *)
extension ViewController2:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView==AccountCollectionView){

            return firstCollectionViewAccountLabel.count
            }
        if (collectionView==MyServiceCollectionView){
            return 6
        }
        if (collectionView==platinumCardCollectionView){
            return 2
        }
        if (collectionView==BillPayCollectionView){
            return 8
        }
        if (collectionView == apiBillPaymentCollectionView){
            return 8
        }

    return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView==AccountCollectionView){

                let cell = AccountCollectionView.dequeueReusableCell(withReuseIdentifier: "cell20", for: indexPath as IndexPath) as! AccountCollectionViewCell
                cell.firstaccountView.layer.cornerRadius=24
                cell.savingCardIconView.layer.cornerRadius=9
                let mapImage = firstCollectionViewMapImage[indexPath.row]
                cell.MapImage.image=mapImage
            
        let accountLabel = firstCollectionViewAccountLabel[indexPath.row]
        cell.AccountLabel.text=accountLabel
        let accountNumberLabel = firstCollectionViewAccountNumber[indexPath.row]
        cell.AccountNumberLabel.text=accountNumberLabel
        let ruppesLabel = firstCollectionViewRupees[indexPath.row]
        cell.RupeeLabel.text=ruppesLabel
        let savingCard = firstCollectionViewSavingCardImage[indexPath.row]
        cell.savingCardiconImage.image=savingCard
                    let firstAccountViewColor = mapImageColor[indexPath.row]
         cell.firstaccountView.backgroundColor = firstAccountViewColor
        cell.savingCardIconView.backgroundColor = iconviewColor[indexPath.row]

    
        let savins = cardOptions[indexPath.row]
        let option1 = savins[0]
        let option2 = savins[1]
        let option3 = savins[2]
        
        cell.cardOptionLabel_1.text = (option1["name"]!)
        cell.cardOptionLabel_2.text = (option2["name"]!)
        cell.cardOptionLabel_3.text = (option3["name"]!)
        cell.cardOptionImage_1.image = UIImage(named:option1["icon"]!)
        cell.cardOptionImage_2.image = UIImage(named:option2["icon"]!)
        cell.cardOptionImage_3.image = UIImage(named:option3["icon"]!)
        cell.withdrawview.layer.cornerRadius=15
        cell.mCashView.layer.cornerRadius=15
        cell.detailsView.layer.cornerRadius=15
            
            cell.cardOptionButton_1.tag = indexPath.row
            cell.cardOptionButton_1.addTarget(self, action: #selector(cardOptions_1), for: UIControl.Event.touchUpInside)
            cell.cardOptionButton_1.titleLabel?.text = option1["name"]
            cell.cardOptionButton_1.imageView?.image = UIImage(named:option1["icon"]!)

            
            cell.cardOptionButton_2.tag = indexPath.row
            cell.cardOptionButton_2.addTarget(self, action: #selector(cardOptions_2), for: UIControl.Event.touchUpInside)
            cell.cardOptionButton_2.titleLabel?.text = option2["name"]
            cell.cardOptionButton_2.imageView?.image = UIImage(named:option2["icon"]!)

            cell.cardOptionButton_3.tag = indexPath.row
            cell.cardOptionButton_3.addTarget(self, action: #selector(cardOptions_3), for: UIControl.Event.touchUpInside)
            cell.cardOptionButton_3.titleLabel?.text = option3["name"]
            cell.cardOptionButton_3.imageView?.image = UIImage(named:option3["icon"]!)
            

            
           
              
    
        return cell

    
    }
        
        
        
    
        if(collectionView==middleCollectionView){
            let cell = middleCollectionView.dequeueReusableCell(withReuseIdentifier: "cell21", for: indexPath as IndexPath) as! MiddleCollectionViewCell
            
            let middleImage = middleCollectionViewImage[indexPath.row]
            cell.sendImage.image=middleImage
            cell.sendView.layer.cornerRadius=19
            let sendImageColor = middlecollectionColor[indexPath.row]
            cell.sendView.backgroundColor = sendImageColor
            let label = middleCollectionViewSendLable[indexPath.row]
            cell.sendLabel.text=label
            
            
           
            
           
           
            return cell
            
        }
        if (collectionView==MyServiceCollectionView){
         
            let cell = MyServiceCollectionView.dequeueReusableCell(withReuseIdentifier: "cell22", for: indexPath as IndexPath) as! MyServiceCollectionViewCell
            
            
            let image = myServiceCollectionViewImages[indexPath.row]
            cell.myServiceCollectionViewImage.image=image
            let label = myServiceCollectionViewlabel[indexPath.row]
            cell.myServiceCollectionViewLabel.text=label
       //     cell.myServiceCollectionview.layer.cornerRadius=19.0
            cell.contentView.layer.cornerRadius=19
            
            cell.self.contentView.layer.cornerRadius = 19.0
            cell.self.contentView.layer.borderColor = UIColor(red: 190/255.0, green: 200/255.0, blue: 212/255.0, alpha: 0.27).cgColor;
            cell.self.layer.masksToBounds = false
            cell.self.contentView.layer.shadowRadius = 3.0
            cell.self.contentView.layer.shadowOffset = CGSize(width: 0, height: -2.0)
            cell.self.contentView.layer.masksToBounds = false

           cell.layer.shadowColor = UIColor(red: 216/255.0, green: 222/255.0, blue: 231/255.0, alpha: 1.0).cgColor
          //  cell.self.MyServiceCollectionView.layer.shadowOffset=CGSize(width: 0.0, height: -2.0)
            //cell.self.myServiceCollectionview.layer.shadowRadius=3.0
            //cell.self.myServiceCollectionview.layer.cornerRadius = 19.0
            //cell.self.myServiceCollectionview.layer.masksToBounds = false
           
            cell.self.contentView.layer.shadowOpacity = 0.07
            cell.self.contentView.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
            cell.layer.shadowRadius = 19.0
            
            cell.layer.shadowOffset = CGSize(width: 0, height: 3.0)

            
            
            
            return cell
        }
        if (collectionView==platinumCardCollectionView){
        
            let cell = platinumCardCollectionView.dequeueReusableCell(withReuseIdentifier: "cell23", for: indexPath as IndexPath) as! PlatinumCardCollectionViewCell
            let image = platinumCardCollectionViewImages[indexPath.row]
            cell.platinumCardImage.image=image
            let label = platinumCardCollectionViewlabel[indexPath.row]
            cell.platinumCardLabel.text=label
            cell.platinumCardView.layer.cornerRadius=18

            let platinumLabel = platinumLabelColor[indexPath.row]
            cell.platinumCardLabel.textColor = platinumLabel
            
            
            let platinumviewColor = platinumviewColor[indexPath.row]
            cell.platinumCardView.backgroundColor = platinumviewColor
            
            
            
           return cell
        }
        
        if(collectionView == apiBillPaymentCollectionView){
            let cell = apiBillPaymentCollectionView.dequeueReusableCell(withReuseIdentifier: "cell30", for: indexPath as IndexPath)as! ApiPayBillCollectionViewCell
            
        
            
            
                //-------Api image called a third party way known as almofire-------\\
            
//            DispatchQueue.main.async {

//                AF.request(self.billerImagePath[indexPath.row],method: .get).responseData { response in
//                    if response.error == nil {
//                         print("expecto\(response.result)")
//        
//                        
//                        
//                         // Show the downloaded image:
//                        DispatchQueue.main.async() {
//
//                         if let data = response.data {
//                             
////                             let imagedData = NSData(contentsOfURL: billerImagePath[indexPath.row]!)!
//                             
//                             
//                             cell.apiBillPayImageView.image = UIImage(data: data)
//                         }
//                         }
//                     }
//                 }


//            }
            
            
            
        var imageDirect:String = ""
            
       cell.apiBillPayView.layer.cornerRadius = 31.5
    
            let jsonbeanDic: NSDictionary = ["accessCode": "EJqg7Op4VHSd2Uta1Z9QVk+8Cets8E2QoCGqVKk0lvZTvIBoaYduKtM6B4jJ7ep56oxOYfooPVLHAK76QlJf81+EBdCm7WZEHvrCwx6MWaP61oMJRdLCCl3FDJ0pLrFeM2SK8W/z9Fhh+1CxsbWC52bgVrOpSoZg0Zd3/ZsISdCILkkj8/UsS3GrpVTugViJY7X1znm358L6YfRa8MkBeiqIT91c25zkKFR+K2wXC+O+x8jj6QWZpj4wEWAl64tIvw3tFkOdZ+60RpTk6BMbjMDcfjPuAwqdoXUbZ4ouVRVxxmHDrArMpYlS5rePQjc0Pp1rVEM3UQTAx8iEYEQFtw==",
                                             "mobileNumber": "256757883769"]
            
            
    //        print("billerList\(jsonbeanDic)")
            
            let dataDic: NSDictionary = [:]
            
            let deviceDic:NSDictionary = [ "deviceId": "6b0d015a-5051-4bfb-b5bb-b9455b8eadde",
                                           "model": "",
                                           "os": "",
                                           "ip": "",
                                           "location": ""]
            
            let mainDic: NSDictionary = ["serviceName":"BILLER_LIST","requestId":"25675788376915092022104902983","newLoginBean":jsonbeanDic,"data":dataDic,"device":deviceDic]

            

            
//            let url1 = URL(string:"https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerList")
            
//            let pathString = url1?.path // String

            ApiBillClass.API(Url: "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerList", parameters: mainDic, httpMethodType: HTTPMethod.PUT, success:{jsonData,response in
            
                let bill = BillerCall(dictionary: jsonData as NSDictionary)

                DispatchQueue.main.async {

                cell.apiBillPayLabel.text = bill?.data?.billersList?[indexPath.row].billerName
               
                }
                
                //----------------image--------------------\\
                imageDirect = bill?.data?.billersList?[indexPath.row].imagePath ?? ""
                
                // use this if the imagepath is having whitespace
                let val = (imageDirect.components(separatedBy: " ").joined(separator: "%20"))
                
             
                let baseImageURL = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerImages?ref="
                let params = NSString(format: "%@",val)
                let urlString = NSString(format: "%@%@",baseImageURL,params)
                cell.apiBillPayImageView.sd_setImage(with: URL(string: urlString as String), placeholderImage: UIImage(named: "placeholder.png"))
                print("the rendervouz\(urlString)")

                
                       //-----------ThirdParty method to call the APi----------\\(sdwebimage)
           //            When calling this thirdparty method we dont need to call api. The value is already getting called using this sdwebimage method
                       
                               //  SDWebImageDownloaderOperation ()
                       
           //            To disable the ssl pinning certificate go to SDWebImageDownloaderOperation.m () and comment out this piece of code:-
                       
           //            if (!(self.options & SDWebImageDownloaderAllowInvalidSSLCertificates)) {
                     //            disposition = NSURLSessionAuthChallengePerformDefaultHandling;
                     //        }
                       
                       //-----------------------------------------------------------\\
                       

            }, Error:{data, response in

            })
     
            
            return cell
        
        }
        
        
        
        
        if(collectionView==BillPayCollectionView){
          //  if indexPath.row==0{
                let cell = BillPayCollectionView.dequeueReusableCell(withReuseIdentifier: "cell24", for: indexPath as IndexPath) as! BillPayCollectionViewCell
                cell.billPayView.layer.cornerRadius=31.5
            let image = BillPayCollectionViewImages[indexPath.row]
            cell.billPayImage.image=image
       let label = BillPayCollectionViewLabel[indexPath.row]
        
            cell.billpayLabel.text=label
//            outerBillPayView.layer.cornerRadius=25

            let gradient = CAGradientLayer()
            cell.gradient?.frame = cell.billPayView.bounds
            cell.gradient?.colors = [UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0).cgColor,UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.0).cgColor]
            cell.billPayView.layer.insertSublayer(gradient, at: 0)
               
          
                return cell
        
        
        }
        
        
    return UICollectionViewCell()
    }
    
    
   

    
    
    
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      if (collectionView==AccountCollectionView){

            return CGSize(width: self.view.frame.size.width*0.825, height: self.view.frame.size.width*0.8)
       }
        if (collectionView==MyServiceCollectionView){
        

            //return CGSize(width: size, height: size1)
            let noOfCellsInRow = 6

                      let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                      let totalSpace = flowLayout.sectionInset.left
                          + flowLayout.sectionInset.right
                          + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 14))

                      let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

                   return CGSize(width: size+20, height: 130)
            
            
        
        }
        if (collectionView==platinumCardCollectionView){
        
            let noOfCellsInRow = 2

                      let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                      let totalSpace = flowLayout.sectionInset.left
                          + flowLayout.sectionInset.right
                          + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 3))

                      let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

                   return CGSize(width: size+20, height: size-16 )
           
        
        
        }
        if(collectionView==BillPayCollectionView){
         
            let noOfCellsInRow = 8

                      let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                      let totalSpace = flowLayout.sectionInset.left
                          + flowLayout.sectionInset.right
                          + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 40))

                      let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

                   return CGSize(width: size, height: 90)
           
       }
        
        if (collectionView==apiBillPaymentCollectionView){
            let noOfCellsInRow = 8

                      let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                      let totalSpace = flowLayout.sectionInset.left
                          + flowLayout.sectionInset.right
                          + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 40))

                      let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

                   return CGSize(width: size, height: 90)
        }
         
           
          
        let noOfCellsInRow = 4

                  let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                  let totalSpace = flowLayout.sectionInset.left
                      + flowLayout.sectionInset.right
                      + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 3))

                  let size = Double((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

               return CGSize(width: size, height: 130)
       
    }
    
     

    
    func collectionView(_ collectionView: UICollectionView,
      didSelectItemAt indexPath: IndexPath) {
        if (collectionView==AccountCollectionView){

            let indexPath = self.AccountCollectionView.indexPathsForSelectedItems?.first ?? IndexPath(item: 1, section: 0)


           self.AccountCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)


        
    }
        if(collectionView==middleCollectionView) {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3
            ViewController3!.imageObj = middleCollectionViewImage[indexPath.row]
            ViewController3!.lableObj = middleCollectionViewSendLable[indexPath.row]
            ViewController3!.viewObj = middlecollectionColor[indexPath.row]
            
            self.navigationController?.pushViewController(ViewController3!, animated: true)
           
            
           
    
    }
        if(collectionView==MyServiceCollectionView) {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3
            ViewController3!.imageObj = myServiceCollectionViewImages[indexPath.row]
            ViewController3!.lableObj = myServiceCollectionViewlabel[indexPath.row]
            ViewController3!.viewVc3?.layer.opacity=0.6
            ViewController3!.viewVc3?.layer.shadowRadius=5
            ViewController3!.viewVc3?.layer.shadowOffset=CGSize(width: 0.0, height: 3)
            ViewController3!.viewVc3?.layer.shadowColor=UIColor(red: 216/255.0, green: 222/255.0, blue: 231/255.0, alpha: 1.0).cgColor

            
            
            self.navigationController?.pushViewController(ViewController3!, animated: true)
           
            
           
    
    }
        if(collectionView==platinumCardCollectionView) {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let ViewController3 = story.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3
            ViewController3!.imageObj = platinumCardCollectionViewImages[indexPath.row]
            ViewController3!.lableObj = platinumCardCollectionViewlabel[indexPath.row]
            ViewController3!.viewObj = platinumviewColor[indexPath.row]
            
            self.navigationController?.pushViewController(ViewController3!, animated: true)
           
            
           
    
    }
        if(collectionView==BillPayCollectionView) {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let KhanepaniViewController = story.instantiateViewController(withIdentifier: "KhanepaniViewController") as? KhanepaniViewController
            KhanepaniViewController!.khanepaniImageObj = BillPayCollectionViewImages[indexPath.row]
//            KhanepaniViewController!.khanepaniLabelObj = BillPayCollectionViewLabel[indexPath.row]
            KhanepaniViewController!.khanepaniViewObj = UIColor(red: 234/255.0, green: 238/255.0, blue: 247/255.0, alpha: 1.0)
            self.navigationController?.pushViewController(KhanepaniViewController!, animated: true)
           
            
           
    
    }
        if(collectionView==apiBillPaymentCollectionView){
            
            
            
//            let cell = apiBillPaymentCollectionView.dequeueReusableCell(withReuseIdentifier: "cell30", for: indexPath as IndexPath)as! ApiPayBillCollectionViewCell
            
            
            
//            let jsonbeanDic: NSDictionary = ["accessCode": "EJqg7Op4VHSd2Uta1Z9QVk+8Cets8E2QoCGqVKk0lvZTvIBoaYduKtM6B4jJ7ep56oxOYfooPVLHAK76QlJf81+EBdCm7WZEHvrCwx6MWaP61oMJRdLCCl3FDJ0pLrFeM2SK8W/z9Fhh+1CxsbWC52bgVrOpSoZg0Zd3/ZsISdCILkkj8/UsS3GrpVTugViJY7X1znm358L6YfRa8MkBeiqIT91c25zkKFR+K2wXC+O+x8jj6QWZpj4wEWAl64tIvw3tFkOdZ+60RpTk6BMbjMDcfjPuAwqdoXUbZ4ouVRVxxmHDrArMpYlS5rePQjc0Pp1rVEM3UQTAx8iEYEQFtw==",
//                                             "mobileNumber": "256757883769"]
//
//            let dataDic: NSDictionary = [:]
//
//            let deviceDic:NSDictionary = [ "deviceId": "6b0d015a-5051-4bfb-b5bb-b9455b8eadde",
//                                           "model": "",
//                                           "os": "",
//                                           "ip": "",
//                                           "location": ""]
//
//            let mainDic: NSDictionary = ["serviceName":"BILLER_LIST","requestId":"25675788376915092022104902983","newLoginBean":jsonbeanDic,"data":dataDic,"device":deviceDic]


//            ApiBillClass.API(Url: "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerList", parameters: mainDic, httpMethodType: HTTPMethod.PUT, success:{jsonData,response in
//                DispatchQueue.main.async {
//                    let bill = BillerCall(dictionary: jsonData as NSDictionary)
//                    let story = UIStoryboard.init(name: "Main", bundle: nil)
//                    let billViewController = story.instantiateViewController(withIdentifier: "BillPayViewController") as?
//                    BillPayViewController
//                    billViewController?.billPayLabelObj = bill?.data?.billersList?[indexPath.row].billerName
//
//                    //------------image------------\\
//
//                       let baseImageURL = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerImages?ref="
//                    let imagePath = bill?.data?.billersList?[indexPath.row].imagePath ?? ""
//                    let value = (imagePath.components(separatedBy: " ").joined(separator: "%20"))
//                    let param = NSString(format: "%@", value)
//                    let fullUrl = NSString(format: "%@%@", baseImageURL,param)
//
//
//
//                    //--------To assign the image from SDwemimage to an image object from another VC Do this -------------\\
//
//                    cell.apiBillPayImageView.sd_setImage(with: URL(string: fullUrl as String), completed: { (image, error, cache, url) in
//                        if image != nil {
//                            billViewController?.billPayImageObj = image
//                        } else {
//                            print("didn't load image")
//                        }
//                    })
//
//                                            //------------------------------------------\\
//
//
//
//
//
//
//                    self.navigationController?.pushViewController(billViewController!, animated: true)
//
//                }
//
//
//
//
//                },Error:{data, response in
//
//
//
//
//                })

//                }
//        })





            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let billViewController = story.instantiateViewController(withIdentifier: "KhanepaniViewController") as?
            KhanepaniViewController
            billViewController?.khanepaniLabelObj = "Label"
            
            
            self.navigationController?.pushViewController(billViewController!, animated: true)



        
        
        }

    }
    
    
    
    
    
}
extension UIImageView {
    func loadFrom(URLAddress: String) {
        guard let url = URL(string: URLAddress) else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let loadedImage = UIImage(data: imageData) {
                        self?.image = loadedImage
                }
            }
        }
    }
}





//struct UploadData {
//    let data:[String:String] = [:]
//    let newLoginBean:NewLoginBean
//    let serviceName:String
//    let requestId:String
//
//    init?(fromDict dict:Dictionary<String, Any>) {
//        guard let data = dict["data"] as? [String:String], let newLoginBean = NewLoginBean(fromDict: dict["newLoginBean"] as? Dictionary<String, String>), let requestId = dict["serviceName"] as? String, let serviceName = dict["requestId"] as? String else {
//            return nil
//        }
//
//        self.data = data
//        self.newLoginBean = newLoginBean
//        self.requestId = requestId
//        self.serviceName = serviceName
//    }
//}
//
////struct Data {
////    let id:String
////    let name:String
////
////    init?(fromDict dict:Dictionary<String, String>?) {
////        guard let id = dict?["id"], let name = dict?["name"] else {
////            return nil
////        }
////        self.id = id
////        self.name = name
////    }
////}
//
//struct NewLoginBean {
//    let mobileNumber:String
//
//    init?(fromDict dict:Dictionary<String, String>?) {
//        guard let mobileNumber = dict?["mobileNumber"] else {
//            return nil
//        }
//        self.mobileNumber = mobileNumber
//    }
//}



//--------Api imagesection--------\\\
//let bill = BillerCall(dictionary: jsonData as NSDictionary)
//let baseImageURL = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerImages?ref="
//let moreDic = bill?.data?.billersList?[indexPath.row].imagePath
//
//let natIDstr = baseImageURL + "\(String(describing: moreDic))"
//
//var data: Data? = nil
//do{
//
//if let url = URL(string: natIDstr) {
//
//
//    data = try Data(contentsOf: url)
//}
//
//let imagePath = String("\(baseImageURL)/\(imageDirect)")
//   let fileName = (UIImage(named: imagePath)?.pngData())!
//
//cell.apiBillPayImageView.sd_setImage(with: fileName)
//
//
//
//
//}
//catch{
//print(error)
//}
