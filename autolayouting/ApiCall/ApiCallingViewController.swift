//
//  ApiCallingViewController.swift
//  autolayouting
//
//  Created by Manam on 15/09/22.
//

import UIKit
import Alamofire

@objc public enum HTTPMethod: Int,Decodable {
    case POST
    case GET
    case PUT
}

class ApiCalling: NSObject, URLSessionDelegate {
    
    @objc static let shared = ApiCalling()
//STATIC:- eaiser to utilize an object property without managing the instance
    
    
  public typealias SuccessHandler = (NSDictionary,HTTPURLResponse?)->Void
    // TYPEAlIAS :- it allows you to provide a new name for an existing data type into your program
    // HTTPURLRESPONSE :- it is a subclass of URLResponse that provides methods for accessing information specific to HTTP protocol responses
   public typealias ErrorHandler = (Any,HTTPURLResponse?) -> Void
    var mainUrl: String?

    
    
    func API(Url: String?,parameters: Any?,httpMethodType: HTTPMethod,success:@escaping SuccessHandler,Error:@escaping ErrorHandler){
        
        let contentType = "application/json"
        let accept = "application/json"
        
        let request = NSMutableURLRequest()
      //NSMUTABLEREQUEST :- it is a subclass of url request that allows us to change the properties of an url request
        
        
         //-----khanepani api
        //"https://uatservices.khalti.com/api/servicegroup/counters/khanepani/"
        
        //------billerlist API---\\
        //"https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerList"
        
        //-----billlerimages----\\
        //"https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerImages?ref="
        
        //-----billerdynamicform------\\
        //"https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerJsonFile"
        
        
        
        
        
        
        if !(Url?.contains("https://"))!{
            if Url == "api/servicegroup/counters/khanepani/"{
                mainUrl = "https://uatservices.khalti.com/"+Url!
            }
            else if Url == "/billPayments/billerList"{
                mainUrl = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com"+Url!
            }
            else if Url == "/billPayments/billerImages?ref="{
                mainUrl = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com"+Url!
            }
        }
            else{
                mainUrl = "https://bp-ug-mobi-test.apps.test.aro.kcbgroup.com/billPayments/billerJsonFile"
            }
        
        
        
        
        
            
        var postData:Data?
        
        if (httpMethodType == .GET){
            var parameterString:String?
            
            if(parameters != nil){
                
                
                
                if parameters is NSString {
                  
                    parameterString = parameters as? String
                    
                }
            
            else if parameters is NSDictionary{
                parameterString = stringParams(Dictionary: parameters as? NSDictionary)
            }
//                mainUrl = mainUrl! + "?" + parameterString!

        }
            request.addValue(contentType, forHTTPHeaderField: "image/png")
            let postLength = String(format: "%lu", Int(postData?.count ?? 0))
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
            let headers = ["Content-Type" : "image/png"]
            request.allHTTPHeaderFields = headers
            request.httpMethod = "GET"
        
        
    }
  
        else if(httpMethodType == .POST){
            var parameterString:String?

            if(parameters != nil){
                
                
                    if parameters is NSData {
                        postData = parameters as? Data
                    }
                
                else  {
                
                   postData = dataParams(parameters)
                    
                }
            
        }
            let postLength = String(format: "%lu", Int(postData?.count ?? 0))
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
            request.httpBody = postData
            request.httpMethod = "POST"
        }
        
        
        else if(httpMethodType == .PUT){
            var parameterString:String?

            if(parameters != nil){
                
                
                    if parameters is NSData {
                        postData = parameters as? Data
                    }
                
                else  {
                
                   postData = dataParams(parameters)
                    
                }
            
        }
            let postLength = String(format: "%lu", Int(postData?.count ?? 0))
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
            request.httpBody = postData
            request.httpMethod = "PUT"
        }
        
        
        let httpHeaders:Dictionary = ["Content-Type":contentType,"accept":accept,"channel":"App","inst":"1"]
        
        request.url = URL(string: mainUrl ?? "")
        request.allHTTPHeaderFields = httpHeaders
        request.timeoutInterval = 30
        
        let session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: nil)
        
        
        //task:- creates a task that retrives the contents of the url then calls completion handler. It returns the data to the app memory
        let task = session
            .dataTask(with: request as URLRequest, completionHandler: {data,response,error in
                guard error == nil else {
                    print("Error while calling the post")
                    return
                }
                guard let data = data else {
                    print("Did not recieve the data")
                    return
                }
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                    print("anytime soon\(json)")
                    success(json,response as? HTTPURLResponse)
                }
                catch{
                    Error(data,response as? HTTPURLResponse)
                }
            })
        
        
        task.resume()
        
        
        
    // converting from Dictionary to String
    func stringParams(Dictionary parameters: NSDictionary?) -> String? {

        var parameterString = ""
        let names: [String] = parameters?.allKeys as! [String]
        //allkeys :- A new array containing the dictionary's keys, or an empty array if the dictionary has no entries
        
        for key in names {
            guard let paramKey = key as? String else {
                continue
            }
            if let paramVal = parameters?[paramKey] {
                parameterString += "\(paramKey)=\(paramVal)"
            }
        }
        return parameterString
    }
    
}
 
    
        //converting data to string
    
    func dataParams(_ parameters: Any?) -> Data? {
        var dictionary: [AnyHashable : Any] = [:]

         let parameters = parameters as? [AnyHashable : Any]
        dictionary = parameters!
        
        var DataConversion: Data? = nil
        do {
            DataConversion = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        } catch {
            print("error in the conversion of string to data")
        }
        
        //conversion of string to data
        var StringConversion: String? = nil
        if let DataConversion = DataConversion {
            StringConversion = String(data: DataConversion, encoding: .utf8)
        }
        print("Result", StringConversion as Any)
        
        return DataConversion
    
    
    }

}
extension ApiCalling {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            guard let serverTrust = challenge.protectionSpace.serverTrust else {
                completionHandler(.cancelAuthenticationChallenge, nil);
                return
            }

            let certificate = SecTrustGetCertificateAtIndex(serverTrust, 0)

            // SSL Policies for domain name check
            let policy = NSMutableArray()
            policy.add(SecPolicyCreateSSL(true, challenge.protectionSpace.host as CFString))

            //evaluate server certifiacte
            let isServerTrusted = SecTrustEvaluateWithError(serverTrust, nil)

            //Local and Remote certificate Data
            let remoteCertificateData:NSData =  SecCertificateCopyData(certificate!)

            let pathToCertificate = Bundle.main.path(forResource: "kcbgroup", ofType: "der")
            let localCertificateData:NSData = NSData(contentsOfFile: pathToCertificate!)!
            //Compare certificates
            if(remoteCertificateData.isEqual(to: localCertificateData as Data)){
                let credential:URLCredential =  URLCredential(trust:serverTrust)
                print("Certificate pinning is successfully completed")
                completionHandler(.useCredential,credential)
            }
            else {

                completionHandler(.cancelAuthenticationChallenge,nil)
            }
        }
    
}
