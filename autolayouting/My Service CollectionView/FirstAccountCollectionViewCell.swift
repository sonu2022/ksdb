//
//  FirstAccountCollectionViewCell.swift
//  autolayouting
//
//  Created by Manam on 25/03/22.
//

import UIKit

class FirstAccountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var FirstAccount: UIView!
    
    @IBOutlet var FirstAccountImage: UIImageView!
    
    
    @IBOutlet var FirstAccountLabel: UILabel!
    
    
    @IBOutlet var MyBillsLabel: UILabel!
    @IBOutlet var MyBillsView: UIView!
    @IBOutlet var MyBillsImage: UIImageView!
}
