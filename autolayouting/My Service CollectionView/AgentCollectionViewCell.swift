//
//  AgentCollectionViewCell.swift
//  autolayouting
//
//  Created by Manam on 25/03/22.
//

import UIKit

class AgentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var AgentView: UIView!
    
    
    @IBOutlet var AgentImage: UIImageView!
    
    @IBOutlet var AgentLabel: UILabel!
    
}
