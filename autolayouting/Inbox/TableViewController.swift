//
//  TableViewController.swift
//  autolayouting
//
//  Created by Manam on 25/05/22.
//

import UIKit
import UserNotifications
import RealmSwift
import Realm
import NotificationCenter



extension String {
  
    var isNotEmptys: Bool {
        return !isEmpty
    }
}







@available(iOS 13.0, *)
class TViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,NotificationSettingDelegate{
    
    
    @IBOutlet var MainView: UIView!
    
    
    
    
    
    
    @IBOutlet var readNotification: UIButton!
    
    
    @IBOutlet var deleteNotification: UIButton!
    
    

    @IBOutlet var readallNotifications: UILabel!
    
    private var notificationUnderLine:UILabel!={
    let notificationUnderLine = UILabel(frame: CGRect(x: 46, y: 360, width: 320, height: 1))
        notificationUnderLine.backgroundColor = UIColor(red: 226/255.0, green: 231/255.0, blue: 239/255.0, alpha: 1.0)
       return notificationUnderLine
    }()
    
    private var middleUnderLine:UILabel!={
    let middleUnderLine = UILabel(frame: CGRect(x: 46, y: 360, width: 320, height: 1))
        middleUnderLine.backgroundColor = UIColor(red: 226/255.0, green: 231/255.0, blue: 239/255.0, alpha: 1.0)
       return middleUnderLine
    }()
    
    
    @IBOutlet var notificationSettingLabel: UILabel!
    
    
    @IBOutlet var notificationSettingView: UIView!
    
    
    
    
    

    
    var notif:[AnyHashable] = []

    
    

    let defaultss = UserDefaults.standard


    
    
    var namez = [UserDefaults.standard.string(forKey: "ReceivedNotifications")]
    
    var hope = [NotificationCenter.default.post(name:NSNotification.Name(rawValue: "ReceivedNotifications") , object: nil, userInfo:UIApplication.shared.userActivity?.userInfo)]
    
    
    

    var userInfo = [NotificationCenter.default]

    
   
    
    @IBOutlet var settingButton: UIButton!

    @IBOutlet weak var tableView: UITableView!
    
    
    

    
    var order:[Order] = []
    var order2: Order?
    var order3 = Order()
    var notificationToken: NotificationToken?

    let realm = try! Realm()
    let results = TableViewCell()
    var updateData:Bool = false


    
    var selectedRow:Int?

    
    var gradient = CAGradientLayer()

    
//
//    var selectedRow: String {
//            return UserDefaults.standard.value(forKey: "ReceivedNotifications") as? String ?? ""
//        }
   
    

        override func viewWillLayoutSubviews() {
            self.navigationController?.isNavigationBarHidden = false
        }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let application = UIApplication.shared

                registerPushNotification(application)
        
        print("the appplication is \(userInfo)")
        
        

        
//        notificationController.allRead = self



        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: NSNotification.Name(rawValue: "TestNotification"), object: nil)

        

        MainView.backgroundColor = UIColor(red: 230/255.0, green: 234/255.0, blue: 243/255.0, alpha: 1.0)


        tableView.allowsMultipleSelection = true
  
        
      
        print("blablabla   \(String(describing: results.name))")
        
        
        
        fetchData()

        

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
//              sample()

        
        
        
        print("the index is \(order.count)")
        
        
        
        
                }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
        if order.count != 0{
            tableView.isHidden = false
            tableView.reloadData()
        }
        else{
            tableView.isHidden = true
        }
        
        
        tableView.reloadData()
    }
      
    
    
    //called for delegation purpose
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if let secondVC = segue.destination as? NotificationSettingViewController {
               secondVC.allRead = self
           }
    
    }

    
    func deleteAllNotification(_ delete: Bool) {
        let realm = try! Realm()
       //
                   try! realm.write {
                       realm.deleteAll()
//                       tableView.deleteRows(at: [currentIndexPath], with: .automatic)

                       tableView.reloadData()
                       tableView.isHidden = true

                       dismiss(animated: true,completion: nil)
                       
                   }
        
        
       
   }
    
    
    func allNotificationRead(_ read: Bool) {
        
        let indexPath = IndexPath.init(row: selectedRow ?? 0, section: 0)

       
        let view = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! TableViewCell
        
        if view.isSelected{
        for lay in view.contentView.layer.sublayers ?? [] {
            if lay is CAGradientLayer {
                lay.removeFromSuperlayer()
                view.subView.layer.backgroundColor = UIColor.white.cgColor
                tableView.reloadData()
            }
        }
        
        }
       
        tableView.reloadData()
        dismiss(animated: true,completion: nil)

        }
        
    
    
    
    
    
    
    
//    func sample(){
//
//
//        let indexPath = IndexPath(row: 0, section: 0)
//
//        let view = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! TableViewCell
//
//
//        let Name =  view.name.text
//        let Subtitle =  view.subtitle.text
//
//
//
//        let newReg = Order()
//        let realm = try! Realm()
//
//        newReg.setValue(view.name.text, forKey: "Name")
//        newReg.setValue(view.subtitle.text, forKey: "Subtitle")
//
//
//
//
//        try! realm.write {
//       //
//                   order3.name = Name ?? ""
//                   order3.subtitle = Subtitle ?? ""
//
//
//                                   print("the NAME is\(String(describing:  order3.name))")
//                                   print("the NAME is\(String(describing:   order3.subtitle))")
//
//
//                           }
//
//
////        let workouts = realm.objects(Order.self)
////
////            if let workout = workouts.first {     // this first is used when there are multiple values
////                try! realm.write {
////
////                    workout.name = Name ?? ""
////                    workout.subtitle = Subtitle ?? ""
////
////                    print("the NAME is\(String(describing:  workout.name))")
////                    print("the NAME is\(String(describing:   workout.subtitle))")
////
////
////            }
////            }
////        else{
////                do {
////                    try realm.write {
////                        realm.add(newReg)
////
////                    view.name.text = ""
////                        view.subtitle.text = ""
////                        print("Added \(newReg.name) to Realm DataBase")
////
////
////                    }
////
////                } catch {
////                    print(error)
////                }
////            }
//
//
//        }
    
    
    
    
 
    

    
    
    
    
    
//    func updateNotificationBadge()
//    {
//    let item = tabBarController?.tabBar.items?[5]
//    let str = NotificationHandler.sharedInstance().getNotiBadgeCount
//    print(" str \( str)")
//    if str != "0" {
//        item?.badgeValue = NotificationHandler.sharedInstance().getNotiBadgeCount
//        UIApplication.shared.applicationIconBadgeNumber = NotificationHandler.shared.getNotiBadgeCount.intValue
//    }
//    else{
//        item.badgeValue = 0
//        UIApplication.shared.applicationIconBadgeNumber = 0
//    }
//    }

    
    
    
    
    
    
    
    
    func registerPushNotification(_ application: UIApplication){

                UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in

                    if granted {
                        
                        print("Notification: Granted")

                    } else {
                        print("Notification: not granted")

                    }
                }

                application.registerForRemoteNotifications()
            }

    
    @objc func notificationReceived(_ notification: Notification) {
        //getting some data from userInfo is optional
//        guard (notification.userInfo?["text"] as? String) != nil else { return }
//        let defaults = UserDefaults.standard
//        if let name = defaults.string(forKey: "notificationTitle") {
//            print(name)
//        }
//
//                tableView.reloadData()
        
        let defaults = UserDefaults.standard
        var checkAlert = ""
        checkAlert = defaults.string(forKey: "ReceivedNotifications") ?? ""
        print("Alert Message: \(checkAlert)")
        if namez == nil {
            namez = [checkAlert]
        } else {
            namez.append(checkAlert)
        }
        
        
        
        
    }
    
    
  
    
    @IBAction func settingButton(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        
        let settings = story.instantiateViewController(withIdentifier: "NotificationSettingViewController") as? NotificationSettingViewController
        self.navigationController?.pushViewController(settings!, animated: true)
    }

    

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    
    
//    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
//    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return order.count
    }
    
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.backgroundColor = .white


        let view = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! TableViewCell

      
        
             
        
           
      
        

    }
    

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        let view = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! TableViewCell
        

        selectedRow = indexPath.row
        
        order = Array(realm.objects(Order.self))

            for person in self.order {
                DispatchQueue.main.async {

                view.name.text = person.name
                view.subtitle.text = person.subtitle
//            print("the value after is\(String(describing: view.name))")
//            print("the location is \(Realm.Configuration.defaultConfiguration.fileURL!)")
                    
                  
                    
                    
                    self.tableView.reloadData()

             }


        }
       
        return view
    }
 
    
    func fetchData() {
        let indexPath = IndexPath(row: 0, section: 0)

           let realm = try! Realm()
            order = Array(realm.objects(Order.self))
        let view = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! TableViewCell

           for person in order {
               print("well the value is \(person.name) , \(person.subtitle)")
           }
       }
    
    
    
    
    
    
    
    //for inserting the notification below the next cell
    override func setEditing(_ editing: Bool, animated: Bool) {
        let indexPath = IndexPath(row: 0, section: 0)
        super.setEditing(editing, animated: animated)
        tableView.setEditing(editing, animated: animated)
        //fill paths of insertion rows here
        if editing {
            tableView.insertRows(at:[indexPath], with: .bottom)
        }
        else {
            tableView.deleteRows(at: [indexPath], with: .bottom)
        }
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    

    
     // for swipe deleting
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                self.order.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.reloadData()
//                let userDefaults = UserDefaults.standard
                
//                let encodedData : Data = NSKeyedArchiver.archivedData(withRootObject: order as Any)
//                userDefaults.set(encodedData, forKey: "ReceivedNotifications")
//                userDefaults.synchronize()
                
                

                
                
                
                
                
            }
        }
    
    
}

