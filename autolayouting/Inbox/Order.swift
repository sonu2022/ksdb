//
//  Order.swift
//  autolayouting
//
//  Created by Manam on 31/05/22.
//

import UIKit
import RealmSwift

class Order: Object {
 

        @objc dynamic var id = UUID().uuidString
        @objc dynamic var name = ""
        @objc dynamic var subtitle = ""
        @objc dynamic var body = ""

        @objc dynamic var createDate = Date()
    


        override static func primaryKey() -> String? {
            return "id"
        }
    
   
    func deleteContact(contact: Order){
        let realm = try! Realm()

        try! realm.write{
            realm.delete(contact)
            }
        }
    
    
    }
    class RealmDao: NSObject {

        static let shared = RealmDao()
        private var realm: Realm!

        private override init() {

            self.realm = try! Realm()
        }

        func getRealmObject() -> Realm {
            return self.realm
        }
        
       
        
    }



