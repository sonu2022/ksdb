//
//  BillPaymentCollectionViewCell.swift
//  autolayouting
//
//  Created by Manam on 28/03/22.
//

import UIKit

class BillPaymentCollectionViewCell: UICollectionViewCell {
    @IBOutlet var ElectricityView: UIView!
    @IBOutlet var ElectricityImage: UIImageView!
    
    
    @IBOutlet var KhanepaniView: UIView!
    @IBOutlet var KhanepaniImage: UIImageView!
    
    
    @IBOutlet var InternetView: UIView!
    @IBOutlet var InternetImage: UIImageView!
    
    
    @IBOutlet var RcCardView: UIView!
    @IBOutlet var RcCardImage: UIImageView!
    
    @IBOutlet var LandlineView: UIView!
    @IBOutlet var LandlineImage: UIImageView!
    
    
    @IBOutlet var TvImage: UIImageView!
    @IBOutlet var TvView: UIView!
    
    
    @IBOutlet var SchoolView: UIView!
    @IBOutlet var SchoolImage: UIImageView!
    
    @IBOutlet var ElearningView: UIView!
    @IBOutlet var ElearningImage: UIImageView!
    
    
    
    
    
    
    
}
